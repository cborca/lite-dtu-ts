import time
import matplotlib.pyplot as plt
import argparse

from ldtu_commlib import ldtu_comm
from analysis_lib import sine_wave, pulse_signal
from inst_commlib import td_t3afg120, ks_e36312a
from autotest_func import *

keysight_ps_ipaddr = "192.168.1.33"
wfg_calibH_ipaddr  = "192.168.1.60"
wfg_calibL_ipaddr  = "192.168.1.61"
wfg_sin_HL_ipaddr  = "192.168.1.62"

parser = argparse.ArgumentParser()
parser.add_argument("-w", "--window", type = int, choices = (8, 16), default = 8,
                    help = "Gain switch window configuration")
args = parser.parse_args()

chip =  64            ## Chip ID number

n_data_pulse = 50000  ## 50000 to have 200 160 kHz cycles
                      ## 12500 to have  50 160 kHz cycles

dt = 6.25e-9
F  = 160e3

ldtu = ldtu_comm()                        ## LiTE-DTU
ps   = ks_e36312a(keysight_ps_ipaddr)     ## Power supply
wfgH = td_t3afg120(wfg_calibH_ipaddr)     ## waveform generator connected to ADCH DC inputs
wfgL = td_t3afg120(wfg_calibL_ipaddr)     ## waveform generator connected to ADCL DC inputs


save_folder = "data"
raw_folder = save_folder + "/raw"
rawDir = raw_folder + "/"

##################################################################################
## Pulse signals test
##

if args.window == 16:
  ldtu.cu_config("cfg/v2.0_manualLock_w16.txt") ## Load w16 configuration
  steps = range(3, 16)                          ## Set number DC steps 3->15
else:  
  ldtu.cu_config("cfg/v2.0_manualLock.txt")     ## Load w08 configuration
  steps = range(3, 21)                          ## Set number of DC steps 3-> 20

ldtu.set_switch("write", "DTU")                 ## Set DTU-mode and enable switches for DC inputs (0,1,1,1,1)
ldtu.dtu_mode()                                 ## Set DTU-mode


for step in steps: 

   ## steps <= 3 -> 0 saturated samples
   ## steps  > 3 -> (steps - 3) saturated samples
                           
   print("\n\n")

   print("Steps = {}".format(step))

   duty = 100*step*dt*F
   print("Duty cycle = {:.02f}%".format(duty))

   filename = ldtu.build_fn()       ## generate filename to save the data acquired
   filename = filename[:-4] + "_{:03d}_w{:02d}_sqrwv_{:02d}".format(chip, args.window, step) + ".txt"

   print("Data will be saved as " +  filename)

   wfgH.set_wfg("square")           ## set parameters for ADCH input pulse
   wfgH.set_wfg_duty(str(duty))

   wfgL.set_wfg("calib")

   time.sleep(2)

   data_pulse = ldtu.acquire_data(n_data_pulse)       ## acquire data
   data_pulse.write_raw(rawDir, filename, "bin")      ## save data in raw format
   pul = pulse_signal(rawDir + filename)              ## prepare for plot
   pul.decode_data()                                  ## decode raw data
   pul.check_samples(pulse_type="saturation")         ## check samples (baseline, G10, G1)
   pul.plot_pulse(show=False)                         ## plot and fit pulse signal

#  pul.save_plot()                                    ## save plot

#  plt.show()
