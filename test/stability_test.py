from ldtu_commlib import ldtu_comm
from inst_commlib import td_t3afg120, ks_e36312a
from inst_commlib import rs_sma100b
from autotest_func import *
from autotest_func_offline import *
import sys, os
import matplotlib.pyplot as plt
import time



try:
   chip_num = int(sys.argv[1])
   print("\n\nStarting test of chip n.{}...\n".format(chip_num))
   chip_folder = True
except:
   print("\n\nWarning: chip number not given!\n")
   ans = input('Do you want to continue anyway? (y=yes) ')
   if ans == "y":
      print("No chip number was given, test will continue with chip_num set to -1.\n")
      chip_num = -1
      chip_folder = False
   else:
      print("Test aborted since chip number was not given.\n")
      sys.exit()

if chip_folder:
   save_folder = "outputs/mass_test/{0:0>5}".format(chip_num)
   raw_folder = save_folder + "/raw"
   dec_folder = save_folder + "/decoded"
   if not(os.path.isdir(save_folder)):
      os.mkdir(save_folder)
      os.mkdir(raw_folder)
      os.mkdir(dec_folder)
else:
   save_folder = "data"
   raw_folder = save_folder + "/raw"
   dec_folder = save_folder + "/decoded"
   

rawDir = raw_folder + "/"
decDir = dec_folder + "/"

init_pass = None
pll_pass = None
align_pass = None
adc_pass = None
dtu_pass = None


start_time = time.time()

##################################################################
## Load settings for autotest procedure
##
mode, inst, lv, daq, enob_opt = load_settings()
offline = mode
keysight_ps_ipaddr, wfg_calibH_ipaddr, wfg_calibL_ipaddr, wfg_sin_HL_ipaddr, rs_sma100b_ipaddr = inst
Iref, tol = lv
cfgfile, n_data_sin, n_data_enob, n_data_pulse, N_ini, N_pll, N_ali, N_cal, N_pul, std_max, enob_min, adc_bsln = daq
enob_meas, enob_scan, enob_freq, enob_ver, enob_filt, enob_OCXO, enob_manLock, enob_clkInt, enob_adcH = enob_opt
##
##################################################################






if not(offline):

   ##################################################################
   ## Initialize classes used during test
   ##
   ldtu = ldtu_comm()                        ## LiTE-DTU
   ps   = ks_e36312a(keysight_ps_ipaddr)     ## Power supply
   wfgH = td_t3afg120(wfg_calibH_ipaddr)     ## waveform generator connected to ADCH DC inputs
   wfgL = td_t3afg120(wfg_calibL_ipaddr)     ## waveform generator connected to ADCL DC inputs
   wfgS = td_t3afg120(wfg_sin_HL_ipaddr)     ## waveform generator connected to ADCs AC inputs
   ##
   ##################################################################

   ##################################################################################
   ##
   #wfgH.set_wfg("off")     ## disable the ADCH pulse generator output
   #wfgL.set_wfg("off")     ## disable the ADCL pulse generator output
   #wfgS.set_wfg("off")     ## disable the AC waveform generator output
   ##
   ##################################################################################

else:
   print("\n")
   print("**************************")
   print("** OFFLINE MODE ENABLED **")
   print("**************************")
   print("\n")

next = True


"""
pllReg = ['0x0', '0x3c']      ## Default settings for PLL manual lock registers
pllReg[1] = hex(57)
reg = int(pllReg[1], 16)
print("{0:3d}\t{1:03x}\t{2:09b}".format(reg, reg, reg))
ldtu.writePLLreg(pllReg)

ldtu.set_switch("write", "ATM")        ## set switches to 'Test-mode' (1,0,0,0,0)

time.sleep(1)

ldtu.eye_align()
ldtu.align_data()
"""


for i in range(100):
   print("Acquiring baseline samples: cycle n.{}".format(i))
   acq_bsln(rawDir, ldtu, wfgH, wfgL, adc_bsln)

sys.exit()



if next:
   ##################################################################
   ## Test DTU with pulse-like signals
   ##
   if offline:
      ret = dtu_test_offline(rawDir, None, None, None, None, n_data_pulse, N_pul)
   else:
      #bslnSub = bsln_test(rawDir, ldtu, wfgH, wfgL, adc_bsln)
      bslnSub = 85
      print("Baseline subtraction = {}".format(bslnSub))
      duration = 10000
      dtu_stability(rawDir, ldtu, wfgH, wfgL, bslnSub, duration)
      #for i in range(100):
      #   print("Acquiring baseline samples: cycle n.{}".format(i))
      #   acq_bsln(rawDir, ldtu, wfgH, wfgL, adc_bsln)
      #ret = dtu_test(rawDir, ps, ldtu, wfgH, wfgL, n_data_pulse, N_pul)

   if ret == -1:
      next = False
      dtu_pass = False
   else:
      dtu_pass = True
      N3a, N3b = ret
   ##
   ##################################################################


plt.show()
sys.exit()


if not(offline):
   ##################################################################################
   ##
   wfgH.set_wfg("off")     ## disable the ADCH pulse generator output
   wfgL.set_wfg("off")     ## disable the ADCL pulse generator output
   ##
   ##################################################################################



end_time = time.time()
print("\n\nElapsed time = {:.2f} s".format(end_time - start_time))




plt.show()



