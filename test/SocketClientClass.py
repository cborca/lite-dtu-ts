########################################################################################
#################### QUALCHE print() BISOGNERA' ELIMINARLO PER NON  ####################
#################### SCRIVERE DURANTE IL RUN DEL TEST PROGRAM INFN. ####################
#################### MAGARI SI TENGONO SOLO QUELLI PIU' IMPORTANTI  ####################
#################### O DI MAGGIOR INTERESSE.                        ####################
########################################################################################
#from collections import Counter
import socket
import sys
import time
import textwrap

HOST = "127.0.0.1"
PORT = 15000


class TcpClient:
    def __init__(self, server_ip, server_port):
        self.server_ip = server_ip
        self.server_port = server_port
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        #self.connect()
        
    def connect(self):
        try:
            self.socket.connect((self.server_ip, self.server_port))
            print("Connecting to the server {} established".format(self.server_ip))
            return self
        except socket.error as error:
            print("Unable to connect to server {}!\nError: {}".format(self.server_ip, error))
            sys.exit()
        
        
    def close(self):
        self.socket.close()


    def send_command(self, command):
    
        ## Commands that close Client connection ##
        if command == "START":                        ## command == "START"
            print("->", command)
            self.socket.send(command.encode())
            data = self.socket.recv(1024)
            print(str(data, "UTF-8 "), '\n')
            
        elif command == "COMPLETE":                   ## command == "COMPLETE"
            print("->", command)
            self.socket.send(command.encode())
            print("Closing connection with server...\n")
            self.close()
            sys.exit()
            
        elif command == "FAIL":                       ## command == "FAIL"
            print("->", command)
            self.socket.send(command.encode())
            print("Step failed!")
            print("Closing connection with server...\n")
            self.close()
            sys.exit()
        
        ## Commands for handling request by Server ##
        elif command == "POWER_CYCLE":                ## command == "POWER CYCLE"
            print("->", command)
            self.socket.send(command.encode())
            data = self.socket.recv(1024)
            print(str(data, "UTF-8 "), '\n')
            
        elif command == "PWRCons":                    ## command == "PWRCons"
            print("->", command)
            self.socket.send(command.encode())
            data = self.socket.recv(1024)
            print(str(data, "UTF-8 "), '\n')
            data = str(data, "UTF-8 ")
            if data[0:5] == 'NOTOK':
                data = "NOTOK"
            elif(data[0:2] == "OK"):
                data = "OK"
            return data
            
        elif command == "WRT_PCons":                  ## command == "WRT_PCons"
            print("->", command)
            self.socket.send(command.encode())
            data = self.socket.recv(1024)
            print(str(data, "UTF-8 "), '\n')
        
        ## Commands for handling External Instrument Error ##
        elif command == "USB_ERROR":                 ## command == "USB_ERROR"
            print("->", command)
            self.socket.send(command.encode())
            print("Closing connection with server...\n")
            self.close()
            sys.exit()
            
        elif command == "CG635_NOTOK":                 ## command == "CG635_NOTOK"
            print("->", command)
            self.socket.send(command.encode())
            print("Closing connection with server...\n")
            self.close()
            sys.exit()
        
        elif command == "FPGA_NOTOK":                 ## command == "FPGA_NOTOK"
            print("->", command)
            self.socket.send(command.encode())
            print("Closing connection with server...\n")
            self.close()
            sys.exit()
            
        elif command == "WFGH_NOTOK":                 ## command == "WFGH_NOTOK"
            print("->", command)
            self.socket.send(command.encode())
            print("Closing connection with server...\n")
            self.close()
            sys.exit()
            
        elif command == "WFGL_NOTOK":                 ## command == "WFGL_NOTOK"
            print("->", command)
            self.socket.send(command.encode())
            print("Closing connection with server...\n")
            self.close()
            sys.exit()
            
        elif command == "WFGS_NOTOK":                 ## command == "WFGS_NOTOK"
            print("->", command)
            self.socket.send(command.encode())
            print("Closing connection with server...\n")
            self.close()
            sys.exit()
        
        ## Commands for send OK for External Instrument ##
        elif command == "CG635_OK":                    ## command == "CG635_OK"
            print("->", command)
            self.socket.send(command.encode())
            data = self.socket.recv(1024)
            print(str(data, "UTF-8 "), '\n')
        
        elif command == "FPGA_OK":                    ## command == "FPGA_OK"
            print("->", command)
            self.socket.send(command.encode())
            data = self.socket.recv(1024)
            print(str(data, "UTF-8 "), '\n')

        elif command == "WFGH_OK":                    ## command == "WFGH_OK"
            print("->", command)
            self.socket.send(command.encode())
            data = self.socket.recv(1024)
            print(str(data, "UTF-8 "), '\n')
            
        elif command == "WFGL_OK":                    ## command == "WFGL_OK"
            print("->", command)
            self.socket.send(command.encode())
            data = self.socket.recv(1024)
            print(str(data, "UTF-8 "), '\n')
            
        elif command == "WFGS_OK":                    ## command == "WFGS_OK"
            print("->", command)
            self.socket.send(command.encode())
            data = self.socket.recv(1024)
            print(str(data, "UTF-8 "), '\n')
        
        ## Commands for handling LiTE-DTU errors ##
        elif command == "ACK":                        ## command == "ACK"
            print("->", command)
            self.socket.send(command.encode())
            print("Closing connection with server...\n")
            self.close()
            sys.exit()
            
        elif command == "MRK":                        ## command == "MRK"
            print("->", command)
            self.socket.send(command.encode())
            print("Closing connection with server...\n")
            self.close()
            sys.exit()
            
        elif command == "DIFF_ALIGN":                 ## command == "DIFF_ALIGN"
            print("->", command)
            self.socket.send(command.encode())
            print("Closing connection with server...\n")
            self.close()
            sys.exit()
            
        elif command == "INV_CMD_STR":                ## command == "INV_CMD_STR"
            print("->", command)
            self.socket.send(command.encode())
            print("Closing connection with server...\n")
            self.close()
            sys.exit()
            
        elif command == "INV_TRGT_STR":               ## command == "INV_TRGT_STR"
            print("->", command)
            self.socket.send(command.encode())
            print("Closing connection with server...\n")
            self.close()
            sys.exit()
            
        elif command == "INV_ARGM":                   ## command == "INV_ARGM"
            print("->", command)
            self.socket.send(command.encode())
            print("Closing connection with server...\n")
            self.close()
            sys.exit()


tcp_client = TcpClient(HOST, PORT)