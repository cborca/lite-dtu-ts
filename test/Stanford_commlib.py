#### DEVELOPMENT STANFORD CG635 CLASS AND FUNCTIONS ####

import pyvisa as visa
import time
import serial.tools.list_ports
import configparser

from SocketClientClass import tcp_client as SockClientTCP


def upper_string(code):
    ## convert the command string into upper case
    code = code.upper()
    return code

def AutoSearchPort(port_vid, port_pid):
    # Get list of all serial and parallel ports
    all_ports = list(serial.tools.list_ports.comports())
    
    if not all_ports:
        return False
    
    # Create empty list to store information for ASRL::INSTR
    instr_ports = []

    # Iterate through each port and check if it is in use
    for port in all_ports:
        if port.name.startswith("COM") or port.name.startswith("LPT"):
            if(port.vid is not None or port.pid is not None):
                if(upper_string(format(port.vid, '04x')) == port_vid and upper_string(format(port.pid, '04x')) == port_pid):
                #if(upper_string(format(port.vid, '#06x')) == '0X067B' and upper_string(format(port.pid, '#06x')) == '0X23A3'):
                    if port.serial_number is not None:
                        instr_port = "ASRL{}::INSTR".format(port.device[-1])        ## It's a single value
                        instr_ports.append(instr_port)                              ## It's a list!
                        print("VID: {}\nPID: {}\nPort: {}\n".format(upper_string(format(port.vid, '04x')), upper_string(format(port.pid, '04x')), instr_port))
                        #print(instr_ports)
                        return instr_port
                else:
                    print("Port didn't find!")
                    SockClientTCP.send_command("USB_ERROR")
                    #return False
        else:
            return False

    # Display list of ASRL::INSTR ports in use
    print(instr_ports)

class stanford_CG635:
    def __init__(self, connection = "RS232", addr = ""):
        self.FlagCheck_USB = None

        if connection == "IEEE488":
            try: 
                self.rm = visa.ResourceManager()
                self.Port = addr
                self.CG635 = self.rm.open_resource('GPIB::' + self.Port + '::INSTR')
                self.FlagCheck_USB = True
                if(self.FlagCheck_USB == True):
                    print("USB cable connected with PC")
                reply = self.CG635.query('*IDN?')
                self.FlagCheck_CG635 = True                          ## Added from F. Deiana
                if(self.FlagCheck_CG635 == True):
                    print("Stanford CG635: switch ON\n")
                #print("CG635 ID: " + reply.strip())
            except:
                self.FlagCheck_CG635 = False                         ## Added from F. Deiana
                print("Possible problem:")
                print("1 - Stanford CG635: switch OFF")
                print("2 - IEEE488 cable,instrument side, not connetced correctly")
                print("3 - Interface set for RS-232 communication; please change to GPIB interface (see instrument front pannel)")
                if(self.FlagCheck_USB != True):
                    print("4 - USB cable,PC side, not connetced correctly")
                print("Unable to reach the synthesized clock generator at " + self.Port, '\n')
                #raise Exception("Unable to reach the synthesized clock generator at " + self.Port)
        elif connection == "RS232":
            try: 
                self.rm = visa.ResourceManager()
                self.COM = addr
                self.CG635 = self.rm.open_resource(self.COM)
                self.FlagCheck_USB = True                          ## Added from F. Deiana
                if(self.FlagCheck_USB == True):
                    print("USB cable connected with PC")
                reply = self.CG635.query('*IDN?')
                self.FlagCheck_CG635 = True
                if(self.FlagCheck_CG635 == True):
                    print("Stanford CG635: switch ON\n")
                #print("CG635 ID: " + reply.strip())
            except:
                self.FlagCheck_CG635 = False                         ## Added from F. Deiana
                print("Possible problem:")
                print("1 - Stanford CG635: switch OFF")
                print("2 - RS232-DB9 cable,instrument side, not connetced correctly")
                print("3 - Interface set for GPIB communication; please change to RS-232 interface (see instrument front pannel)")
                if(self.FlagCheck_USB != True):
                    print("4 - USB cable,PC side, not connetced correctly")
                print("Unable to reach the synthesized clock generator at {}\n".format(self.COM))
                #raise Exception("Unable to reach the synthesized clock generator at " + self.COM)
    

###################################################################
### Function Synthesized Clock Generator
    
    def IndexCommands_CG635(self, ctrl_cmd):
    
        ## Instrument Control Command
        ctrl_cmd_up = upper_string(ctrl_cmd)
        
        if ctrl_cmd_up == "CMOS_OUTPUT":        ## CMOS Output
            ctrl_cmd_up = "CMOS"
            return ctrl_cmd_up
        
        elif ctrl_cmd_up == "DISPLAY":          ## Display
            ctrl_cmd_up = "DISP"
            return ctrl_cmd_up
            
        elif ctrl_cmd_up == "FREQUENCY":        ## Frequency
            ctrl_cmd_up = "FREQ"
            return ctrl_cmd_up
        
        elif ctrl_cmd_up == "PHASE":            ## Phase)
            ctrl_cmd_up = "PHAS"
            return ctrl_cmd_up
        
        elif ctrl_cmd_up == "PRBS":             ## Enable PRBS
            ctrl_cmd_up = "PRBS"
            return ctrl_cmd_up
        
        elif ctrl_cmd_up == "Q_OUTPUT":         ## Q/Q- Output
            ctrl_cmd_up = "QOUT"
            return ctrl_cmd_up
        
        elif ctrl_cmd_up == "REL_PHASE":        ## Relative Phase
            ctrl_cmd_up = "RPHS"
            return ctrl_cmd_up

        elif ctrl_cmd_up == "RUN":              ## Running State
            ctrl_cmd_up = "RUNS"
            return ctrl_cmd_up

        elif ctrl_cmd_up == "SHOW_DISPLAY":     ## Show Display
            ctrl_cmd_up = "SHDP"
            return ctrl_cmd_up

        elif ctrl_cmd_up == "STOP_LVL":         ## Stop Level
            ctrl_cmd_up = "SLVL"
            return ctrl_cmd_up

        elif ctrl_cmd_up == "STD_CMOS":         ## Standard CMOS
            ctrl_cmd_up = "STDC"
            return ctrl_cmd_up

        elif ctrl_cmd_up == "STD_Q":            ## Standard Q/Q-
            ctrl_cmd_up = "STDQ"
            return ctrl_cmd_up

        elif ctrl_cmd_up == "STEP_DOWN":        ## Step Down
            ctrl_cmd_up = "STPD"
            return ctrl_cmd_up

        elif ctrl_cmd_up == "STEP_SIZE":        ## Step Size
            ctrl_cmd_up = "STPS"
            return ctrl_cmd_up

        elif ctrl_cmd_up == "STEP_UP":          ## Step Up
            ctrl_cmd_up = "STPU"
            return ctrl_cmd_up

        elif ctrl_cmd_up == "TIMEBASE":         ## Timebase
            ctrl_cmd_up = "TIMB"
            return ctrl_cmd_up

        elif ctrl_cmd_up == "UNIT":             ## Units Display
            ctrl_cmd_up = "UNIT"
            return ctrl_cmd_up
    
    
        ## Interface Command
        elif ctrl_cmd_up == "ID_STRING":        ## Identification String
            ctrl_cmd_up = "*IDN"
            return ctrl_cmd_up

        elif ctrl_cmd_up == "OPER_COMPLETE":    ## Operation Complete
            ctrl_cmd_up = "*OPC"
            return ctrl_cmd_up

        elif ctrl_cmd_up == "RCL_INSTR_SET":    ## Recall Instrument Setting
            ctrl_cmd_up = "*RCL"
            return ctrl_cmd_up

        elif ctrl_cmd_up == "RST_INSTR":        ## Reset the Instrument
            ctrl_cmd_up = "*RST"
            return ctrl_cmd_up

        elif ctrl_cmd_up == "SAVE_SET":         ## Save Instrument Setting
            ctrl_cmd_up = "*SAV"
            return ctrl_cmd_up

        elif ctrl_cmd_up == "SELF_TEST":        ## SelfTest
            ctrl_cmd_up = "*TST"
            return ctrl_cmd_up

        elif ctrl_cmd_up == "WAIT_CMD_EXE":     ## Wait for Command Execution
            ctrl_cmd_up = "*WAI"
            return ctrl_cmd_up


        ## Status Report Command
        elif ctrl_cmd_up == "CLEAR":                ## Clear Status
            ctrl_cmd_up = "*CLS"
            return ctrl_cmd_up
        
        elif ctrl_cmd_up == "STD_STS_EN":           ## Standard Event Status Enable
            ctrl_cmd_up = "*ESE"
            return ctrl_cmd_up
            
        elif ctrl_cmd_up == "STD_STS_REG":          ## Standard Event Status Register
            ctrl_cmd_up = "*ESR"
            return ctrl_cmd_up
        
        elif ctrl_cmd_up == "POW_ON":               ## Power-on Status Clear
            ctrl_cmd_up = "*PSC"
            return ctrl_cmd_up
            
        elif ctrl_cmd_up == "SRV_REQ_EN":           ## Service Request Enable
            ctrl_cmd_up = "*SRE"
            return ctrl_cmd_up
            
        elif ctrl_cmd_up == "SRL_STS_BYTE":         ## Serial Poll Status Byte
            ctrl_cmd_up = "*STB"
            return ctrl_cmd_up
            
        elif ctrl_cmd_up == "COMM_ERR_STS_EN":      ## Communication Error Status Enable
            ctrl_cmd_up = "CESE"
            return ctrl_cmd_up
        
        elif ctrl_cmd_up == "COMM_ERR_STS_REG":     ## Communication Error Status Register
            ctrl_cmd_up = "CESR"
            return ctrl_cmd_up
            
        elif ctrl_cmd_up == "INSTR_STS_EN":         ## Instrument Status Enable
            ctrl_cmd_up = "INSE"
            return ctrl_cmd_up
            
        elif ctrl_cmd_up == "INSTR_STS_REG":        ## Instrument Status Register
            ctrl_cmd_up = "INSR"
            return ctrl_cmd_up
            
        elif ctrl_cmd_up == "PLL_LOCK_EN":          ## PLL Lock Status Enable
            ctrl_cmd_up = "LCKE"
            return ctrl_cmd_up
        
        elif ctrl_cmd_up == "PLL_LOCK_REG":         ## PLL Lock Status Register
            ctrl_cmd_up = "LCKR"
            return ctrl_cmd_up
            
        elif ctrl_cmd_up == "LAST_ERROR":           ## Last Error
            ctrl_cmd_up = "LERR"
            return ctrl_cmd_up

    def ErrorsCode_CG635(self, errcode):
        errcode = int(errcode)
        
        if(errcode == 0):
            msg = "No error"                        ## No more errors left in the queue
        
        elif(errcode == 10):
            msg = "Illegal value"                   ## A parameter was out of range.
        
        elif(errcode == 20):
            msg = "Frequency error"                 ## The frequency or frequency step size was illegal.
        
        elif(errcode == 30):
            msg = "Phase error"                     ## The phase step size was illegal.
        
        elif(errcode == 31):
            msg = "Phase step error"                ## The phase could not be set because it would have caused a phase step larger than 360°.
        
        elif(errcode == 40):
            msg = "Voltage error"                   ## The voltage could not be set because it was out of range.
        
        elif(errcode == 51):
            msg = "Q / Q¯ Low Changed"              ## Q / Q¯ high was set to the requested value, but Q / Q¯ low was also changed in order to
                                                    ## satisfy the Q / Q¯ amplitude limits.
        
        elif(errcode == 52):
            msg = "Q / Q¯ High Changed"             ## Q / Q¯ low was set to the requested value, but Q / Q¯ high was also changed in order to
                                                    ## satisfy the Q / Q¯ amplitude limits.
        
        elif(errcode == 61):
            msg = "CMOS Low Changed"                ## CMOS high was set to the requested value, but CMOS low was also changed in order to
                                                    ## satisfy the CMOS amplitude limits.
        
        elif(errcode == 62):
            msg = "CMOS High Changed"               ## CMOS low was set to the requested value, but CMOS high was also changed in order to
                                                    ## satisfy the CMOS amplitude limits.
        
        elif(errcode == 71):
            msg = "No PRBS"                         ## PRBS could not be enabled because it’s not installed.
        
        elif(errcode == 72):
            msg = "Failed Self Test"                ## The self test failed. The device dependent errors that caused the self test to fail will
                                                    ## follow in the error buffer.
        
        elif(errcode == 100):
            msg = "Lost Data"                       ## Data in the output buffer was lost. This occurs if the output buffer overflows, or if a
                                                    ## communications error occurs and data in output buffer is discarded.
        
        elif(errcode == 102):
            msg = "No Listener"                     ## This is a communications error that occurs if the CG635 is addressed to talk on the GPIB
                                                    ## bus, but there are no listeners. The CG635 discards any pending output.
        
        elif(errcode == 110):
            msg = "Illegal Command"                 ## The command syntax used was illegal. A command is normally a sequence of four letters,
                                                    ## or a ‘*’ followed by three letters.
        
        elif(errcode == 111):
            msg = "Undefined Command"               ## The specified command does not exist.
        
        elif(errcode == 112):
            msg = "Illegal Query"                   ## The specified command does not permit queries
        
        elif(errcode == 113):
            msg = "Illegal Set"                     ## The specified command can only be queried.
        
        elif(errcode == 114):
            msg = "Null Parameter"                  ## The parser detected an empty parameter.
        
        elif(errcode == 115):
            msg = "Extra Parameters"                ## The parser detected more parameters than allowed by the command.
        
        elif(errcode == 116):
            msg = "Missing Parameters"              ## The parser detected missing parameters required by the command.
        
        elif(errcode == 117):
            msg = "Parameter Overflow"              ## The buffer for storing parameter values overflowed. This probably indicates a syntax error.
        
        elif(errcode == 118):
            msg = "Invalid Floating Point Number"   ## The parser expected a floating point number, but was unable to parse it.
        
        elif(errcode == 120):
            msg = "Invalid Integer"                 ## The parser expected an integer, but was unable to parse it.
        
        elif(errcode == 122):
            msg = "Invalid Hexadecimal"             ## The parser expected hexadecimal characters, but was unable to parse them.
        
        elif(errcode == 126):
            msg = "Syntax Error"                    ## The parser detected a syntax error in the command.
        
        elif(errcode == 151):
            msg = "Failed ROM Check"                ## The ROM checksum failed. The firmware code is likely corrupted.
        
        elif(errcode == 152):
            msg = "Failed 24 V Out of Range"        ## The CG635 24 V power is out of range.
        
        elif(errcode == 153):
            msg = "Failed 19.44 MHz Low Rail"       ## The 19.44 MHz crystal can not tune to low enough frequencies.
        
        elif(errcode == 154):
            msg = "Failed 19.44 MHz High Rail"      ## The 19.44 MHz crystal can not tune to high enough frequencies.
        
        elif(errcode == 155):
            msg = "Failed 19.40 MHz Low Rail"       ## The 19.40 MHz crystal can not tune to low enough frequencies.
        
        elif(errcode == 156):
            msg = "Failed 19.40 MHz High Rail"      ## The 19.40 MHz crystal can not tune to high enough frequencies.
        
        elif(errcode == 157):
            msg = "Failed RF at 2 GHz"              ## The RF VCO could not tune to 2 GHz.
        
        elif(errcode == 158):
            msg = "Failed RF at 1 GHz"              ## The RF VCO could not tune to 1 GHz.
        
        elif(errcode == 159):
            msg = "Failed CMOS Low Spec."           ## The CMOS low output is out of specification.
        
        elif(errcode == 160):
            msg = "Failed CMOS High Spec."          ## The CMOS high output is out of specification.
        
        elif(errcode == 161):
            msg = "Failed Q / Q¯ Low Spec."         ## The Q / Q¯ low output is out of specification.
        
        elif(errcode == 162):
            msg = "Failed Q / Q¯ High Spec."        ## The Q / Q¯ high output is out of specification.
        
        elif(errcode == 163):
            msg = "Failed Optional Timebase"        ## An installed optional timebase is not oscillating.
        
        elif(errcode == 164):
            msg = "Failed Clock Symmetry"           ## The clock output symmetry is out of specification
        
        elif(errcode == 254):
            msg = "Too Many Errors"                 ## The error buffer is full. Subsequent errors have been dropped.
        
        return msg

    def command_string_CG635(self, str_cmd, query = "?", value = ""):   ## Debugging In Progress
        str_cmd = self.IndexCommands_CG635(str_cmd)
        command = str(str_cmd) + str(query) + str(value)
        #print("Command send: {}".format(command))
        return command

    def ControlBitsRegister(self, value_bit, istance):
        control_bit = list()
        istance_list = list()
        
        if(istance == "SESR"):
            istance_list = ['PON', 'Reserved', 'CME', 'EXE', 'DDE', 'QYE', 'Reserved', 'OPC']
        elif(istance == "SPSB"):
            istance_list = ['Reserved', 'MSS', 'ESB', 'MAV', 'Reserved', 'CESB', 'LCKB', 'INSB']
        elif(istance == "CESR"):
            istance_list = ['DCAS', 'Reserved', 'NL', 'OVFL', 'OR', 'NF', 'FE', 'PE']
        elif(istance == "INSR"):
            istance_list = ['Reserved', 'RB_COMM', 'ILGL_RESET', 'COP_RESET', 'CMF_RESET', 'EE_VERF', 'EE_PROT', 'EE_BDRY']
        elif(istance == "LCKR"):
            istance_list = ['Reserved', 'Reserved', 'PHASE_SHIFT', 'OUT_DISABLED', 'RB_UNLOCK', '10MHZ_UNLOCK', '19MHZ_UNLOCK', 'RF_UNLOCK']

        for i in range(len(value_bit)):
            control_bit_tmp = value_bit[i]
            control_bit.append(control_bit_tmp)
        
        print("Name:{}\nBit: {}".format(istance_list, control_bit))
        
        ## NB: in control_bit[0] è presente il bit 7, mentre in control_bit[7] è presente il bit 0
        
        if(istance == "SESR"):
            if(int(control_bit[7]) == 1):
                print("Bit OPC: {}\nOperation complete. All previous commands have completed".format(int(control_bit[7])))
            
            if(int(control_bit[6]) == 1):
                print("Bit Reserved: {}".format(int(control_bit[6])))
            
            if(int(control_bit[5]) == 1):
                print("Bit QYE: {}\nQuery error occurred".format(int(control_bit[5])))
            
            if(int(control_bit[4]) == 1):
                print("Bit DDE: {}\nDevice dependent error".format(int(control_bit[4])))
            
            if(int(control_bit[3]) == 1):
                print("Bit EXE: {}\nExecution error. A command failed to execute correctly because a parameter was out of range.".format(int(control_bit[3])))
            
            if(int(control_bit[2]) == 1):
                print("Bit CME: {}\nCommand error. The parser detected a syntax error".format(int(control_bit[2])))
            
            if(int(control_bit[1]) == 1):
                print("Bit Reserved: {}".format(int(control_bit[1])))
            
            if(int(control_bit[0]) == 1):
                print("Bit PON: {}\nPower on. The CG635 has been power cycled".format(int(control_bit[0])))
        
        elif(istance == "SPSB"):
            if(int(control_bit[7]) == 1):
                print("Bit INSB: {}\nAn unmasked bit in the instrument status register (INSR) has been set".format(int(control_bit[7])))
            
            if(int(control_bit[6]) == 1):
                print("Bit LCKR: {}\nAn unmasked bit in the PLL lock status register (LCKR) has been set".format(int(control_bit[6])))
            
            if(int(control_bit[5]) == 1):
                print("Bit CESB: {}\nAn unmasked bit in the communications status register (CESR) has been set".format(int(control_bit[5])))
            
            if(int(control_bit[4]) == 1):
                print("Bit Reserved: {}".format(int(control_bit[4])))
            
            if(int(control_bit[3]) == 1):
                print("Bit MAV: {}\nThe interface output buffer is non-empty".format(int(control_bit[3])))
            
            if(int(control_bit[2]) == 1):
                print("Bit ESB: {}\nAn unmasked bit in the standard event status register (*ESR) has been set".format(int(control_bit[2])))
            
            if(int(control_bit[1]) == 1):
                print("Bit MSS: {}\nMaster summary bit. Indicates that the CG635 is requesting service because an unmasked bit in this register has been set".format(int(control_bit[1])))
            
            if(int(control_bit[0]) == 1):
                print("Bit Reserved: {}".format(int(control_bit[0])))

        elif(istance == "CESR"):
            if(int(control_bit[7]) == 1):
                print("Bit PE: {}\nParity error. An RS-232 parity error was detected".format(int(control_bit[7])))
            
            if(int(control_bit[6]) == 1):
                print("Bit FE: {}\nFraming error. An RS-232 framing error was detected".format(int(control_bit[6])))
            
            if(int(control_bit[5]) == 1):
                print("Bit NF: {}\nNoise flag. An RS-232 character may have been received incorrectly due to noise on the line".format(int(control_bit[5])))
            
            if(int(control_bit[4]) == 1):
                print("Bit OR: {}\nOverrun error. An RS-232 character was received before the CG635 had time to process the previous character".format(int(control_bit[4])))
            
            if(int(control_bit[3]) == 1):
                print("Bit OVFL: {}\nThe input buffer of the CG635 overflowed".format(int(control_bit[3])))
            
            if(int(control_bit[2]) == 1):
                print("Bit NL: {}\nThe CG635 was addressed to talk under GPIB, but there were no listeners".format(int(control_bit[2])))
            
            if(int(control_bit[1]) == 1):
                print("Bit Reserved: {}".format(int(control_bit[1])))
            
            if(int(control_bit[0]) == 1):
                print("Bit DCAS: {}\nDevice clear active state. The CG635 received a device clear or selected device clear command".format(int(control_bit[0])))
        
        elif(istance == "INSR"):
            if(int(control_bit[7]) == 1):
                print("Bit EE_BDRY: {}\nEeprom write occurred outside of eeprom".format(int(control_bit[7])))
            
            if(int(control_bit[6]) == 1):
                print("Bit EE_PROT: {}\nEeprom write occurred in protected eeprom".format(int(control_bit[6])))
            
            if(int(control_bit[5]) == 1):
                print("Bit EE_VERF: {}\nEeprom write failed to verify".format(int(control_bit[5])))
            
            if(int(control_bit[4]) == 1):
                print("Bit CMF_RESET: {}\nA clock-monitor-fail reset has occurred".format(int(control_bit[4])))
            
            if(int(control_bit[3]) == 1):
                print("Bit COP_RESET: {}\nA COP-timeout reset has occurred".format(int(control_bit[3])))
            
            if(int(control_bit[2]) == 1):
                print("Bit ILGL_RESET: {}\nAn illegal-instruction reset has occurred".format(int(control_bit[2])))
            
            if(int(control_bit[1]) == 1):
                print("Bit RB_COMM: {}\nAn installed rubidium oscillator failed to communicate".format(int(control_bit[1])))
            
            if(int(control_bit[0]) == 1):
                print("Bit RF_UNLOCK: {}\nThe RF PLL has come unlocked".format(int(control_bit[0])))
        
        elif(istance == "LCKR"):
            if(int(control_bit[7]) == 1):
                print("Bit RF_UNLOCK: {}\nThe RF PLL has come unlocked".format(int(control_bit[7])))
            
            if(int(control_bit[6]) == 1):
                print("Bit 19MHZ_UNLOCK: {}\nThe 19 MHz PLL has come unlocked".format(int(control_bit[6])))
            
            if(int(control_bit[5]) == 1):
                print("Bit 10MHZ_UNLOCK: {}\nThe 10 MHz PLL has come unlocked".format(int(control_bit[5])))
            
            if(int(control_bit[4]) == 1):
                print("Bit RB_UNLOCK: {}\nThe optional rubidium oscillator has come unlocked".format(int(control_bit[4])))
            
            if(int(control_bit[3]) == 1):
                print("Bit OUT_DISABLED: {}\nThe output was disabled".format(int(control_bit[3])))
            
            if(int(control_bit[2]) == 1):
                print("Bit PHASE_SHIFT: {}\nThe output has or is scheduled to shift phase".format(int(control_bit[2])))
            
            if(int(control_bit[1]) == 1):
                print("Bit Reserved: {}".format(int(control_bit[1])))
            
            if(int(control_bit[0]) == 1):
                print("Bit Reserved: {}".format(int(control_bit[0])))

#################################################################################
### STANFORD CG635 SINGLE FUNCTIONS
###
#################################################################################
### Instrument Control Commands


    def CMOS_Output(self, cmos_out = "", value = "", query = ""):           ## Debugged - Good function
        if(cmos_out == ""):
            ## Setup default CMOS HIGH value ##
            cmos_out = 1
            if(query == ""):
                value = 3.30
                query_h = query + str(cmos_out) + ","
            elif(query == "?"):
                query_h = query + str(cmos_out)
            cmd_cmos_out = self.command_string_CG635("CMOS_OUTPUT", query_h, str(value))
            self.CG635.write(cmd_cmos_out)
            if(query == "?"):
                print("Voltage CMOS High: {}".format(self.CG635.read()))
            else:
                print("Voltage CMOS High setting: {}\n".format(value))
            
            ## Setup default CMOS LOW value ##
            cmos_out = 0
            if(query == ""):
                value = 0.00
                query_l = query + str(cmos_out) + ","
            elif(query == "?"):
                query_l = query + str(cmos_out)
            cmd_cmos_out = self.command_string_CG635("CMOS_OUTPUT", query_l, str(value))
            self.CG635.write(cmd_cmos_out)
            if(query == "?"):
                print("Voltage CMOS Low: {}".format(self.CG635.read()))
            else:
                print("Voltage CMOS Low setting: {}\n".format(value))
            
        elif(cmos_out == "LOW"):
            cmos_out = 0
            if(query == ""):
                query_l = query + str(cmos_out) + ","
                if(value == ""):
                    value = 0.00
            elif(query == "?"):
                value = ""
                query_l = query + str(cmos_out)
            cmd_cmos_out = self.command_string_CG635("CMOS_OUTPUT", query_l, str(value))
            self.CG635.write(cmd_cmos_out)
            if(query == "?"):    
                print("Voltage CMOS Low: {}".format(self.CG635.read()))
            else:
                print("Voltage CMOS Low setting: {}\n".format(value))
        
        elif(cmos_out == "HIGH"):
            cmos_out = 1
            if(query == ""):
                query_h = query + str(cmos_out) + ","
                if(value == ""):
                    value = 3.30
            elif(query == "?"):
                value = ""
                query_h = query + str(cmos_out)            
            cmd_cmos_out = self.command_string_CG635("CMOS_OUTPUT", query_h, str(value))
            self.CG635.write(cmd_cmos_out)
            if(query == "?"):    
                print("Voltage CMOS High: {}".format(self.CG635.read()))
            else:
                print("Voltage CMOS High setting: {}\n".format(value))
        self.LastError()
        print("")

    def Display(self, show = "FREQ", query = ""):                           ## Debugged - Good function
        if(query != "?"):    
            if(show == "FREQ"):
                value = 0
            elif(show == "PHASE"):
                value = 1
            elif(show == "Q_HIGH"):
                value = 2
            elif(show == "Q_LOW"):
                value = 3
            elif(show == "CMOS_HIGH"):
                value = 4
            elif(show == "CMOS_LOW"):
                value = 5
            elif(show == "FREQ_STEP"):
                value = 6
            elif(show == "PHASE_STEP"):
                value = 7
            elif(show == "Q_HIGH_STEP"):
                value = 8
            elif(show == "Q_LOW_STEP"):
                value = 9
            elif(show == "CMOS_HIGH_STEP"):
                value = 10
            elif(show == "CMOS_LOW_STEP"):
                value = 11
        else:
            value = ""
        
        cmd_show = self.command_string_CG635("DISPLAY", query, value)
        self.CG635.write(cmd_show)
        if(query != "?"):
            print("Display setting for shows you: {}".format(show))
        elif(query == "?"):
            value = int(self.CG635.read())
            if(value != -1):
                if(value == 0):
                    show = "FREQ"
                elif(value == 1):
                    show = "PHASE"
                elif(value == 2):
                    show = "Q_HIGH"
                elif(value == 3):
                    show = "Q_LOW"
                elif(value == 4):
                    show = "CMOS_HIGH"
                elif(value == 5):
                    show = "CMOS_LOW"
                elif(value == 6):
                    show = "FREQ_STEP"
                elif(value == 7):
                    show = "PHASE_STEP"
                elif(value == 8):
                    show = "Q_HIGH_STEP"
                elif(value == 9):
                    show = "Q_LOW_STEP"
                elif(value == 10):
                    show = "CMOS_HIGH_STEP"
                elif(value == 11):
                    show = "CMOS_LOW_STEP"
                print("Display shows you: {}".format(show))
            else:
                print("Display doesn't shows you to one of the standard display!\nReturned value: {}".format(value))
        self.LastError()
        print("")

    def Frequency_CG635(self, freq = 1, unit = "Hz", query = ""):           ## Debugged - Good function
        if(query != "?"):
            if(unit == "uHz"):
                freq = freq * pow(10, -6)
            elif(unit == "mHz"):
                freq = freq * pow(10, -3)
            elif(unit == "Hz"):
                freq = freq * pow(10, 0)
            elif(unit == "KHz"):
                freq = freq * pow(10, 3)
            elif(unit == "MHz"):
                freq = freq * pow(10, 6)
            elif(unit == "GHz"):
                freq = freq * pow(10, 9)
        else:
            freq = ""
        
        cmd_freq = self.command_string_CG635("FREQUENCY", query, freq)
        self.CG635.write(cmd_freq)
        
        if(query != "?"):
            #freq = format(float(freq), '.1E')                          ## Don't delete - use to back-up solution
            #print("Frequency: {}".format(freq))                        ## Don't delete - use to back-up solution
            print("Frequency: {}".format(format(float(freq), '.1E')))
        elif(query == "?"):
            #freq = format(float(self.CG635.read()), '.1E')             ## Don't delete - use to back-up solution
            #print("Frequency: {}".format(freq))                        ## Don't delete - use to back-up solution
            print("Frequency: {}".format(format(float(self.CG635.read()), '.1E')))
        self.LastError()
        print("")
        
    def Phase(self, degree = 0, query = ""):                                ## Debugged - Good function
        if(query == "?"):    
            degree = ""
        
        cmd_phase = self.command_string_CG635("PHASE", query, degree)
        self.CG635.write(cmd_phase)
        
        if(query != "?"):
            print("Phase setting: {}".format(degree))
        elif(query == "?"):
            print("Phase: {}".format(self.CG635.read()))
        self.LastError()
        print("")

    def Enable_PRBS(self, switch = "OFF", query = ""):                      ## Debugged - Good function
        if(query != "?"):
            if(switch == "ON"):
                value = 1
            elif(switch == "OFF"):
                value = 0
        elif(query == "?"):
            value = ""

        cmd_prbs = self.command_string_CG635("PRBS", query, value)
        self.CG635.write(cmd_prbs)

        if(query != "?"):
            print("PRBS setting: {}".format(switch))
        elif(query == "?"):
            value = int(self.CG635.read())
            if(value == 1):
                switch = "ON"
            elif(value == 0):
                switch = "OFF"
            print("PRBS is: {}".format(switch))
        self.LastError()
        print("")

    def Q_Output(self, q_out = "", value = "", query = ""):                 ## Debugged - Good function
        if(q_out == ""):
            ## Setup default Q/Q- HIGH value ##
            q_out = 1
            if(query == ""):
                value = 1.43
                query_h = query + str(q_out) + ","
            elif(query == "?"):
                query_h = query + str(q_out)
            cmd_q_out = self.command_string_CG635("Q_OUTPUT", query_h, str(value))
            self.CG635.write(cmd_q_out)
            if(query == "?"):    
                print("Voltage Q/Q- High: {}".format(self.CG635.read()))
            else:
                print("Voltage Q/Q- High setting: {}\n".format(value))
            
            ## Setup default Q/Q- LOW value ##
            q_out = 0
            if(query == ""):
                value = 1.07
                query_l = query + str(q_out) + ","
            elif(query == "?"):
                query_l = query + str(q_out)
            cmd_q_out = self.command_string_CG635("Q_OUTPUT", query_l, str(value))
            self.CG635.write(cmd_q_out)
            if(query == "?"):    
                print("Voltage Q/Q- Low: {}".format(self.CG635.read()))
            else:
                print("Voltage Q/Q- Low setting: {}\n".format(value))
        elif(q_out == "LOW"):
            q_out = 0
            if(query == ""):
                query_l = query + str(q_out) + ","
                if(value == ""):
                    ## Default low value ##
                    value = 1.07
            elif(query == "?"):
                value = ""
                query_l = query + str(q_out)
            cmd_q_out = self.command_string_CG635("Q_OUTPUT", query_l, str(value))
            self.CG635.write(cmd_q_out)
            if(query == "?"):    
                print("Voltage Q/Q- Low: {}".format(self.CG635.read()))
            else:
                print("Voltage Q/Q- Low setting: {}\n".format(value))
        elif(q_out == "HIGH"):
            q_out = 1
            if(query == ""):
                query_h = query + str(q_out) + ","
                if(value == ""):
                    ## Default high value ##
                    value = 1.43
            elif(query == "?"):
                value = ""
                query_h = query + str(q_out)            
            cmd_q_out = self.command_string_CG635("Q_OUTPUT", query_h, str(value))
            self.CG635.write(cmd_q_out)
            if(query == "?"):    
                print("Voltage Q/Q- High: {}".format(self.CG635.read()))
            else:
                print("Voltage Q/Q- High setting: {}\n".format(value))
        self.LastError()
        print("")

    def Relative_Phase(self, query = ""):                                   ## Debugged - Good function
        cmd_rel_phase = self.command_string_CG635("REL_PHASE", query)
        self.CG635.write(cmd_rel_phase)
        print("Relative phase setted to 0")
        self.LastError()
        print("")
        
    def RunningState_CG635(self, switch = "OFF", query = ""):               ## Debugged - Good function    
        if(query != "?"):    
            if (switch == "OFF"):
                value = 0
            elif (switch == "ON"):
                value = 1
        else:
            value = ""
        cmd_run_out = self.command_string_CG635("RUN", query, value)
        self.CG635.write(cmd_run_out)
        if(query != "?"):
            print("Running state setting: {}".format(switch))
        elif(query == "?"):
            value = int(self.CG635.read())
            if(value == 1):
                switch = "ON"
            elif(value == 0):
                switch = "OFF"
            print("Running state is: {}".format(switch))
        self.LastError()
        print("")

    def ShowDisplay(self, switch = "", query = "?"):                        ## Debugged - Good function
        if(query != "?"):    
            if(switch == "OFF"):
                value = 0
            elif(switch == "ON"):
                value = 1
        else:
            value = ""
        cmd_show_display = self.command_string_CG635("SHOW_DISPLAY", query, value)
        self.CG635.write(cmd_show_display)
        if(query != "?"):
            print("Display CG635 setting: switch {}".format(switch))
        elif(query == "?"):
            value = int(self.CG635.read())
            if(value == 1):
                switch = "ON"
            elif(value == 0):
                switch = "OFF"
            print("Display CG635 is: switch {}".format(switch))
        self.LastError()
        print("")

    def StopLevel(self, level = "LOW", query = ""):                         ## Debugged - Good function
        if(query != "?"):    
            if(level == "LOW"):
                value = 0
            elif(level == "HIGH"):
                value = 1
            elif(level == "TOGGLE"):
                value = 2
        else:
            value = ""
        cmd_stop_lvl = self.command_string_CG635("STOP_LVL", query, value)
        self.CG635.write(cmd_stop_lvl)
        if(query != "?"):
            print("Level output setting: {}".format(level))
        elif(query == "?"):
            value = int(self.CG635.read())
            if(value == 0):
                level = "LOW"
            elif(value == 1):
                level = "HIGH"
            elif(value == 2):
                level = "TOGGLE"
            print("Level output is stopped in: {}".format(level))
        self.LastError()
        print("")

    def StandardCMOS_CG635(self, std_cmos = "3V3", query = ""):             ## Debugged - Good function
        if(query != "?"):    
            if(std_cmos == "1V2"):
                value = 0
            elif(std_cmos == "1V8"):
                value = 1
            elif(std_cmos == "2V5"):
                value = 2
            elif(std_cmos == "3V3"):
                value = 3
            elif(std_cmos == "5V0"):
                value = 4
        else:
            value = ""
        cmd_std_cmos = self.command_string_CG635("std_cmos", query, value)
        self.CG635.write(cmd_std_cmos)
        if(query != "?"):
            print("Output level CMOS setting: {}".format(std_cmos))
        elif(query == "?"):
            value = int(self.CG635.read())
            #print("Value: {}\n Type: {}".format(value, type(value)))       ## Don't delete - use for debug
            if(value == 0):
                std_cmos = "1V2"
            elif(value == 1):
                std_cmos = "1V8"
            elif(value == 2):
                std_cmos = "2V5"
            elif(value == 3):
                std_cmos = "3V3"
            elif(value == 4):
                std_cmos = "5V0"
            else:
                std_cmos = "VAR"
            print("Output level CMOS is: {}".format(std_cmos))
        self.LastError()
        print("")

    def StandardQ_CG635(self, std_q = "LVDS", query = ""):                  ## Debugged - Good function
        if(query != "?"):
            if(std_q == "ECL"):
                value = 0
            elif(std_q == "+7dBm"):
                value = 1
            elif(std_q == "LVDS"):
                value = 2
            elif(std_q == "PECL3V3"):
                value = 3
            elif(std_q == "+PECL5V"):
                value = 4
        else:
            value = ""
        cmd_std_q = self.command_string_CG635("std_q", query, value)
        self.CG635.write(cmd_std_q)
        if(query != "?"):
            print("Output level Q/Q- setting: {}".format(std_q))
        elif(query == "?"):
            value = int(self.CG635.read())
            #print("Value: {}\n Type: {}".format(value, type(value)))       ## Don't delete - use for debug
            if(value == 0):
                std_q = "ECL"
            elif(value == 1):
                std_q = "+7dBm"
            elif(value == 2):
                std_q = "LVDS"
            elif(value == 3):
                std_q = "PECL3V3"
            elif(value == 4):
                std_q = "+PECL5V"
            else:
                std_q = "VAR"
            print("Output level Q/Q- is: {}".format(std_q))
        self.LastError()
        print("")

    def StepDown(self, stepdown = "FREQ", query = ""):                      ## Debugged - Good function
        if(stepdown == "FREQ" ):
            value = 0
        elif(stepdown == "PHASE"):
            value = 1
        elif(stepdown == "Q_HIGH"):
            value = 2
        elif(stepdown == "Q_LOW"):
            value = 3
        elif(stepdown == "CMOS_HIGH"):
            value = 4
        elif(stepdown == "CMOS_LOW"):
            value = 5
        
        cmd_stepdown = self.command_string_CG635("STEP_DOWN", query, value)
        self.CG635.write(cmd_stepdown)
        print("Component stepped: {}".format(stepdown))
        self.LastError()
        print("")

    def StepSize(self, component = "FREQ", stepsize = 1, unit = "Hz", query = ""):
        if(component == "FREQ"):
            value = 0
            if(unit == "Hz"):
                stepsize = stepsize * pow(10, 0)
            elif(unit == "KHz"):
                stepsize = stepsize * pow(10, 3)
            elif(unit == "MHz"):
                stepsize = stepsize * pow(10, 6)
            elif(unit == "GHz"):
                stepsize = stepsize * pow(10, 9)
            stepsize = format(float(stepsize), '.1E')
            self.UnitsDisplay_CG635("FREQSTEP", unit)
        elif(component == "PHASE"):
            unit = "°"
            value = 1
        elif(component == "Q_HIGH"):
            unit = "V"
            value = 2
        elif(component == "Q_LOW"):
            unit = "V"
            value = 3
        elif(component == "CMOS_HIGH"):
            unit = "V"
            value = 4
        elif(component == "CMOS_LOW"):
            unit = "V"
            value = 5
        
        if(query != "?"):
            query_1 = query + str(value) + ","
            cmd_stepsize = self.command_string_CG635("STEP_SIZE", query_1, stepsize)
        elif(query == "?"):
            stepsize = ""
            query_2 = query + str(value)
            cmd_stepsize = self.command_string_CG635("STEP_SIZE", query_2, stepsize)
            
        self.CG635.write(cmd_stepsize)
        
        if(query != "?"):
            value = stepsize
            if(component == "FREQ"):
                unit = "Hz"
                value = format(float(value), '.1E')
            elif(component != "FREQ"):
                value = format(float(value))
            print("Component setting: {}\nStep size setting: {}\nUnit step setting: {}".format(component, value, unit))
        elif(query == "?"):
            value = self.CG635.read()
            if(component == "FREQ"):
                unit = "Hz"
                value = format(float(value), '.1E')
            elif(component != "FREQ"):
                value = format(float(value))
            print("Component: {}\nStep size: {}\nUnit step: {}".format(component, value, unit))
        self.LastError()
        print("")

    def StepUp(self, stepup = "FREQ", query = ""):                          ## Debugged - Good function
        if(stepup == "FREQ" ):
            value = 0
        elif(stepup == "PHASE"):
            value = 1
        elif(stepup == "Q_HIGH"):
            value = 2
        elif(stepup == "Q_LOW"):
            value = 3
        elif(stepup == "CMOS_HIGH"):
            value = 4
        elif(stepup == "CMOS_LOW"):
            value = 5
        
        cmd_stepup = self.command_string_CG635("STEP_UP", query, value)
        self.CG635.write(cmd_stepup)
        print("Component stepped: {}".format(stepup))
        self.LastError()
        print("")

    def TimeBase(self):
        cmd_tmbs = self.command_string_CG635("TIMEBASE")
        self.CG635.write(cmd_tmbs)
        value = int(self.CG635.read())
        if(value == 0):
            tmbs = "Internal timebase"
        elif(value == 1):
            tmbs = "OCXO timebase"
        elif(value == 2):
            tmbs = "Rubidium timebase"
        elif(value == 3):
            tmbs = "External timebase"
        print("Timebase used: {}".format(tmbs))
        self.LastError()
        print("")

    def UnitsDisplay_CG635(self, item = "FREQ", unit = "Hz", query = ""):   ## Debugged - Good function
        if(item == "FREQ"):
            value = 0
        elif(item == "FREQ_STEP"):
            value = 1
        
        if(query != "?"):
            query_tmp = query + str(value) + ","
        
            if (unit == "Hz"):
                value = 0
            elif(unit == "KHz"):
                value = 1
            elif(unit == "MHz"):
                value = 2
            elif(unit == "GHz"):
                value = 3
        elif(query == "?"):
            query_tmp = query
            
        cmd_meas_unit = self.command_string_CG635("UNIT", query_tmp, value)
        self.CG635.write(cmd_meas_unit)

        if(query != "?"):
            print("Item setting: {}\nUnit setting: {}".format(item, unit))
        elif(query == "?"):
            value = int(self.CG635.read())
            if(value == 0):
                unit = "Hz"
            elif(value == 1):
                unit = "KHz"
            elif(value == 2):
                unit = "MHz"
            elif(value == 3):
                unit = "GHz"
            print("Item: {}\nUnit: {}".format(item, unit))
        self.LastError()
        print("")


#################################################################################
### Interface Commands
    
    def ID_Instrument(self):                                                ## Debugged - Good function
        cmd_ID = self.command_string_CG635("ID_STRING")
        self.CG635.write(cmd_ID)
        print("ID Instrument: {}".format(self.CG635.read()))
        print("")
    
    def OPComplete(self, query = "?"):                                      ## Debugged - Good function
        cmd_OPC = self.command_string_CG635("OPER_COMPLETE", query)
        self.CG635.write(cmd_OPC)
        
        if(query == "?"):
            value = int(self.CG635.read())
            print("OPC Flag: {}".format(value))
        self.LastError()
        print("")
    
    def RecallSetting(self, location = 1, query = ""):                      ## Debugged - Good function
        cmd_recall = self.command_string_CG635("RCL_INSTR_SET", query, location)
        self.CG635.write(cmd_recall)
        print("Memory location recalled: {}".format(location))
        self.LastError()
        print("")

    def ResetInstrument(self, query = ""):                                  ## Debugged - Good function
        cmd_reset = self.command_string_CG635("RST_INSTR", query)
        self.CG635.write(cmd_reset)
        print("Resets the instrument to default settings: {}".format(cmd_reset))
        self.LastError()
        print("")

    def SaveSetting(self, location = 1, query = ""):                        ## Debugged - Good function
        cmd_save = self.command_string_CG635("SAVE_SET", query, location)
        self.CG635.write(cmd_save)
        print("Setting saved in memory location: {}".format(location))
        self.LastError()
        print("")

    def SelfTest(self):                                                     ## Debugged - Good function
        cmd_TST = self.command_string_CG635("SELF_TEST")
        self.CG635.write(cmd_TST)
        time.sleep(2)
        SelfTest = int(self.CG635.read())
        print("Self Test: {}".format(SelfTest))
        if(SelfTest == 0):
            print("Self Test Successfully")
        else:
            print("Self Test Failed")
        print("")

    def WaitCMDExe(self, query = ""):                                       ## Debugged - Good function
        cmd_wait = self.command_string_CG635("WAIT_CMD_EXE", query)
        self.CG635.write(cmd_wait)
        print("Waiting Command Execution")
        print("")


#################################################################################
### Status Reporting Commands

    def ClearStatus(self, query = ""):                                      ## Debugged - Good function
        cmd_clr = self.command_string_CG635("CLEAR", query)
        self.CG635.write(cmd_clr)
        print("Clear Status")
        print("")

    def StdEventStatusEnable(self, value = "", query = "?"):                  ## Debugged - Good function
        if(query != "?"):    
            #print("Binary: {}".format(value))
            value_dec = int(value, 2)
            #print("Decimal: {}".format(value))
        if(query == "?"):
            value_dec = ""
        
        cmd_SESE = self.command_string_CG635("STD_STS_EN", query, value_dec)
        self.CG635.write(cmd_SESE)
        
        if(query != "?"):
            print("Standard Event Status Enable setting to:\t[Dec: {} ; Bin: {}]".format(value_dec, value))
        elif(query == "?"):
            value = int(self.CG635.read())
            value_bin = format(value, '08b')
            print("Standard Event Status Enable\t\t[Dec: {} ; Bin: {}]".format(value, value_bin))
            #print("Standard Event Status Enable: {}".format(self.CG635.read_raw())) ## Don't delete - Another output string view
        self.LastError()
        print("")

    def StdEventStatusRegister(self):                                       ## Debugged - Good function
        cmd_SESR = self.command_string_CG635("STD_STS_REG")
        self.CG635.write(cmd_SESR)
        value = int(self.CG635.read())
        binary_code = format(value, '08b')                                  ## ATTENZIONE, binary_code è UNA STRINGA!
        print("Standard Event Status Register:\n[{0:08b}]".format(value))
        self.ControlBitsRegister(binary_code, "SESR")
        print("")

    def PowerOnStatusClear(self, value = "", query = "?"):                  ## Debugged - Good function
        if(query != "?"):     
            print("Binary: {}".format(value))
            value = int(value, 2)
            print("Decimal: {}".format(value))
        elif(query == "?"):
            value = ""
        cmd_POSC = self.command_string_CG635("POW_ON", query, value)
        self.CG635.write(cmd_POSC)
        
        if(query == "?"):
            print("Power On Status Clear: {}".format(self.CG635.read_raw()))
        elif(query != "?"):
            print("Power On Status Clear setting to: {}".format(value))
        self.LastError()
        print("")

    def ServiceRequestEnable(self, value = "", query = "?"):                ## Debugged - Good function
        if(query != "?"):    
            #print("Binary: {}".format(value))
            value_dec = int(value, 2)
            #print("Decimal: {}".format(value))
        elif(query == "?"):
            value_dec = ""
        cmd_SRE = self.command_string_CG635("SRV_REQ_EN", query, value_dec)
        self.CG635.write(cmd_SRE)
        
        if(query == "?"):
            value = int(self.CG635.read())
            value_bin = format(value, '08b')
            print("Service Request Enable\t\t\t[Dec: {} ; Bin: {}]".format(value, value_bin))
            #print("Service Request Enable: {}".format(self.CG635.read_raw()))      ## Don't delete - Another output string view
        elif(query != "?"):
            print("Service Request Enable setting to:\t\t[Dec: {} ; Bin: {}]".format(value_dec, value))
        self.LastError()
        print("")

    def SerialPollStatusByte(self):                                         ## Debugged - Good function
        cmd_SPSB = self.command_string_CG635("SRL_STS_BYTE")
        self.CG635.write(cmd_SPSB)
        value = int(self.CG635.read())
        binary_code = format(value, '08b')
        print("Serial Poll Status Byte:\n[{0:08b}]".format(value))
        self.ControlBitsRegister(binary_code, "SPSB")
        print("")

    def CommunicationErrorStatusEnable(self, value = "", query = "?"):      ## Debugged - Good function
        if(query != "?"):    
            #print("Binary: {}".format(value))
            value_dec = int(value, 2)
            #print("Decimal: {}".format(value))
        if(query == "?"):
            value_dec = ""
        cmd_CESE = self.command_string_CG635("COMM_ERR_STS_EN", query, value_dec)
        self.CG635.write(cmd_CESE)
        
        if(query == "?"):
            value = int(self.CG635.read())
            value_bin = format(value, '08b')
            print("Communication Error Status Enable\t[Dec: {} ; Bin: {}]".format(value, value_bin))
            #print("Communication Error Status Enable: {}".format(self.CG635.read_raw()))   ## Don't delete - Another output string view
        elif(query != "?"):
            print("Communication Error Status Enable setting to:\t[Dec: {} ; Bin: {}]".format(value_dec, value))
        self.LastError()
        print("")

    def CommunicationErrorStatusRegister(self):                             ## Debugged - Good function
        cmd_CESR = self.command_string_CG635("COMM_ERR_STS_REG")
        self.CG635.write(cmd_CESR)
        value = int(self.CG635.read())
        binary_code = format(value, '08b')
        print("Communication Error Status Register:\n[{0:08b}]".format(value))
        self.ControlBitsRegister(binary_code, "CESR")
        print("")

    def InstrumentStatusEnable(self, value = "", query = "?"):              ## Debugged - Good function
        if(query != "?"):    
            #print("Binary: {}".format(value))
            value_dec = int(value, 2)
            #print("Decimal: {}".format(value_dec))
        if(query == "?"):
            value_dec = ""
        cmd_INSE = self.command_string_CG635("INSTR_STS_EN", query, value_dec)
        self.CG635.write(cmd_INSE)
        
        if(query == "?"):
            value = int(self.CG635.read())
            value_bin = format(value, '08b')
            print("Instrument Status Enable\t\t[Dec: {} ; Bin: {}]".format(value, value_bin))
            #print("Instrument Status Enable: {}".format(self.CG635.read_raw()))        ## Don't delete - Another output string view
        elif(query != "?"):
            print("Instrument Status Enable setting to:\t\t[Dec: {} ; Bin: {}]".format(value_dec, value))
        self.LastError()
        print("")

    def InstrumentStatusRegister(self):                                     ## Debugged - Good function
        cmd_INSR = self.command_string_CG635("INSTR_STS_REG")
        self.CG635.write(cmd_INSR)
        value = int(self.CG635.read())
        binary_code = format(value, '08b')
        print("Instrument Status Register:\n[{0:08b}]".format(value))
        self.ControlBitsRegister(binary_code, "INSR")
        print("")

    def PLLLockStatusEnable(self, value = "", query = "?"):                 ## Debugged - Good function
        if(query != "?"):    
            #print("Binary: {}".format(value))
            value_dec = int(value, 2)
            #print("Decimal: {}".format(value))
        if(query == "?"):
            value_dec = ""
        cmd_LCKE = self.command_string_CG635("PLL_LOCK_EN", query, value_dec)
        self.CG635.write(cmd_LCKE)
        
        if(query == "?"):
            value = int(self.CG635.read())
            value_bin = format(value, '08b')
            print("PLL Lock Status Enable\t\t\t[Dec: {} ; Bin: {}]".format(value, value_bin))
            #print("PLL Lock Status Enable: {}".format(self.CG635.read_raw()))       ## Don't delete - Another output string view
        elif(query != "?"):
            print("PLL Lock Status Enable setting to:\t\t[Dec: {} ; Bin: {}]".format(value_dec, value))
        self.LastError()
        print("")

    def PLLLockStatusRegister(self):                                        ## Debugged - Good function
        cmd_LCKR = self.command_string_CG635("PLL_LOCK_REG")
        self.CG635.write(cmd_LCKR)
        value = int(self.CG635.read())
        binary_code = format(value, '08b')
        print("PLL Lock Status Register:\n[{0:08b}]".format(value))
        self.ControlBitsRegister(binary_code, "LCKR")
        print("")

    def LastError(self):                                                    ## Debugged - Good function
        cmd_LERR = self.command_string_CG635("LAST_ERROR")
        self.CG635.write(cmd_LERR)
        error_code = int(self.CG635.read())
        print("Last error code: {}".format(error_code))
        err_msg = self.ErrorsCode_CG635(error_code)
        print("Error message: {}".format(err_msg))
        print("")


#################################################################################
### STANFORD CG635 CONCATENATION FUNCTIONS
###
    def Setup_CG635(self, frequency, unit, item, degree, CMOS, Q, switch):
        self.Frequency_CG635(frequency, unit)
        self.UnitsDisplay_CG635(item, unit)
        self.Display(item)
        #self.Phase(degree)
        self.Relative_Phase()
        self.StandardCMOS_CG635(CMOS)
        self.StandardQ_CG635(Q)
        self.RunningState_CG635(switch, "?")

    def RecallAllRegister(self):
        self.StdEventStatusRegister()
        self.CommunicationErrorStatusRegister()
        self.InstrumentStatusRegister()
        self.PLLLockStatusRegister()
        self.SerialPollStatusByte()

    def RecallAllEnable(self, value = "", query = "?"):
        self.StdEventStatusEnable(value, query)
        self.ServiceRequestEnable(value, query)
        self.CommunicationErrorStatusEnable(value, query)
        self.InstrumentStatusEnable(value, query)
        self.PLLLockStatusEnable(value, query)



#config = configparser.ConfigParser()                                       ## Recall parser
#config.read(r"cfg/autotest.ini")                                           ## Read autotest.ini file
#
#sgc635_PortVID = config.get('inst', 'sgc635_PortVID')                      ## Read Port vid for auto search port
#sgc635_PortPID = config.get('inst', 'sgc635_PortPID')                      ## Read Port pid for auto search port
#scg635_connect  = config.get('inst', 'scg635_connect')                     ## Read type of connection
#
#scg635_port = AutoSearchPort(sgc635_PortVID, sgc635_PortPID)               ## Search port where Stanford CG635 is connect
#if(scg635_port == False):                                                  ## Control USB connection
#    SockClientTCP.send_command("USB_ERROR")
#
#scg_635 = stanford_CG635(scgCG635_connect, scg635_port)                    ## Creation Stanford CG635
#if(scg_635.FlagCheck_CG635 == False or scg_635.FlagCheck_USB == False):
#    SockClientTCP.send_command("CG635_NOTOK")
#else:
#    print("Synthesized Clock Generator CG635 connected at port: {}".format(scg635_port))
#    SockClientTCP.send_command("CG635_OK")