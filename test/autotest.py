from SocketClientClass import tcp_client as SockClientTCP                       ## Added by Deiana
from Stanford_commlib import stanford_CG635, upper_string, AutoSearchPort       ## Added by Deiana
from Data_log import write_to_datalog

from ldtu_commlib import ldtu_comm
from inst_commlib import td_t3afg120
from inst_commlib import ks_e36312a
from inst_commlib import rs_sma100b
from autotest_func import *
from autotest_func_offline import *
from test_functions import *

import sys
import matplotlib.pyplot as plt
import time
import argparse
import shutil

##########################
##                      ##
                        ##
VersionNumber = "1.4"   ##
                        ##
##                      ##
##########################

"""

INFN version commands

For debug run using:
yes y | py test/autotest.py -i -p

For mass test run using:
py test/autotest.py -i -c <chip_number>

"""

parser = argparse.ArgumentParser()
parser.add_argument('-c', '--chip', type=int, default=-99,
                    help='chip number')
parser.add_argument('-i', '--infn', action='store_true',
                    help='set INFN mode (otherwise set Microtest mode)')
parser.add_argument('-p', '--plot', action='store_true',
                    help='display plots at the end of the test')
args = parser.parse_args()

INFN_mode = args.infn
chip_num = args.chip
show_plt = args.plot

if chip_num == -99:
   print("\n\nWarning: chip number not given!\n")
   ans = input('Do you want to continue anyway? (y=yes) ')
   if ans == "y":
      print("No chip number was given, test will continue with chip_num set to -1.\n")
      chip_num = -1
      chip_folder = False
   else:
      print("Test aborted since chip number was not given.\n")
      sys.exit()
else:
   print("\n\nStarting test of chip n.{}...\n".format(chip_num))
   chip_folder = True


"""
try:
   chip_num = int(sys.argv[1])
   print("\n\nStarting test of chip n.{}...\n".format(chip_num))
   chip_folder = True
except:
   print("\n\nWarning: chip number not given!\n")
   ans = input('Do you want to continue anyway? (y=yes) ')
   if ans == "y":
      print("No chip number was given, test will continue with chip_num set to -1.\n")
      chip_num = -1
      chip_folder = False
   else:
      print("Test aborted since chip number was not given.\n")
      sys.exit()
"""

##################################################################
## Load settings for autotest procedure
##
mode, inst, opt, lv, idel, daq, adc, wfg_opt, tp_opt, enob_opt, stb = load_settings(INFN_mode)
##
dtu_ver, infn, offline, verbosity, save_mode, data_red = mode
keysight_ps_ipaddr, sgc635_PortVID, sgc635_PortPID, scgCG635_connect, wfg_calibH_ipaddr, wfg_calibL_ipaddr, wfg_sin_HL_ipaddr, rs_sma100b_ipaddr = inst       ## keysight_ps_ipaddr deleted
align_dbg, do_inv_phase = opt
Iref, tol = lv
cfgfile, n_data_sin, n_data_enob, n_data_pulse, N_ini, N_pll, N_ali, N_cal, N_pul, N_ctp = daq
long_calib, bsln_mean, std_max, enob_min, adc_bsln = adc
tp_step = tp_opt[0]
enob_meas, enob_scan, enob_freq, enob_ver, enob_filt, enob_OCXO, enob_manLock, enob_clkInt, enob_adcH = enob_opt
stb_en, pll_start, pll_stop, n_cycles, time_step = stb
bsln_opt = bsln_mean, std_max
##
##################################################################

##################################################################
##
if do_inv_phase:
   inv_phase = 0b1
else:
   inv_phase = 0b0
##
##################################################################


if infn:
   print("\n")
   print("*****************************************")
   print("Running test sequence using INFN settings")
   print("*****************************************")
   print("\n")
else:
   print("\n")
   print("**********************************************")
   print("Running test sequence using Microtest settings")
   print("**********************************************")
   print("\n")


##################################################################
## Open connection socket Client
##
if not(infn):
   SockClientTCP.connect()
   SockClientTCP.send_command("START")
##
##################################################################

start_time = time.time()

##################################################################
## Stanford CG635 creation class
##
#config = configparser.ConfigParser()                                    ## Recall parser
#config.read(r"cfg/autotest.ini")                                        ## Read autotest.ini file

#sgc635_PortVID = config.get('inst', 'sgc635_PortVID')                   ## Read Port vid for auto search port
#sgc635_PortPID = config.get('inst', 'sgc635_PortPID')                   ## Read Port pid for auto search port
#scg635_connect  = config.get('inst', 'scg635_connect')                  ## Read type of connection

#scg635_port = AutoSearchPort(sgc635_PortVID, sgc635_PortPID)            ## Search port where Stanford CG635 is connect
#if(scg635_port == False):                                               ## Control USB connection
#    SockClientTCP.send_command("USB_ERROR")

#SCG635 = stanford_CG635(scgCG635_connect, scg635_port)                  ## Creation Stanford CG635
#if(SCG635.FlagCheck_CG635 == False or SCG635.FlagCheck_USB == False):
#    SockClientTCP.send_command("CG635_NOTOK")
#else:
#    print("Synthesized Clock Generator CG635 connected at port: {}".format(scg635_port))
#    SockClientTCP.send_command("CG635_OK")


#SCG635.Setup_CG635(160, "MHz", "FREQ", 0, "1V2", "LVDS", "")                            ## Stanford CG635 setting
#SCG635.RunningState_CG635("ON")
#time.sleep(0.5)
##
##################################################################



##################################################################
## Prepare folders
##
rawDir, decDir, pllDir, extraDir, save_folder = check_folders(chip_folder=chip_folder, stb_en=stb_en, chip_num=chip_num)   ## FC 20.11.2023: added save_folder
##
##################################################################


##################################################################
## Autotest 'passed steps' control variables
##
init_pass  = None
pll_pass   = None
align_pass = None
adc_pass   = None
dtu_pass   = None
ctp_pass   = None
##
##################################################################



if not(offline):

   ##################################################################
   ## Initialize classes used during test
   ##
   if not(infn):
      scg635_port = AutoSearchPort(sgc635_PortVID, sgc635_PortPID)            ## Search port where Stanford CG635 is connect
      if(scg635_port == False):                                               ## Control USB connection
         SockClientTCP.send_command("USB_ERROR")

      SCG635 = stanford_CG635(scgCG635_connect, scg635_port)                  ## Creation Stanford CG635
      if(SCG635.FlagCheck_CG635 == False or SCG635.FlagCheck_USB == False):
         SockClientTCP.send_command("CG635_NOTOK")
      else:
         print("Synthesized Clock Generator CG635 connected at port: {}".format(scg635_port))
         SockClientTCP.send_command("CG635_OK")

      SCG635.Setup_CG635(160, "MHz", "FREQ", 0, "1V2", "LVDS", "")                            ## Stanford CG635 setting
      SCG635.RunningState_CG635("ON")
      time.sleep(0.5)
      ps = -1                                   ## Avoid use of external power supply (Microtest mode)
   else:
      ps   = ks_e36312a(keysight_ps_ipaddr)     ## Power supply (INFN mode)
   
   ldtu = ldtu_comm()                        ## LiTE-DTU

   wfgH = td_t3afg120(wfg_calibH_ipaddr)     ## waveform generator connected to ADCH DC inputs
   wfgL = td_t3afg120(wfg_calibL_ipaddr)     ## waveform generator connected to ADCL DC inputs
   wfgS = td_t3afg120(wfg_sin_HL_ipaddr)     ## waveform generator connected to ADCs AC inputs
   ##
   ##################################################################

   ##################################################################################
   ##
   wfgH.set_wfg("off")     ## disable the ADCH pulse generator output
   wfgL.set_wfg("off")     ## disable the ADCL pulse generator output
   wfgS.set_wfg("off")     ## disable the AC waveform generator output
   ##
   ##################################################################################

else:
   print("\n")
   print("**************************")
   print("** OFFLINE MODE ENABLED **")
   print("**************************")
   print("\n")

do_next = True

"""
N_init = -1                                                         ## curr deleted
N_PLL, pllRegs, pllRange = -1, -1, -1
N_align, Neye, NdataAlign = -1, -1, -1
adc_h_mean, adc_h_stdv, adc_l_mean, adc_l_stdv = -1, -1, -1, -1
fitH_ampl, fitH_freq, fitH_phase, fitH_ofst, fitL_ampl, fitL_freq, fitL_phase, fitL_ofst = -1, -1, -1, -1, -1, -1, -1, -1
enob_H_fit, enob_H_fft, enob_L_fit, enob_L_fft, N_adc, N_adc_bsln, N_adc_sine = -1, -1, -1, -1, -1, -1, -1
N_cal_H, N_cal_L = -1, -1
Bsln_sub = -1
N_G10, N_G1 = -1, -1
G10_n_tot, G10_n_bsln, G10_n_G10, G10_n_G1, G1_n_tot, G1_n_bsln, G1_n_G10, G1_n_G1 = -1, -1, -1, -1, -1, -1, -1, -1
G10_max, G10_bsln, G10_ampl, G10_slope, G10_t0, G1_max, G1_bsln, G1_ampl, G1_slope, G1_t0 = -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
CATIA_slope, CATIA_const = -1, -1
N_catia = -1
"""
##################################################################
## Prepare dict for datalog .txt
##
name_list = ["TS_version", "pass", "test_time", "dtu_ver", "i2c_err", "pllRegs", "pllRange", "pllRegAuto", "pll_diff", "Idel_0", "Idel_1", "Idel_2", "Idel_3", "adc_h_mean", "adc_h_stdv", "adc_l_mean", "adc_l_stdv", "fitH_ampl", "fitH_freq", "fitH_phase", "fitH_ofst", "fitL_ampl", "fitL_freq", "fitL_phase", "fitL_ofst", "ENOB_Hfit", "ENOB_Lfit", "ENOB_Hfft", "ENOB_Lfft", "Bsln_sub", "G10_n_tot", "G10_n_bsln", "G10_n_G10", "G10_n_G1", "G1_n_tot", "G1_n_bsln", "G1_n_G10", "G1_n_G1", "G10_max", "G10_bsln", "G10_ampl", "G10_width", "G10_t0", "G1_max", "G1_bsln", "G1_ampl", "G1_width", "G1_t0",  "CATIA_slope", "CATIA_const", "N_init", "Nconf",  "N_pll",  "N_align", "Neye",   "NdataAlign", "N_adc",  "N_adc_bsln", "N_adc_sine", "N_cal_H", "N_cal_L", "N_G10",  "N_G1",   "N_catia"]
unit_list = ["none",       "none", "s",         "none",    "none",    "none",    "Ncodes",   "none",       "Ncodes",   "taps",   "taps",   "taps",   "taps",   "ADC",        "ADC",        "ADC",        "ADC",        "ADC",       "MHz",       "rad",        "ADC",       "ADC",       "MHz",       "rad",        "ADC",       "bit",       "bit",       "bit",       "bit",       "digits",   "samples",   "samples",    "samples",   "samples",  "samples",  "samples",   "samples",  "samples", "ADC",     "ADC",      "ADC",      "samples",   "sample", "ADC",    "ADC",     "ADC",     "samples",  "sample", "Nclk/reg",    "Nclk",        "Ntries", "Ntries", "Ntries", "Ntries",  "Ntries", "Ntries",     "Ntries", "Ntries",     "Ntries",     "Ntries",  "Ntries",  "Ntries", "Ntries", "Ntries"]
if infn:
   name_list.append("I_ana")
   name_list.append("I_dig")
   name_list.append("I_ext")
   name_list.append("I0_ana")
   name_list.append("I0_dig")
   name_list.append("I0_ext")
   for n in range(6):
      unit_list.append("A")
#unit_list = ["none" for i in range(len(name_list))]
datalog_dict = dict()
for k in name_list:
   if k == "TS_version":
      datalog_dict[k] = VersionNumber
   else:
      datalog_dict[k] = -1
#value_list = [VersionNumber, pllRegs,   pllRange,   adc_h_mean,   adc_h_stdv,   adc_l_mean,   adc_l_stdv,   fitH_ampl,   fitH_freq,   fitH_phase,   fitH_ofst,   fitL_ampl,   fitL_freq,   fitL_phase,   fitL_ofst,   enob_H_fit,  enob_L_fit,  enob_H_fft,  enob_L_fft,  Bsln_sub,   G10_n_tot,   G10_n_bsln,   G10_n_G10,   G10_n_G1,   G1_n_tot,   G1_n_bsln,   G1_n_G10,   G1_n_G1,   G10_max,   G10_bsln,   G10_ampl,   G10_slope,   G10_t0,   G1_max,   G1_bsln,   G1_ampl,   G1_slope,   G1_t0,   CATIA_slope,   CATIA_const,   N_init,   N_PLL,   N_align,   Neye,   NdataAlign,   N_adc,   N_adc_bsln,   N_adc_sine,   N_cal_H,   N_cal_L,   N_G10,   N_G1,   N_catia]
#datalog_dict = dict(zip(name_list,value_list))
##
##################################################################


datalog_dict["dtu_ver"] = dtu_ver


##################################################################
## Initialize LiTE-DTU (I2C, currents)
##
if offline:
   ret = ldtu_init_offline(None, None, cfgfile, N_ini, Iref, tol)
else:
   ret = ldtu_init(ps, ldtu, cfgfile, N_ini, Iref, tol, inv_phase, verbosity, dtu_ver)

__, curr, curr_pre, i2c_err, Nconf, N_init = ret

if infn:
   datalog_dict["I_ana"] = round(curr[0], 3)
   datalog_dict["I_dig"] = round(curr[1], 3)
   datalog_dict["I_ext"] = round(curr[2], 3)
   datalog_dict["I0_ana"] = round(curr_pre[0], 3)
   datalog_dict["I0_dig"] = round(curr_pre[1], 3)
   datalog_dict["I0_ext"] = round(curr_pre[2], 3)

datalog_dict["Nconf"]   = Nconf
datalog_dict["i2c_err"] = i2c_err

if ret[0] == 0:
   init_pass = True
   datalog_dict["N_init"] = N_init
else:
   do_next = False
   init_pass = False
   datalog_dict["N_init"] = N_init + 1
   datalog = write_to_datalog(list(datalog_dict.keys()), list(datalog_dict.values()), unit_list)
   if not(infn):
      SCG635.RunningState_CG635("OFF")
      SockClientTCP.send_command("FAIL")
   else:
      ps.power_off()
##
##################################################################

if do_next:
   ##################################################################
   ## PLL lock: manual and/or automatic
   ##
   if offline:
      ret = ldtu_pll_offline(pllDir, None, None, cfgfile, N_pll)
   else:
      ret = ldtu_pll(pllDir, ps, ldtu, cfgfile, N_pll, save_mode, inv_phase, verbosity, dtu_ver)

   __, pllReg, pllRange, pllRegAuto, pll_diff, N_PLL = ret
   datalog_dict["pllRange"]   = pllRange
   vcoCap_MSB  = int(pllRegAuto[0], 16)
   vcoCap_LSB  = int(pllRegAuto[1], 16)
   pllRegsAuto = (vcoCap_MSB << 8) + vcoCap_LSB
   datalog_dict["pllRegAuto"] = pllRegsAuto
   datalog_dict["pll_diff"]   = pll_diff

   if ret[0] == 0:
      pll_pass = True
      vcoCapSelect_MSB = int(pllReg[0], 16)
      vcoCapSelect_LSB = int(pllReg[1], 16)
      pllRegs = (vcoCapSelect_MSB << 8) + vcoCapSelect_LSB
      datalog_dict["pllRegs"]  = pllRegs
      datalog_dict["N_pll"] = N_PLL
   else:
      do_next = False
      pll_pass = False
      datalog_dict["pllRegs"]  = -99
      datalog_dict["N_pll"] = N_PLL + 1
      datalog = write_to_datalog(list(datalog_dict.keys()), list(datalog_dict.values()), unit_list)
      if not(infn):
         SCG635.RunningState_CG635("OFF")
         SockClientTCP.send_command("FAIL")
      else:
         ps.power_off()
   ##
   ##################################################################

if do_next:
   ##################################################################
   ## Align data links
   ##
   if offline:
      ret = ldtu_align_offline(None, None, cfgfile, pllReg, N_ali)
   else:
      ret = ldtu_align(ps, ldtu, cfgfile, pllReg, idel, align_dbg, N_ali, inv_phase, verbosity, dtu_ver)

   __, tap_list, N_align, Neye, NdataAlign = ret
   
   datalog_dict["Idel_0"] = tap_list[0]
   datalog_dict["Idel_1"] = tap_list[1]
   datalog_dict["Idel_2"] = tap_list[2]
   datalog_dict["Idel_3"] = tap_list[3]
   datalog_dict["Neye"] = Neye
   datalog_dict["NdataAlign"] = NdataAlign

   if ret[0] == 0:
      align_pass = True
      datalog_dict["N_align"] = N_align
   else:
      do_next = False
      align_pass = False
      datalog_dict["N_align"] = N_align + 1
      datalog = write_to_datalog(list(datalog_dict.keys()), list(datalog_dict.values()), unit_list)
      if not(infn):
         SCG635.RunningState_CG635("OFF")
         SockClientTCP.send_command("FAIL")
      else:
         ps.power_off()
   ##
   ##################################################################


##################################################################
## Run detailed ADC performance measurements
##
if enob_meas:
   sma100b = rs_sma100b(rs_sma100b_ipaddr)
   inst = [wfgH, wfgL, sma100b]
   options = enob_opt[1:]
   do_enob(decDir, inst, ldtu, n_data_enob, options)

   sys.exit()
##
##################################################################


if do_next:
   ##################################################################
   ## Calibrate and test ADCs
   ##
   if offline:
      ret = adc_test_offline(decDir, None, None, None, None, None, std_max, n_data_sin, enob_min, N_cal)
   else:   
      ret = adc_test(decDir, ldtu, wfgH, wfgL, wfgS, bsln_opt, n_data_sin, enob_min, N_cal, save_mode, data_red, long_calib, inv_phase, verbosity)

   __, adc_h_mean, adc_h_stdv, adc_l_mean, adc_l_stdv, fitH_param, fitL_param, enob_H_fit, enob_H_fft, enob_L_fit, enob_L_fft, N_adc, N_adc_bsln, N_adc_sine, N_cal_H, N_cal_L = ret
   datalog_dict["adc_h_mean"] = round(adc_h_mean, 3)
   datalog_dict["adc_h_stdv"] = round(adc_h_stdv, 3)
   datalog_dict["adc_l_mean"] = round(adc_l_mean, 3)
   datalog_dict["adc_l_stdv"] = round(adc_l_stdv, 3)
   datalog_dict["fitH_ampl"]  = round(fitH_param[0], 3)
   datalog_dict["fitH_freq"]  = round(fitH_param[1], 7)
   datalog_dict["fitH_phase"] = round(fitH_param[2], 3)
   datalog_dict["fitH_ofst"]  = round(fitH_param[3], 3)
   datalog_dict["fitL_ampl"]  = round(fitL_param[0], 3)
   datalog_dict["fitL_freq"]  = round(fitL_param[1], 7)
   datalog_dict["fitL_phase"] = round(fitL_param[2], 3)
   datalog_dict["fitL_ofst"]  = round(fitL_param[3], 3)
   datalog_dict["ENOB_Hfit"]  = round(enob_H_fit, 3)
   datalog_dict["ENOB_Lfit"]  = round(enob_L_fit, 3)
   datalog_dict["ENOB_Hfft"]  = round(enob_H_fft, 3)
   datalog_dict["ENOB_Lfft"]  = round(enob_L_fft, 3)
   datalog_dict["N_adc_bsln"] = N_adc_bsln
   datalog_dict["N_adc_sine"] = N_adc_sine
   datalog_dict["N_cal_H"]    = N_cal_H
   datalog_dict["N_cal_L"]    = N_cal_L

   if ret[0] == 0:
      adc_pass = True
      datalog_dict["N_adc"] = N_adc
   else:
      do_next = False
      adc_pass = False
      datalog_dict["N_adc"] = N_adc + 1
      datalog = write_to_datalog(list(datalog_dict.keys()), list(datalog_dict.values()), unit_list)
      if not(infn):
         SCG635.RunningState_CG635("OFF")
         SockClientTCP.send_command("FAIL")
      else:
         ps.power_off()
   ##
   ##################################################################

if do_next:
   ##################################################################
   ## Test DTU with pulse-like signals
   ##
   if offline:
      ret = dtu_test_offline(rawDir, None, None, None, None, n_data_pulse, N_pul)
   else:
      bslnSub = bsln_test(extraDir, ldtu, wfgH, wfgL, adc_bsln, save_mode, data_red, inv_phase, verbosity)
      datalog_dict["Bsln_sub"] = bslnSub
      
      if stb_en:
         if time_step > 0:        ## perform power-cycles pll-scan stability test
            dtu_PLL_CRC_cycles(pllDir, chip_num, ps, ldtu, wfgH, wfgL, cfgfile, bslnSub, duration=time_step, N_cycles=n_cycles, start=pll_start, stop=pll_stop, dtu_ver=dtu_ver)
         else:                   ## perform single point stability test
            dtu_PLL_CRC(pllDir, ldtu, wfgH, wfgL, bslnSub, duration=-1, chip_num=chip_num, n_cycle=0, start=pll_start, stop=pll_stop, EoE=False)
      
      ret = dtu_test(rawDir, ldtu, wfgH, wfgL, wfg_opt, n_data_pulse, N_pul, save_mode, data_red, inv_phase, verbosity)

   __, N_G10, N_G1, g10_dict, g1_dict = ret
   
   datalog_dict["G10_n_tot"]  = g10_dict["N_tot"]
   datalog_dict["G10_n_bsln"] = g10_dict["N_bsln"]
   datalog_dict["G10_n_G10"]  = g10_dict["N_G10"]
   datalog_dict["G10_n_G1"]   = g10_dict["N_G1"]
   datalog_dict["G1_n_tot"]   = g1_dict["N_tot"]
   datalog_dict["G1_n_bsln"]  = g1_dict["N_bsln"]
   datalog_dict["G1_n_G10"]   = g1_dict["N_G10"]
   datalog_dict["G1_n_G1"]    = g1_dict["N_G1"]

   datalog_dict["G10_max"]   = g10_dict["max"]
   datalog_dict["G10_bsln"]  = int(g10_dict["bsln"])
   datalog_dict["G10_ampl"]  = int(g10_dict["ampl"])
   datalog_dict["G10_width"] = round(g10_dict["width"], 3)
   datalog_dict["G10_t0"]    = int(g10_dict["t0"])
   datalog_dict["G1_max"]    = g1_dict["max"]
   datalog_dict["G1_bsln"]   = int(g1_dict["bsln"])
   datalog_dict["G1_ampl"]   = int(g1_dict["ampl"])
   datalog_dict["G1_width"]  = round(g1_dict["width"], 3)
   datalog_dict["G1_t0"]     = int(g1_dict["t0"])

   if ret[0] == 0:
      dtu_pass = True
      datalog_dict["N_G10"] = N_G10
      datalog_dict["N_G1"] = N_G1
   else:
      do_next = False
      dtu_pass = False
      datalog_dict["N_G10"] = N_G10 + 1
      datalog_dict["N_G1"] = N_G1 + 1
      datalog = write_to_datalog(list(datalog_dict.keys()), list(datalog_dict.values()), unit_list)
      if not(infn):
         SCG635.RunningState_CG635("OFF")
         SockClientTCP.send_command("FAIL")
      else:
         ps.power_off()
   ##
   ##################################################################


if do_next:
   ##################################################################
   ## Test CATIA test-pulse duration
   ##
   if offline:
      ret = 1
   else:
      ret = tp_test(extraDir, ldtu, N_ctp, save_mode, tp_step, inv_phase, verbosity)

   __, N_catia, CATIA_slope, CATIA_const = ret
   datalog_dict["CATIA_slope"] = round(CATIA_slope, 2)
   datalog_dict["CATIA_const"] = round(CATIA_const, 2)

   if ret[0] == 0:
      ctp_pass = True
      datalog_dict["N_catia"] = N_catia
   
      ldtu_cfg = ldtu.read_iic_config(2, 0, 26, quiet=True)
      ret_cfg  = check_cfg( ldtu_cfg, cfgfile, Reg10=format(int(pllReg[1], 16), '02x'), quiet=True)
      if ret_cfg != 0:
         print("LiTE-DTU configuration not OK!!!\n\n")
   
   else:
      do_next = False
      ctp_pass = False
      datalog_dict["N_catia"] = N_catia + 1
      datalog = write_to_datalog(list(datalog_dict.keys()), list(datalog_dict.values()), unit_list)
      if not(infn):
         SCG635.RunningState_CG635("OFF")
         SockClientTCP.send_command("FAIL")
      else:
         ps.power_off()
   ##
   ##################################################################

if not(offline):
   ##################################################################################
   ##
   wfgH.set_wfg("off")     ## disable the ADCH pulse generator output
   wfgL.set_wfg("off")     ## disable the ADCL pulse generator output
   wfgS.set_wfg("off")     ## disable the ADCS generator output
   ##
   ##################################################################################

   ##################################################################################
   ## Chip power OFF
   ##
   if infn:
      ps.power_off()
   ##
   ##################################################################################


end_time = time.time()
test_time = end_time - start_time
print("\n\nElapsed time = {:.2f} s".format(test_time))
datalog_dict["test_time"] = round(test_time, 2)
if do_next:
   datalog_dict["pass"] = "PASS"
else:
   datalog_dict["pass"] = "FAIL"


print_summary(chip_num, init_pass, pll_pass, align_pass, adc_pass, dtu_pass, ctp_pass)


if infn and not(stb_en) and not(enob_meas):         ## FC 20.11.2023
   write_log(chip_num, datalog_dict)

datalog = write_to_datalog(list(datalog_dict.keys()), list(datalog_dict.values()), unit_list)

if (do_next) and (verbosity > 0):
   print_summary_pass(datalog_dict)

if not(infn):
   SCG635.RunningState_CG635("OFF")
   SockClientTCP.send_command("COMPLETE")
else:
   if chip_folder:
      shutil.copy("data/results_data.txt", save_folder + "/results_data.txt")    ## FC 20.11.2023
   result_summary(do_next, round(test_time, 1))
   if show_plt:
      plt.show()



