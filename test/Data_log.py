#####First type of datalog write
import sys
from datetime import datetime
import io
import os

def write_datalog(list1, list2, list3):     ## NOT USED
    try:
        with open("data/results_data2.txt", "w") as f:
            #Writing column headers
            #f.write("TS_version,5000,none\n")
        
            #Writing column data
            for i in range(len(list1)):
                f.write("{},{},{}\n".format(list1[i], list2[i], list3[i]))
            f.close()
        return True
    except:
        return False

#####Second type of datalog write

def write_to_datalog(list1, list2, list3):
    try:
        with open("data/results_data.txt", 'w') as f:
            #f.write("TS_version,VersionNumber,none\n")
            for i in range(max(len(list1), len(list2), len(list3))):
                str1 = list1[i] if i < len(list1) else ""
                str2 = str(list2[i]) if i < len(list2) else ""
                str3 = list3[i] if i < len(list3) else ""
                f.write("{},{},{}\n".format(str1, str2, str3))
            f.close()
        return True
    except:
        return False

##Complement datalog

def add_row_datalog(list1):
    try:
        with open("data/results_data3.txt", "a") as f:        
            #Writing column data
            for i in range(len(list1)):
                f.write("{}\t\t".format(list1[i]))
            f.write("\n")
            f.close()
        return True
    except:
        return False


def ClientLog_copydata():
    now = datetime.now()
    l = []
    l.append(now.strftime("%Y"))
    l.append(now.strftime("%m"))
    l.append(now.strftime("%d"))
    l.append(now.strftime("%H%M%S"))
    filename = "-".join(l)
    filename = filename + ".txt"
    #open(filename, 'w')
    
    pathfile = "..\RunPython_vs09.bat > " + "ClientLog/" + filename
    
    os.system(pathfile)
    #sys.stdout.getvalue(pathfile)


def copia_e_salva_dati():
    # Cattura l'output della console
    output = sys.stdout.getvalue()

    # Verifica che ci siano dati da copiare e salvare
    if output:
        # Chiede all'utente il percorso del file di destinazione
        now = datetime.now()
        l = []
        l.append(now.strftime("%Y"))
        l.append(now.strftime("%m"))
        l.append(now.strftime("%d"))
        l.append(now.strftime("%H%M%S"))
        filename = "-".join(l)
        filename = filename + ".txt"
        pathfile = "ClientLog/" + filename
        
        print(pathfile)
        #percorso_file = input("Inserisci il percorso completo del file di destinazione: ")

        # Apre o crea il file di destinazione in modalità scrittura
        with open(pathfile, 'w') as file:
            # Scrive i dati sul file
            file.write(output)

        print("I dati sono stati copiati e salvati correttamente su", percorso_file)
    else:
        print("Nessun dato presente per essere copiato e salvato.")