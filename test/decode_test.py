import argparse

from analysis_lib import pll_an, sine_wave, pulse_signal


parser = argparse.ArgumentParser()
parser.add_argument("-f", "--filename", type = str, default = "/home/cms/lite-dtu-sw/py-LiTE-DTU-kuodev/microtest/data/pll/stb_test/538/chipscope_crc_3_31_2023-04-05-112731_decoded.txt",
                    help = "Input file")
args = parser.parse_args()
filename = args.filename

#with open(filename, "r") as fin:
#    lines = fin.readlines()

chip_scope = pulse_signal(filename, style="hex_pd")
chip_scope.decode_data()


#path = "/home/cms/Documents/DTU/"
#filename = "G1pulse_CRCerrors.txt"
#filename = "bsln_CRCerrors.txt"
#filename = "frameRepeated.txt"

#pul = pulse_signal(path + filename)                ## prepare for plot
#ret0 = pul.decode_data()                           ## decode raw data
