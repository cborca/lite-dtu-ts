'''
List of funcion to run autotest procedure

'''

import time
import matplotlib.pyplot as plt
import numpy as np
from test_functions import *

from analysis_lib import pll_an, sine_wave, pulse_signal








###########################################################################################################

def ldtu_init_offline(ps, ldtu, cfgfile, N_ini, Iref, tol):
   ##############################
   ## LiTE-DTU initialization
   ## 1. Power ON
   ## 2. Full reset
   ## 3. I2C configuration
   ##
   
   next = False
   for n0 in range(N_ini):
      N0 = n0 + 1

      #################################
      ## Chip power ON and configuration
      print("OFFLINE mode: chip power off")
      time.sleep(0.5)
      print("OFFLINE mode: chip power on")
      time.sleep(2)
      print("OFFLINE mode: set switches to Test mode")
      print("OFFLINE mode: full reset")
      print("OFFLINE mode: configure DTU I2C")
      print("OFFLINE mode: read currents")
      curr = offline_currents(Iref)
      ret = check_curr(curr, Iref, tol)
      print(curr)
      if ret != 0:
         print("Current Values not Ok !")
         print("OFFLINE mode: chip power off\n\n")
      else:
         print("Chip power-on successful")
         next = True
         break
         
   if not(next):
      print("OFFLINE mode: chip power off")
      print("LiTE-DTU initialization failed after {} attempts\n\n".format(N0))
      return -1
   else:
      print("LiTE-DTU initialization successful after {} attempts\n\n".format(N0))
      return curr, N0

###########################################################################################################


def ldtu_pll_offline(path, ps, ldtu, cfgfile, N_pll):
   ##############################
   ## LiTE-DTU PLL settings
   ##
   
   next = False
   for n in range(N_pll):
      N = n + 1

      ############################################
      ## Chip power ON and configuration
      print("OFFLINE mode: chip power off")
      time.sleep(0.5)
      print("OFFLINE mode: chip power on")
      time.sleep(2)
      print("OFFLINE mode: set switches to Test mode")
      print("OFFLINE mode: full reset")
      print("OFFLINE mode: configure DTU I2C")

      #################################
      ## PLL manual lock scan
      print("OFFLINE mode: PLL manual lock scan")
      fn = build_fn().split(".")[0]
      outfile = path + "PLL_scan_{}.png".format(fn)

      pll_dict = offline_pll()
      time.sleep(1)
      pll = pll_an()
      ret = pll.pll_analysis(pll_dict, outfile, show=False)

      if ret != 0:
         print("PLL scan not Ok !")
         print("OFFLINE mode: chip power off\n\n")
         time.sleep(0.5)
      else:
         print("PLL scan successful")
         pllRange = pll.get_pllRange()
         pllReg   = pll.get_pllReg()
         print("OFFLINE mode: set PLL manual lock settings")
         print("PLL lock range = {}".format(pllRange))
         print("PLL IIC registers -> 0x0f: {}, 0x10: {}".format(pllReg[0], pllReg[1]))
         next = True
         break

   if not(next):
      print("OFFLINE mode: chip power off")
      print("LiTE-DTU PLL manual lock failed after {} attempts\n\n".format(N))
      return -1
   else:
      print("LiTE-DTU PLL manual lock successful after {} attempts\n\n".format(N))
      return pllReg, pllRange, N

###########################################################################################################







###########################################################################################################

def ldtu_align_offline(ps, ldtu, cfgfile, pllReg, N_ali):
   ###################################
   ## LiTE-DTU data links alignment
   ##
   next = False
   for n1 in range(N_ali):
      N1 = n1 + 1
      #################################
      ## Eye alignment
      print("OFFLINE mode: full reset")
      print("OFFLINE mode: configure DTU I2C")
      print("OFFLINE mode: configure PLL")
      print("OFFLINE mode: Eye align")
      time.sleep(1)
      r = np.random.rand()
      if r < 0.7:
         ret = 0
      else:
         ret = -1
      if ret != 0:
         print("Eye alignment not Ok !")
         print("OFFLINE mode: chip power off\n\n")
         time.sleep(0.5)
         print("OFFLINE mode: chip power on")
         time.sleep(1)
      else:
         print("Eye alignment successful")

         #################################
         ## Data alignment
         print("OFFLINE mode: Data align")
         time.sleep(1)
         r = np.random.rand()
         if r < 0.7:
            ret = 0
         else:
            ret = -1
         if ret != 0:
            print("Data alignment not Ok !")
            print("OFFLINE mode: chip power off\n\n")
            time.sleep(0.5)
            print("OFFLINE mode: chip power on")
            time.sleep(1)
         else:
            print("Data alignment successful")
            next = True
            break

   if not(next):
      print("OFFLINE mode: chip power off")
      print("LiTE-DTU data links alignment failed after {} attempts\n\n".format(N1))
      return -1
   else:
      print("LiTE-DTU data links alignment successful after {} attempts\n\n".format(N1))
      return N1

###########################################################################################################









###########################################################################################################

def adc_cal_offline(ldtu, wfgH, wfgL):
   ##################################################################################
   ## ADC calibration
   ##
   print("OFFLINE mode: set waveform generator to calibration mode for ADCH DC inputs")
   print("OFFLINE mode: set waveform generator to calibration mode for ADCL DC inputs")
   print("OFFLINE mode: enable switches for DC inputs")
   time.sleep(1.0)
   print("OFFLINE mode: run ADC calibration")
   time.sleep(0.5)
   print("OFFLINE mode: disable switches for DC inputs")
   print("OFFLINE mode: turn off waveform generator for ADCH DC inputs")
   print("OFFLINE mode: turn off waveform generator for ADCL DC inputs")
   time.sleep(0.5)

###########################################################################################################




###########################################################################################################

def adc_test_offline(path, ps, ldtu, wfgH, wfgL, wfgS, std_max, n_data_sin, enob_min, N_cal):
   ##################################################################################
   ## Test ADC
   ##
   next = False
   for n2 in range(N_cal):
      N2 = n2 + 1

      ## Run ADC calibration
      adc_cal_offline(ldtu, wfgH, wfgL)

      ## Check ADC calibration: baseline
      print("OFFLINE mode: acquire and check baseline")
      time.sleep(1.0)
      r = np.random.rand()
      if r < 0.7:
         ret = 0
      else:
         ret = -1

      if ret != 0:
         print("ADC baseline not Ok !")
         print("Restarting ADC Calibration\n\n")
      else:
         ## Check ADC calibration: sine wave
         print("OFFLINE mode: set waveform generator to sine-wave mode for AC inputs")

         freq = "0.5"
         freq_dict = build_freq(filename="cfg/freq_table_teledyne.txt", isFilt="NO")
         f = float(get_freqParam(freq_dict, freq, "Val"))
         a = float(get_freqParam(freq_dict, freq, "Ampl"))
         print("OFFLINE mode: set frequency and amplitude of sine-wave")

         time.sleep(0.5)
         filename = build_fn()                           ## generate filename to save the data acquired
         print("OFFLINE mode: acquire sine-wave samples")
         time.sleep(1)
         #data_sin.write_decoded(path, filename)          ## save data in decoded format
         print("OFFLINE mode: disable waveform generator for AC inputs")

         print("OFFLINE mode: open default acquired data")
         path_offline = "offline/"
         r = np.random.rand()
         if r < 0.7:
            filename_offline = "default_sinewave.txt"
         elif r < 0.8:
            filename_offline = "bad_sinewave.txt"
         elif r < 0.9:
            filename_offline = "bad_sinewave2.txt"
         else:
            filename_offline = "bad_sinewave3.txt"

         sin = sine_wave(path_offline + filename_offline)   ## prepare for plot

         sin.plot_sine(zoom=True, show=False)               ## plot sine wave
         sin.save_plot(path + filename)                     ## save figure
         ret1 = sin.do_fit(f/1e6, "H")
         ret2 = sin.do_fit(f/1e6, "L")
         if (ret1 == 1) or (ret2 == 1):
            print("Bad sine waveform fit")
            print("Restarting ADC Calibration\n\n")
         else:                                           ## Use ENOB to validate ADC calibration and performance
            yfit_H, ydata_H = ret1
            yfit_L, ydata_L = ret2
            enob_H_fit = sin.enob_from_fit(yfit_H, ydata_H)
            enob_L_fit = sin.enob_from_fit(yfit_L, ydata_L)
            print("\nENOB ADCH fit = {:.2f} bit".format(enob_H_fit))
            print(  "ENOB ADCL fit = {:.2f} bit\n".format(enob_L_fit))
            retH = sin.check_enob(enob_H_fit, "ADCH", enob_min)
            retL = sin.check_enob(enob_L_fit, "ADCL", enob_min)

            if (retH != 0) or (retL != 0):
               print("ENOB not Ok !")
               print("Restarting ADC Calibration\n\n")
            else:
               enob_H_fft, enob_H2, maxFreq = sin.enob_from_fft("ADCH", is_plot=False, Ncut=1)    ## Evaluate ENOB also from FFT
               print("ENOB ADCH fft = {:.2f} bit".format(enob_H_fft))
               enob_L_fft, enob_L2, maxFreq = sin.enob_from_fft("ADCL", is_plot=False, Ncut=1)    ## Evaluate ENOB also from FFT
               print("ENOB ADCL fft = {:.2f} bit".format(enob_L_fft))
               next = True
               break

   if not(next):
      print("OFFLINE mode: chip power off")
      print("Calibration failed after {} attempts\n\n".format(N2))
      return -1
   else:
      print("Calibration successful after {} attempts\n\n".format(N2))
      return enob_H_fit, enob_H_fft, enob_L_fit, enob_L_fft, N2

###########################################################################################################







###########################################################################################################

def dtu_test_offline(path, ps, ldtu, wfgH, wfgL, n_data_pulse, N_pul):
   ##################################################################################
   ## Pulse signals test
   ##
   print("OFFLINE mode: set DTU mode and enable switches for DC inputs")
   time.sleep(0.5)
   N3a = 0
   N3b = 0

   for g in ["normal", "saturation"]:     ## repeat acquisition for 2 different gain selection
      next = False
      for n3 in range(N_pul):

         N3 = n3 + 1
         if g == "normal":
            N3a = N3
         else:
            N3b = N3

         filename = build_fn()            ## generate filename to save the data acquired

         print("OFFLINE mode: set waveform generator parameters for ADCH input pulse")
         time.sleep(0.5)

         delay = "20e-9"
         print("OFFLINE mode: set waveform generator parameters for ADCL input pulse")
         print("OFFLINE mode: set waveform generator delay for ADCL input pulse")
         time.sleep(0.5)
         print("OFFLINE mode: set waveform generators burst settings")
         time.sleep(0.5)
         print("OFFLINE mode: do burst")
         time.sleep(0.5)

         print("OFFLINE mode: acquire data and write raw file")
         time.sleep(1)
         #data_pulse.write_raw(path, filename, "bin")           ## save data in raw format

         print("OFFLINE mode: open default acquired data")
         path_offline = "offline/"
         if g == "normal":
            filename_offline = "default_g10.txt"
         else:
            r = np.random.rand()
            if r < 0.8:
               filename_offline = "default_g1.txt"
            else:
               filename_offline = "bad_g1.txt"
         pul = pulse_signal(path_offline + filename_offline)    ## prepare for plot

         ret0 = pul.decode_data()                               ## decode raw data
         ret1 = pul.check_samples(pulse_type=g)                 ## check samples (baseline, G10, G1)
         ret2 = pul.plot_pulse(show=False)                      ## plot and fit pulse signal
         pul.save_plot(outf=path+filename, gain=g)              ## save plot
         if (ret0 != 0) or (ret1 != 0) or (ret2 != 0):
            print("Pulse signal not Ok !")
            print("Restarting Pulse Generation\n\n")
         else:
            next = True
            break

      if not(next):
         print("Pulse signal test failed after {} and {} attempts\n\n".format(N3a, N3b))
         print("OFFLINE mode: chip power off")
         return -1

   if next:
      print("Pulse signal test successful after {} and {} attempts\n\n".format(N3a, N3b))
      return N3a, N3b

###########################################################################################################


















