import os, sys
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import argparse
from matplotlib.colors import ListedColormap
 
 
parser = argparse.ArgumentParser()
parser.add_argument("-c", "--correction", action = "store_true", default=False,
                    help = "Apply correction from chipscope and log info")
parser.add_argument("-n", "--chip_num", type = int, default=-1,
                    help = "chip number")
args = parser.parse_args()
correction = args.correction
chip_num = args.chip_num

#chip_num = int(sys.argv[1])


#path = 'C:/Users/Fabio/Documents/python_conda/'
#data_path = path + 'data/LiTE-DTU/stb_test/'
#out_path = path + 'outputs/LiTE-DTU/stb_test/'
#path = 'data/pll/report/'

path = 'data/pll/stb_test/'
csv_path = path + 'outputs/'
out_path = path
log_path = path + 'outputs/'
img_path = path + 'img/'

os.makedirs(img_path, exist_ok=True)  


filename = "chip{}_out.csv".format(chip_num)
ndiv = 30

df = pd.read_csv(csv_path + filename, index_col=0)
grs = df.groupby(df.N)
n = df.N.unique()
N_cycles = len(n)
print("Found {} cycles".format(N_cycles))
if np.max(n) > 30:
   print("Warning: too many cycles to plot in a single figure")
   print("Data will be displayed in plots with {} cycles each".format(ndiv))
   #n = range(30)

data_list = list()
for n_cycle in n:
    dfg = grs.get_group(n_cycle)
    df_sel = dfg.rename(columns={'crc': n_cycle})
    dfT = df_sel.T
    dfT.columns = [dfg.pll.values]
    data_list.append(dfT.loc[n_cycle])
df_crc = pd.concat(data_list, axis=1).T.sort_index()
print(df_crc)

if correction:
   print("\nApply correction form chipscope and log info...\n")
   df_fail = pd.read_csv(log_path + "chip{}_fail.csv".format(chip_num), index_col=0)
   #print(df_fail)
   grs_fail = df_fail.groupby(df_fail.N)
   k_list = grs_fail.keys
   for k in k_list:
      g = grs_fail.get_group(k)
      if g.shape[0] == 1:
         N = g.N.values[0]
         pll = g.pll.values[0]
         fail = g.failure.values[0]
         #print(n, pll, fail)
         if fail == "sync_word":
            df_crc.loc[N].loc[pll] = -3
         else:
            df_crc.loc[N].loc[pll] = -2
      else:
         print("Too many entries")
         sys.exit()
print(df_crc)



data_list = list()
for n_cycle in n:
    dfg = grs.get_group(n_cycle)
    df_sel = dfg.rename(columns={'bit': n_cycle})
    dfT = df_sel.T
    dfT.columns = [dfg.pll.values]
    data_list.append(dfT.loc[n_cycle])
df_bit = pd.concat(data_list, axis=1).T.sort_index()
#print(df_bit)


print("\n\nPreparing for plots...")
if correction:
   cmap = ListedColormap(['k','orange','c','b','r'])
else:
   cmap = ListedColormap(['c','b','r'])

fig = list()
ax = list()

for i in range( int( np.ceil(N_cycles/ndiv) ) ):
   fig.append(0)
   ax.append(0)
   start = i*ndiv
   stop  = (i+1)*ndiv
   
   if stop > N_cycles:
      stop = N_cycles

   print("Plotting block n. {}: from cycle {} to cycle {}".format(i, start, stop))
   df_crc_plot = df_crc.loc[start : stop-1]
   df_bit_plot = df_bit.loc[start : stop-1]

   fig[i], ax[i] = plt.subplots( 1, figsize=( 15, df_crc_plot.shape[0]/4 + 1 ) )
   #note = df_crc_plot
   note = df_bit_plot
   if correction:
      ax[i] = sns.heatmap(df_crc_plot, annot=note, annot_kws={"size": 7}, linewidths=.5, linecolor='lightgray', cmap=cmap, vmin=-3.5, vmax=1.5)
      colorbar = ax[i].collections[0].colorbar
      colorbar.set_ticks([-3, -2, -1, 0, 1])
      colorbar.set_ticklabels(['sync_mode','frames error', 'not locked', 'OK', 'CRC errors'])
   else:
      ax[i] = sns.heatmap(df_crc_plot, annot=note, annot_kws={"size": 7}, linewidths=.5, linecolor='lightgray', cmap=cmap, vmin=-1.5, vmax=1.5)
      colorbar = ax[i].collections[0].colorbar
      colorbar.set_ticks([-1, 0, 1])
      colorbar.set_ticklabels(['not locked', 'OK', 'CRC errors'])
   _, labels = plt.yticks()
   plt.setp(labels, rotation=0)
   ax[i].set_title("CHIP {} - CRC errors (numbers indicate bit_shift for data word alignment)".format(chip_num))
   #ax[i].set_title("CRC errors (numbers indicate bit_shift for data word alignment)")
   ax[i].set_xlabel("PLL reg")
   ax[i].set_ylabel("Cycle")
   ax[i].tick_params(axis='both', which='major', labelsize=8)
   #ax[i].tick_params(axis='both', which='minor', labelsize=8)
   fig[i].tight_layout()
   fig[i].savefig(img_path + "chip{}_{}.png".format(chip_num, i))

#plt.show()



