open_hw
connect_hw_server
open_hw_target

current_hw_device [lindex [get_hw_devices] 0]
refresh_hw_device -update_hw_probes false [lindex [get_hw_devices] 0]

## Add prob .ltx file
set_property PROBES.FILE {/home/cms/lite-dtu-sw/stb_test/debug_probes.ltx} [lindex [get_hw_devices] 0]
refresh_hw_device [lindex [get_hw_devices] 0]

display_hw_ila_data [ get_hw_ila_data hw_ila_data_1 -of_objects [get_hw_ilas -of_objects [get_hw_devices xcku040_0] -filter {CELL_NAME=~"u_ila_0"}]]

## Set trigger
set_property TRIGGER_COMPARE_VALUE eq1'b0 [get_hw_probes command_processor_0/adc_controller_0/adc0_seu_counters/LDTUv1b_SeuTeste_0/crc_error -of_objects [get_hw_ilas -of_objects [get_hw_devices xcku040_0] -filter {CELL_NAME=~"u_ila_0"}]]
## Set AND between trigger conditions
set_property CONTROL.TRIGGER_CONDITION AND [get_hw_ilas -of_objects [get_hw_devices xcku040_0] -filter {CELL_NAME=~"u_ila_0"}]

### St window depth and trigger position
set_property CONTROL.DATA_DEPTH 2048 [get_hw_ilas -of_objects [get_hw_devices xcku040_0] -filter {CELL_NAME=~"u_ila_0"}]
set_property CONTROL.TRIGGER_POSITION 1024 [get_hw_ilas -of_objects [get_hw_devices xcku040_0] -filter {CELL_NAME=~"u_ila_0"}]

### Run trigger for the ILA core
run_hw_ila [get_hw_ilas -of_objects [get_hw_devices xcku040_0] -filter {CELL_NAME=~"u_ila_0"}]

wait_on_hw_ila -timeout 0.1 [get_hw_ilas -of_objects [get_hw_devices xcku040_0] -filter {CELL_NAME=~"u_ila_0"}]


display_hw_ila_data [upload_hw_ila_data [get_hw_ilas -of_objects [get_hw_devices xcku040_0] -filter {CELL_NAME=~"u_ila_0"}]]

#after 10
## arg: full path (from python...)
set myName [lindex $argv 0]
write_hw_ila_data -csv_file [format "%s" $myName]

