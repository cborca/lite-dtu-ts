from PIL import ImageGrab
import sys
import time
from datetime import datetime


def GetScreenShot():
    
    #Acquire screen
    screenshot = ImageGrab.grab()
    
    now = datetime.now()
    l = []
    l.append(now.strftime("%Y"))
    l.append(now.strftime("%m"))
    l.append(now.strftime("%d"))
    l.append(now.strftime("%H%M%S"))
    filename = "_".join(l)
    filename = filename + ".png"
    
    #Save screen
    screenshot.save("Screen/" + filename)


GetScreenShot()