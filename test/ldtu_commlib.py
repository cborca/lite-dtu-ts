from lite_dtu_kuodev_functions import *
import sys
import numpy as np
import time

from SocketClientClass import tcp_client as SockClientTCP       ## Added by Deiana


class ldtu_rawdata:

   def __init__(self):
      self.data=[]

   def write4ser(self, dout0, dout1, dout2, dout3):
      """ write the two 16-bit words from the 4 serial lines"""
      tmpd = (dout0, dout1, dout2, dout3)
      self.data.append(tmpd)

   def get_data(self):
      return self.data


   def write_raw(self, path, filename, format="bin", switch_ADCs=False):
      """ format of data can be bin or hex"""
      f_raw = open(path + filename, "w")
      for w in self.data:
         (dout0, dout1, dout2, dout3) = w
         if format =="hex":
            f_raw.write("{:032h}, {:032h}, {:032h}, {:032h}\n".format(int(dout0,16), int(dout1,16), int(dout2,16), int(dout3,16)))
         else:
            if switch_ADCs:             ## to read ADCL and save data as ADCH 
               f_raw.write("{:032b}, {:032b}, {:032b}, {:032b}\n".format(int(dout2,16), int(dout3,16), int(dout0,16), int(dout1,16)))
            else:
               f_raw.write("{:032b}, {:032b}, {:032b}, {:032b}\n".format(int(dout0,16), int(dout1,16), int(dout2,16), int(dout3,16)))
      f_raw.close()

      
   def write_decoded(self, path, filename, switch_ADCs=False):
      f_dec = open(path + filename, "w")
      for w in self.data:
         if not(switch_ADCs):
            (dout0, dout1, dout2, dout3) = w
         else:
            (dout2, dout3, dout0, dout1) = w                             ## to read ADCL and save data as ADCH 
         f_dec.write("{}, {}\n".format(dout1[5:8], dout3[5:8]))
         f_dec.write("{}, {}\n".format(dout0[5:8], dout2[5:8]))
         f_dec.write("{}, {}\n".format(dout1[1:4], dout3[1:4]))
         f_dec.write("{}, {}\n".format(dout0[1:4], dout2[1:4]))
      f_dec.close()
 


class ldtu_comm:

   def __init__(self):
      self.local_address  = '192.168.1.1'
      self.remote_address = '192.168.1.10'
      self.remote_port    = 10000
      self.timeout        = 1.0 #seconds
      self.buffer_size    = 8192
      self.udp_socket     = None
      self.quiet          = True
      self.init()


   def init(self):
      try:
         self.udp_socket=socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
         self.udp_socket.settimeout(self.timeout)
         self.udp_socket.bind((self.local_address, self.remote_port))

         if not self.quiet:
            print("\nFPGA connection established.\n")
            SockClientTCP.send_command("FPGA_OK")

      except:
         print("Unable to establish connection to FPGA")
         SockClientTCP.send_command("FPGA_NOTOK")
         #raise SystemExit

   def send_and_listen(self,trgt, cmd, data=0, quiet=True):
      if quiet == False:
         print("Command: {} (target: {})".format(cmd, trgt))

      packet = packet_builder(trgt, cmd, data)
      self.udp_socket.sendto(packet, (self.remote_address, self.remote_port))
      reply_string = self.udp_socket.recvfrom(self.buffer_size)[0]

      if not quiet:
         print("\tTX packet: %s"   % (binascii.hexlify(packet)).decode("utf-8"))
         print("\tRX packet: %s\n" % (binascii.hexlify(reply_string)).decode("utf-8"))

      return (binascii.hexlify(reply_string)).decode("utf-8")
      
   def set_switch(self, mode="read", cmd=0):
      if mode == "write":
         if cmd == "ATM":    ## mode for alignment and sine wave signal
            atm   = 1
            vinhp = 0
            vinhn = 0
            vinlp = 0
            vinln = 0
         elif cmd == "DC":   ## mode for ADC calibration
            atm   = 1
            vinhp = 1
            vinhn = 1
            vinlp = 1
            vinln = 1
         elif cmd == "DTU":  ## mode for pulse signals
            atm   = 0
            vinhp = 1
            vinhn = 1
            vinlp = 1
            vinln = 1
         else:
            atm   = 0
            vinhp = 0
            vinhn = 0
            vinlp = 0
            vinln = 0
         self.send_and_listen("switch", "set atm", atm)
         self.send_and_listen("switch", "set vinhp", vinhp)
         self.send_and_listen("switch", "set vinhn", vinhn)
         self.send_and_listen("switch", "set vinlp", vinlp)
         self.send_and_listen("switch", "set vinln", vinln)   
      else:
         reply = self.send_and_listen("switch", "read switch status")
         reply = int(reply[-2:], 16)
         reply = '{:05b}'.format(reply)
         switch_list = ["ATM  ", "VINHP", "VINHN", "VINLP", "VINLN"]
         for i, j in enumerate(switch_list):
            print(j, "->", reply[i])
   
   

   def acquire_data(self, n_data=65536, quiet=True):
      """ acquire data and return a ldtu_rawdata object"""
      ## FILL DATA RAM

      if quiet == False:
         print("Setting data ram addrmax...")

      self.send_and_listen("adc", "Acquire ADC Data", n_data-1)
      if quiet == False:
         print("Reading data RAM...\n")

      ## For the ENOB: 65536 data words mandatory
      N = 64        ## max number of commands in one packet
      nSer = 4
      nCmd = 2 * nSer  ## number of commands in one RAM address cycle (=2 for each serializer to be read)
      n_cycles = int(n_data / (N / nCmd))

      ## Set first RAM address and then use auto-increment inside for loop
      self.send_and_listen("adc", "Set ADC RAM Address", 0, True)

      ## READING DATA:
      dtudata = ldtu_rawdata()
      for nc in range(n_cycles):

         full_pkt = bytearray()

         for j in range(N):      ## build packet with max number of commands (64)
            if j%8 != 7:
               data = j%8
            else:
               data = j%8 + 8

            packet = packet_builder("adc", "Read ADC Data RAM", data)    ## build 64-bit command
            full_pkt.extend(packet)                                      ## assemble the commands together in a single packet

         self.udp_socket.sendto(full_pkt, (self.remote_address, self.remote_port))
         reply_string = self.udp_socket.recvfrom(self.buffer_size)[0]
         rcv_pkt = (binascii.hexlify(reply_string)).decode("utf-8")
         n_bytes = int(len(rcv_pkt) / 2)
         n_words = int(n_bytes / 8)
         dout = [ rcv_pkt[i*16 : (i+1)*16][-4:] for i in range(n_words) ]

         for i in range(int(len(dout)/nCmd)):
            dout0 = dout[i*8+1] + dout[i*8]
            dout1 = dout[i*8+3] + dout[i*8+2]
            dout2 = dout[i*8+5] + dout[i*8+4]
            dout3 = dout[i*8+7] + dout[i*8+6]
            dtudata.write4ser(dout0, dout1, dout2, dout3)
            
      return dtudata



   def reset_full(self, invert_phase=0b1, version=3.0, quiet=True):
      self.send_and_listen("hardware", "adc logic reset", quiet = True)
      self.send_and_listen("adc", "set adc test mode",  0b1, quiet = True)
      self.send_and_listen("adc", "set adc sync reset", 0b1, quiet = True)
      self.send_and_listen("adc", "set adc sync reset", 0b0, quiet = True)
      addr = 0
      data = write_resync_ram_data(addr, 0xc) ## IDLE PATTERN
      self.send_and_listen("adc", "write resync ram", data, quiet = True)
      addr += 1
      data = write_resync_ram_data(addr, 0x1) ## Start
      self.send_and_listen("adc", "write resync ram", data, quiet = True)
      addr += 1
      data = write_resync_ram_data(addr, 0x3) ## I2C Reset
      self.send_and_listen("adc", "write resync ram", data, quiet = True )
      if version == 3.0:
         addr += 1
         data = write_resync_ram_data(addr, 0xf) ## PLL Reset
         self.send_and_listen("adc", "write resync ram", data, quiet = True)
      addr += 1
      data = write_resync_ram_data(addr, 0x4) ## ATU Reset
      self.send_and_listen("adc", "write resync ram", data, quiet = True)
      addr += 1
      data = write_resync_ram_data(addr, 0x8) ## ADC H Reset
      self.send_and_listen("adc", "write resync ram", data, quiet = True)
      addr += 1
      data = write_resync_ram_data(addr, 0xa) ## ADC L Reset
      self.send_and_listen("adc", "write resync ram", data, quiet = True)
      addr += 1
      data = write_resync_ram_data(addr, 0x2) ## DTU and Serializer Reset (also PLL for v2)
      self.send_and_listen("adc", "write resync ram", data, quiet = True)
      addr += 1
      data = write_resync_ram_data(addr, 0x0) ## Stop
      self.send_and_listen("adc", "write resync ram", data, quiet = True)

      ## Do Resync Sequence
      addr_max = addr
      self.send_and_listen("adc", "do resync sequence", ((invert_phase & 0b1) << 4) | addr_max, quiet = True)
      if not(quiet):
         print("LiTE-DTU reset")


   def reset_ATU(self, invert_phase=0b1, version=3.0, quiet=True):      ## reset ADC test unit
      data = write_resync_ram_data(0x0, 0xc) ## IDLE PATTERN
      self.send_and_listen("adc", "write resync ram", data, quiet = True)
      data = write_resync_ram_data(0x1, 0x1) ## Start
      self.send_and_listen("adc", "write resync ram", data, quiet = True)
      data = write_resync_ram_data(0x2, 0x4) ## ATU Reset
      self.send_and_listen("adc", "write resync ram", data, quiet = True)
      data = write_resync_ram_data(0x3, 0x0) ## Stop
      self.send_and_listen("adc", "write resync ram", data, quiet = True)
      ## Do Resync Sequence
      addr_max = 0x3
      self.send_and_listen("adc", "do resync sequence", ((invert_phase & 0b1) << 4) | addr_max, quiet = True)
      if not(quiet):
         print("ADC Test Unit reset")



   def reset_pll(self, invert_phase=0b1, version=3.0, quiet=True):
      if version != 3.0:
         print("Error: cannot reset only PLL in LiTE-DTU version {}".format(version))
      else:
         data = write_resync_ram_data(0x0, 0xc) ## IDLE PATTERN
         self.send_and_listen("adc", "write resync ram", data, quiet = True)
         data = write_resync_ram_data(0x1, 0x1) ## Start
         self.send_and_listen("adc", "write resync ram", data, quiet = True)
         data = write_resync_ram_data(0x2, 0xf) ## PLL Reset
         self.send_and_listen("adc", "write resync ram", data, quiet = True)
         data = write_resync_ram_data(0x3, 0x0) ## Stop
         self.send_and_listen("adc", "write resync ram", data, quiet = True)
         ## Do Resync Sequence
         addr_max = 0x3
         self.send_and_listen("adc", "do resync sequence", ((invert_phase & 0b1) << 4) | addr_max, quiet = True)
         if not(quiet):
            print("PLL reset")


   def reset_dtu(self, invert_phase=0b1, version=3.0, quiet=True):
      if version != 3.0:
         print("Error: cannot reset only DTU and Serializer in LiTE-DTU version {}".format(version))
      else:
         data = write_resync_ram_data(0x0, 0xc) ## IDLE PATTERN
         self.send_and_listen("adc", "write resync ram", data, quiet = True)
         data = write_resync_ram_data(0x1, 0x1) ## Start
         self.send_and_listen("adc", "write resync ram", data, quiet = True)
         data = write_resync_ram_data(0x2, 0x2) ## PLL Reset
         self.send_and_listen("adc", "write resync ram", data, quiet = True)
         data = write_resync_ram_data(0x3, 0x0) ## Stop
         self.send_and_listen("adc", "write resync ram", data, quiet = True)
         ## Do Resync Sequence
         addr_max = 0x3
         self.send_and_listen("adc", "do resync sequence", ((invert_phase & 0b1) << 4) | addr_max, quiet = True)
         if not(quiet):
            print("DTU and Serializer reset")

      
   def reset_adcH(self, invert_phase=0b1, quiet=True):
      data = write_resync_ram_data(0x0, 0xc) ## IDLE PATTERN
      self.send_and_listen("adc", "write resync ram", data, quiet = True)
      data = write_resync_ram_data(0x1, 0x1) ## Start
      self.send_and_listen("adc", "write resync ram", data, quiet = True)
      data = write_resync_ram_data(0x2, 0x8) ## ADC H Reset
      self.send_and_listen("adc", "write resync ram", data, quiet = True)
      data = write_resync_ram_data(0x3, 0x0) ## Stop
      self.send_and_listen("adc", "write resync ram", data, quiet = True)
      ## Do Resync Sequence
      addr_max = 0x3
      self.send_and_listen("adc", "do resync sequence", ((invert_phase & 0b1) << 4) | addr_max, quiet = True)
      if not(quiet):
         print("ADC_H reset")


   def reset_adcL(self, invert_phase=0b1, quiet=True):
      data = write_resync_ram_data(0x0, 0xc) ## IDLE PATTERN
      self.send_and_listen("adc", "write resync ram", data, quiet = True)
      data = write_resync_ram_data(0x1, 0x1) ## Start
      self.send_and_listen("adc", "write resync ram", data, quiet = True)
      data = write_resync_ram_data(0x2, 0xa) ## ADC L Reset
      self.send_and_listen("adc", "write resync ram", data, quiet = True)
      data = write_resync_ram_data(0x3, 0x0) ## Stop
      self.send_and_listen("adc", "write resync ram", data, quiet = True)
      ## Do Resync Sequence
      addr_max = 0x3
      self.send_and_listen("adc", "do resync sequence", ((invert_phase & 0b1) << 4) | addr_max, quiet = True)
      if not(quiet):
         print("ADC_L reset")


   def dtu_mode(self, invert_phase=0b1, quiet=False):
      ## Write Resync RAM
      data = write_resync_ram_data(0x0, 0xc) ## IDLE
      self.send_and_listen("adc", "write resync ram", data)
      data = write_resync_ram_data(0x1, 0x1) ## Start
      self.send_and_listen("adc", "write resync ram", data)
      data = write_resync_ram_data(0x2, 0x6) ## DTU mode
      self.send_and_listen("adc", "write resync ram", data)
      data = write_resync_ram_data(0x3, 0x0) ## Stop
      self.send_and_listen("adc", "write resync ram", data, quiet = True)
      ## Do Resync Sequence
      addr_max = 0x3
      self.send_and_listen("adc", "do resync sequence", ((invert_phase & 0b1) << 4) | addr_max)
      if not(quiet):
         print("DTU mode enabled")


   def sync_mode(self, invert_phase=0b1):
      ## Write Resync RAM
      data = write_resync_ram_data(0x0, 0xc) ## IDLE
      self.send_and_listen("adc", "write resync ram", data)
      data = write_resync_ram_data(0x1, 0x1) ## Start
      self.send_and_listen("adc", "write resync ram", data)
      data = write_resync_ram_data(0x2, 0x5) ## Sync mode
      self.send_and_listen("adc", "write resync ram", data)
      data = write_resync_ram_data(0x3, 0x0) ## Stop
      self.send_and_listen("adc", "write resync ram", data, quiet = True)
      ## Do Resync Sequence
      addr_max = 0x3
      self.send_and_listen("adc", "do resync sequence", ((invert_phase & 0b1) << 4) | addr_max)
      print("Sync mode enabled")


   def adc_calib(self, invert_phase=0b1, quiet=True):
      ## Write Resync RAM
      data = write_resync_ram_data(0x0, 0xc) ## IDLE
      self.send_and_listen("adc", "write resync ram", data)
      data = write_resync_ram_data(0x1, 0x1) ## Start
      self.send_and_listen("adc", "write resync ram", data)
      data = write_resync_ram_data(0x2, 0x9) ## ADC H Calibration
      self.send_and_listen("adc", "write resync ram", data)
      data = write_resync_ram_data(0x3, 0xb) ## ADC L Calibration
      self.send_and_listen("adc", "write resync ram", data)
      data = write_resync_ram_data(0x4, 0x0) ## Stop
      self.send_and_listen("adc", "write resync ram", data)
      ## Do Resync Sequence
      addr_max = 0x4
      self.send_and_listen("adc", "do resync sequence", ((invert_phase & 0b1) << 4) | addr_max)
      if not(quiet):
         print("\nADC calibration")

   def adcH_calib(self, invert_phase=0b1, quiet=True):
      ## Write Resync RAM
      data = write_resync_ram_data(0x0, 0xc) ## IDLE
      self.send_and_listen("adc", "write resync ram", data)
      data = write_resync_ram_data(0x1, 0x1) ## Start
      self.send_and_listen("adc", "write resync ram", data)
      data = write_resync_ram_data(0x2, 0x9) ## ADC H Calibration
      self.send_and_listen("adc", "write resync ram", data)
      data = write_resync_ram_data(0x3, 0x0) ## Stop
      self.send_and_listen("adc", "write resync ram", data)
      ## Do Resync Sequence
      addr_max = 0x3
      self.send_and_listen("adc", "do resync sequence", ((invert_phase & 0b1) << 4) | addr_max)
      if not(quiet):
         print("\nADC_H calibration")

   def adcL_calib(self, invert_phase=0b1, quiet=True):
      ## Write Resync RAM
      data = write_resync_ram_data(0x0, 0xc) ## IDLE
      self.send_and_listen("adc", "write resync ram", data)
      data = write_resync_ram_data(0x1, 0x1) ## Start
      self.send_and_listen("adc", "write resync ram", data)
      data = write_resync_ram_data(0x2, 0xb) ## ADC L Calibration
      self.send_and_listen("adc", "write resync ram", data)
      data = write_resync_ram_data(0x3, 0x0) ## Stop
      self.send_and_listen("adc", "write resync ram", data)
      ## Do Resync Sequence
      addr_max = 0x3
      self.send_and_listen("adc", "do resync sequence", ((invert_phase & 0b1) << 4) | addr_max)
      if not(quiet):
         print("\nADC_L calibration")


   def adc_bsln(self, quiet=False):
      adc_h = list()
      adc_l = list()
      data = self.acquire_data(1024)     ## acquire data
      for w in data.get_data():
         (dout0, dout1, dout2, dout3) = w
         adc_h.append(int(dout1[5:8],16))
         adc_h.append(int(dout0[5:8],16))
         adc_h.append(int(dout1[1:4],16))
         adc_h.append(int(dout0[1:4],16))
         adc_l.append(int(dout3[5:8],16))
         adc_l.append(int(dout2[5:8],16))
         adc_l.append(int(dout3[1:4],16))
         adc_l.append(int(dout2[1:4],16))

      adc_h_mean = np.mean(adc_h)
      adc_h_stdv = np.std(adc_h)
      adc_l_mean = np.mean(adc_l)
      adc_l_stdv = np.std(adc_l)
      if not(quiet):
         print("\nBaseline after calibration:")
         print("- ADCH mean -> {:.3f}".format(adc_h_mean))
         print("- ADCH stdv -> {:.3f}".format(adc_h_stdv))
         print("- ADCL mean -> {:.3f}".format(adc_l_mean))
         print("- ADCL stdv -> {:.3f}".format(adc_l_stdv))

      return adc_h_mean, adc_h_stdv, adc_l_mean, adc_l_stdv



   def cu_config(self, cfgfile, quiet=True):
      with open(cfgfile, "r") as f:
         f.seek(0)
         l = len(f.readlines())
         f.seek(0)
         if not(quiet):
            print("Using cfg ", cfgfile)
         for i in range(l):
            value = f.readline().split()[1]
            value = int(value.strip(), 16)
            data = write_iic_spi_ram_data(addra = i, din = value)
            if not ( (i == 9 or i == 10) and l != 19):
                self.send_and_listen("hardware", "Set IIC SPI data", data, quiet = True)
         #f.close()
      ## Set IIC SPI Ram Maximum Address
      data = write_iic_spi_addrmax(mode = "w", addr_max = 0x12)
      self.send_and_listen("hardware", "Set IIC SPI data", data, quiet = True)
      ## DO write operation
      data = write_do_iic_spi_operation_data(invert_dso = 0, disable_dt = 0, mode = "w", addr = 0x02, reg = 0x00)
      reply = self.send_and_listen("hardware", "Do IIC SPI operation", data, quiet = True)
      ## Get acknowledgement
      if bin(int(reply, 16))[-14:-11] != '111':
         print("Read not acknowledged.")
         #raise Exception("Read not acknowledged.")
         #SockClientTCP.send_command("ACK")


   def write_iic_register(self, reg = 0x0f, value = 0x00, addr = 0x02):
      ## Write IIC RAM
      data = write_iic_spi_ram_data(addra = 0, din = value)
      self.send_and_listen("hardware", "Set IIC SPI data", data, quiet = True)
      ## Set maximum reading addr from IIC write RAM
      data = write_iic_spi_addrmax(mode = "w", addr_max = 0)
      self.send_and_listen("hardware", "Set IIC SPI data", data, quiet = True)
      ## Do IIC operation; fill read RAM
      ## addr -> device address (0x2->CU); reg -> starting register
      data = write_do_iic_spi_operation_data(mode = "w", addr = addr, reg = reg)
      reply = self.send_and_listen("hardware", "Do IIC SPI operation", data, quiet = True)
      ## If the write operation is not acknowledged -> return 1
      if bin(int(reply, 16))[-14:-11] != '111':
         return 1
      else:
         return 0
         
   def read_iic_config(self, device, starting_register, n_registers, quiet=True):
      max_reg = starting_register + n_registers
      if device == 1 or device == 0:
         if max_reg > 90 :
            raise Exception("Too many registers to read or invalid start")
      else:
         if max_reg > 0x1a:
            raise Exception("Too many registers to read or invalid start")
      data = write_iic_spi_addrmax(mode = "r", addr_max = n_registers)
      self.send_and_listen("hardware", "Set IIC SPI data", data, quiet = True)
      data = write_do_iic_spi_operation_data(addr = device, mode = "r", reg = starting_register)
      reply = self.send_and_listen("hardware", "Do IIC SPI operation", data, quiet = True)
      reg_list = []
      reg_save = []
      for i in range(starting_register, max_reg):
         reply = self.send_and_listen("hardware", "Read IIC SPI Read RAM", i, quiet = True)
         value = int(reply[-2:], 16)
         reg_list.append("Reg 0x{:02x} -> 0x{:02x}".format(i, value))
         reg_save.append("{:02x} {:02x}".format(i, value))
      ## Print Register Configuration on screen 
      if not(quiet):
         print("\n IIC Registers Configuration:")
         for i in reg_save:
            print(i)

      return reg_save


   def write_iic_config(self, device, cfg_filename):
      f = open(cfg_filename, "r")
      f.seek(0)
      lines = list(enumerate(f))[-1][0]
      f.close()
      print("LINES: {}".format(lines))
      with open(cfg_filename, "r") as f:
         f.seek(0)
         print("\nCU Configuration Written:")
         for i in range(lines+1):
            value = f.readline().split()[1]
            value = int(value.strip(), 16)
            print("reg {:02x} {:02x}".format(i,value), end="")
            data = write_iic_spi_ram_data(addra = i, din = value)
            if (device == 2) and ((i == 9) or (i == 10)):
               print(" -- {:08x} R.O.".format(data))
            else:
               print(" -- {:08x}".format(data))
               self.send_and_listen("hardware", "Set IIC SPI data", data, quiet = True)

      ## Set IIC SPI Ram Maximum Address
      data = write_iic_spi_addrmax(mode = "w", addr_max = lines)
      self.send_and_listen("hardware", "Set IIC SPI data", data, quiet = True)
      ## DO write operation
      data = write_do_iic_spi_operation_data(invert_dso = 0, disable_dt = 0, mode = "w", addr = device, reg = 0x00)
      reply = self.send_and_listen("hardware", "Do IIC SPI operation", data, quiet = True)





   def vcoCap_scan(self):     ## function to perform VCO cap values for PLL manual lock scan
      out_dict = {}
      cnt_6 = 0               ## 6-bit Digital Thermometer Code (MSB)
      cnt_3 = 0               ## 3-bit Binary Code (LSB)
      self.send_and_listen("adc", "reset adc clk counters")
      for ith in range(7):          ## 7 cycles to test all 6-bit Digital Thermometer codes
         for ibin in range(8):      ## 8 cycles to test all 3-bit Binary codes
            vcoCapSelect = "{:06b}{:03b}".format(cnt_6, cnt_3)    ## 'MSB' + 'LSB'
            reg0f = "{:07b}".format(0) + vcoCapSelect[0]          ## Register 0f -> '0000000' + MSB of vcoCapSelect
            reg10 = vcoCapSelect[1:]                              ## Register 10 -> remaining bits of vcoCapSelect
            ret = self.write_iic_register(0x0f, int(reg0f, 2))    ## IIC WRITE THE NEW VALUES
            ret = self.write_iic_register(0x10, int(reg10, 2))    ## IIC WRITE THE NEW VALUES
            self.send_and_listen("adc", "reset adc clk counters")                         ## reset FPGA counters
            r1 = self.send_and_listen("adc", "readadcplllockcounter", 0, quiet = True)    ## read PLL lock counter (16 LSB)
            r2 = self.send_and_listen("adc", "readadcplllockcounter", 1, quiet = True)    ## read PLL lock counter (16 MSB)
            pll_cnt = r2[-4:] + r1[-4:]
            out_dict[reg0f[-1] + reg10] = int(pll_cnt, 16)
            cnt_3 = cnt_3 + 1
         cnt_3 = 0
         cnt_6 = cnt_6 + 2**ith
      return out_dict
      
   def do_pll_scan(self):
      pll_dict = self.vcoCap_scan()
      return pll_dict

      
   def setManLock(self, version=3.0, quiet=True):             ## addr=11 val=3e [PLL control register 0]
      addr = 0x11
      if version>2.1:
         val  = 0x7e
      else:
         val  = 0x3e
      if not(quiet):
         print("\nSetting PLL manual lock...")      
      self.write_iic_register(addr, val)
      if not(quiet):
         print("Reg. {} set to {}".format(hex(addr), hex(val)))

   def setAutoLock(self, version=3.0, quiet=True):             ## addr=11 val=32 [PLL control register 0]
      addr = 0x11
      if version>2.1:
         val  = 0x72
      else:
         val  = 0x32
      if not(quiet):
         print("\nSetting PLL auto lock...")      
      self.write_iic_register(addr, val)
      if not(quiet):
         print("Reg. {} set to {}".format(hex(addr), hex(val)))


   def writePLLreg(self, pllReg, quiet=True):
      if not(quiet):
         print("Writing PLL IIC registers...")      
      self.write_iic_register(0x0f, int(pllReg[0], 16))
      self.write_iic_register(0x10, int(pllReg[1], 16))
      if not(quiet):
         print("Reg. 0x0f set to {}".format(pllReg[0]))
         print("Reg. 0x10 set to {}".format(pllReg[1]))
      

   def writeADC_ctrlReg(self, adcH=False, adcL=False, quiet=True):
      dtu_addr = 0x02
      reg_addr = 0x01
      
      ## Take into account gain mode sel: 8, 16 or fixed (according to last 2 MSB --> 0xc0)
      regs = self.read_iic_config(device=dtu_addr, starting_register=reg_addr-1, n_registers=2, quiet=True)
      adc_reg = int(regs[1].split(" ")[-1], 16)
      mask = 0xc0
      gain_sel = adc_reg & mask
      #print(adc_reg, mask, gain_sel)
      ##
      if not(quiet):
         print("\nWriting ADCs control register...")      
      if adcH and adcL:
         reg_value = gain_sel + 0x03
         txt = "ADCH enabled, ADCL enabled"
      elif adcH:
         reg_value = gain_sel + 0x02
         txt = "ADCH enabled, ADCL disabled"
      elif adcL:
         reg_value = gain_sel + 0x01
         txt = "ADCH disabled, ADCL enabled"
      else:
         reg_value = gain_sel + 0x00
         txt = "ADCH disabled, ADCL disabled"

      self.write_iic_register(reg=reg_addr, value=reg_value, addr = dtu_addr)

      if not(quiet):
         print("Reg. {} set to {}".format(hex(reg_addr), hex(reg_value)))
         print(txt)


   def writeBslnreg(self, adc, bslnReg, quiet=True):
      if not(quiet):
         print("\nWriting ADC {} baseline subtraction IIC register...".format(adc))      
      if adc == "H":
         self.write_iic_register(0x05, int(bslnReg, 16))
         if not(quiet):
            print("Reg. 0x05 set to {}".format(bslnReg))
      elif adc == "L":
         self.write_iic_register(0x06, int(bslnReg, 16))
         if not(quiet):
            print("Reg. 0x06 set to {}".format(bslnReg))
      else:
         print("Error: no ADC selected!!!\n\n")
      

   def writeAdcSatReg(self, bslnReg=-99, quiet=True):
      addr = 0x08
      #reg = 255 - int(bslnReg, 16)
      reg = 0x07
      if not(quiet):
         print("\nWriting ADC saturation value register...")      
      self.write_iic_register(addr, reg)
      if not(quiet):
         print("Reg. {} set to {}".format(hex(addr), hex(reg)))


   def writeAdcDivReg(self, div):
      print("Writing PLL IIC registers...")
      if div == 2:
         r = "0x4f"
      elif div == 4:
         r = "0x8f"
      else:
         r = "0x0f"
      self.write_iic_register(0x08, int(r, 16))
      print("Reg. 0x08 set to {}".format(r))

      
   def eye_align_dbg(self, dout_ref=0, eye_scan_step=5):
       ## Read IDELAYE3 cntvalue initial
       reply = self.send_and_listen("adc", "Read ADC Idelays Cntvalue", 0, quiet = True)
       ## Convert to binary to select last 9 bits
       cntvalue0 = bin(int(reply, 16))
       cntvalue0 = int(cntvalue0[-9:], 2)
       header = [['0011', '1001'],  ## ...3xxx9xxx...
                 ['0110', '1100'],  ## ...6xxxcxxx...
                 ['1100', '0110'],  ## ...cxxx6xxx...
                 ['1001', '0011']]  ## ...9xxx3xxx...
       markers_found_list = []
       n_repetitions = 16

       ## Cycle to align dout0:
       ##  - change cntvalue,
       ##  - acquire data n times -> count how many times alignment marker is found
       for cntval in range(0, 512, eye_scan_step):
          ## Change idelay cntvalue
          data = write_adc_idelays_cntvalue_data(cntval, dout_ref)
          print("\nCntvalue set to {:03d}. Markers found: ".format(cntval), end = "")
          self.send_and_listen("adc", "Write ADC Idelays cntvalue", data, quiet = True)
          markers_found = 0
          for n in range(n_repetitions):
             data_obj = self.acquire_data(8)
             data_l = data_obj.get_data()
             data_list = []
             for i in range(len(data_l)):
                (dout0, dout1, dout2, dout3) = data_l[i]
                temp = [ "{:032b}".format(int(dout0,16)), 
                         "{:032b}".format(int(dout1,16)),
                         "{:032b}".format(int(dout2,16)), 
                         "{:032b}".format(int(dout3,16))]
                data_list.append(temp)

             ## Find alignment on DOUT0
             data_conc = data_list[0][dout_ref] + data_list[1][dout_ref]
             for j in range(32):
                 h0 = data_conc[j:j+4]
                 h1 = data_conc[j+16:j+20]
                 if (h0 == header[dout_ref][0] and h1 == header[dout_ref][1]):
                     markers_found += 1
                     print("*", end = "")
                     break
                     
          markers_found_list.append([cntval, markers_found])


       ## Find the cntvalue to apply by selecting the one surrounded by the maximum number of successfull repetition
       ## Tuning the steps to consider to avoid boundaries where the width of the plateu cannot be evaluated
       ##  -> the length of the plateau is ~30% of the full range convered by the taps
       ##  -> we would like to avoid using a ref ponit that falls in the first/last third
       step_min = int(len(markers_found_list)*0.30)
       step_max = int(len(markers_found_list)*0.70)
       width_list = []
       for i in range(step_min, step_max):
          j = 0
          while markers_found_list[i-j][1] == n_repetitions and markers_found_list[i+j][1] == n_repetitions:
             j += 1
          width_list.append([markers_found_list[i][0], j])

       ## transpose the list
       width_list = list(zip(*width_list))
       max_id = width_list[1].index(max(width_list[1]))
       cntvalue_to_write = width_list[0][max_id] ##FIXME


       ## Uncomment the lines below to print maximum widths
       ## they are used to choose the optimal input delay values
       print("\n\nWidths found:")
       for i in range(len(width_list[1])):
          print("{:03d}=>{:02d} |{}".format(width_list[0][i], width_list[1][i], width_list[1][i]*"+"))

       print("\n")
       ## Set value found on all three input lines
       for i in range(4):
          if i == 3:
             ## Extra tap for dout3 (delayed)
             extra_tap = 20
             data = write_adc_idelays_cntvalue_data(cntvalue_to_write + extra_tap, i)
             self.send_and_listen("adc", "Write ADC Idelays cntvalue", data, quiet = True)
             print("- Cntvalue for DOUT{} set to {:03d}".format(i, cntvalue_to_write + extra_tap))
          else:
             data = write_adc_idelays_cntvalue_data(cntvalue_to_write , i)
             self.send_and_listen("adc", "Write ADC Idelays cntvalue", data, quiet = True)
             print("- Cntvalue for DOUT{} set to {:03d}".format(i, cntvalue_to_write))

       ## CHECK THE CNT VALUE WORKS FOR DOUT0,1,2,3
       print("\n")
       ret = 0
       for adcn in range(0,4):
          markers_found = 0
          for n in range(n_repetitions):
             data_obj = self.acquire_data(8)
             data_l = data_obj.get_data()
             data_list = []
             for i in range(len(data_l)):
                (dout0, dout1, dout2, dout3) = data_l[i]
                temp = ["{:032b}".format(int(dout0,16)),
                        "{:032b}".format(int(dout1,16)),
                        "{:032b}".format(int(dout2,16)),
                        "{:032b}".format(int(dout3,16)) ]
                data_list.append(temp)
              
              ## Find alignment on DOUT0
             data_conc = data_list[0][adcn] + data_list[1][adcn]
             for j in range(32):
                h0 = data_conc[j:j+4]
                h1 = data_conc[j+16:j+20]
                if (h0 == header[adcn][0] and h1 == header[adcn][1]):
                   markers_found += 1
                   print("*", end = "")
          if markers_found == n_repetitions:
             print(" DOUT{} -> ok".format(adcn))
          else:
             print(" DOUT{} -> **WARNING**".format(adcn))
             ret = 1

       return ret


   def eye_align(self, idel=[0,0,0,0], quiet=False):
       ## Read IDELAYE3 cntvalue initial
       reply = self.send_and_listen("adc", "Read ADC Idelays Cntvalue", 0, quiet = True)
       #if (self.FlagCheck_ICS == False) or (self.FlagCheck_ITS == False):     ## Added from F. Deiana
          #return
       
       ## Convert to binary to select last 9 bits
       cntvalue0 = bin(int(reply, 16))
       cntvalue0 = int(cntvalue0[-9:], 2)
       header = [['0011', '1001'],  ## ...3xxx9xxx...
                 ['0110', '1100'],  ## ...6xxxcxxx...
                 ['1100', '0110'],  ## ...cxxx6xxx...
                 ['1001', '0011']]  ## ...9xxx3xxx...
       markers_found_list = []
       n_repetitions = 16

       ## Cycle to align dout0:
       ##  - change cntvalue,
       ##  - acquire data n times -> count how many times alignment marker is found
       for cntval in range(0, 512, 5):
          ## Change idelay cntvalue
          data = write_adc_idelays_cntvalue_data(cntval, 0)
          #print("\nCntvalue set to {:03d}. Markers found: ".format(cntval), end = "")
          self.send_and_listen("adc", "Write ADC Idelays cntvalue", data, quiet = True)
          #if (self.FlagCheck_ICS == False) or (self.FlagCheck_ITS == False):     ## Added from F. Deiana
             #return

          markers_found = 0
          for n in range(n_repetitions):
             data_obj = self.acquire_data(8)
             #if (self.FlagCheck_ICS == False) or (self.FlagCheck_ITS == False):     ## Added from F. Deiana
                #return

             data_l = data_obj.get_data()
             data_list = []
             for i in range(len(data_l)):
                (dout0, dout1, dout2, dout3) = data_l[i]
                temp = [ "{:032b}".format(int(dout0,16)) , "{:032b}".format(int(dout1,16)) , "{:032b}".format(int(dout2,16)) , "{:032b}".format(int(dout3,16)) ]
                data_list.append(temp)

             ## Find alignment on DOUT0
             data_conc = data_list[0][0] + data_list[1][0]
             #print(data_conc)
             for j in range(32):
                 h0 = data_conc[j:j+4]
                 h1 = data_conc[j+16:j+20]
                 if (h0 == header[0][0] and h1 == header[0][1]):
                    markers_found += 1
                    break

          markers_found_list.append([cntval, markers_found])


       ## Find the cntvalue to apply by selecting the one surrounded by the maximum number of successfull repetition
       width_list =[]
       range_min = int(len(markers_found_list)*0.30)
       range_max = int(len(markers_found_list)*0.70)
       for i in range(range_min, range_max):
          j = 0
          while markers_found_list[i-j][1] == n_repetitions and markers_found_list[i+j][1] == n_repetitions:
             j += 1
          width_list.append([markers_found_list[i][0], j])

       ## transpose the list
       width_list = list(zip(*width_list))
       max_id = width_list[1].index(max(width_list[1]))
       cntvalue_to_write = width_list[0][max_id] ##FIXME

       ## Set delays on the 4 lanes
       extra_tap_list = idel
       tap_list = list()
       for i in range(4):
          tap = cntvalue_to_write + extra_tap_list[i]
          tap_list.append(tap)
          data = write_adc_idelays_cntvalue_data(tap, i)
          self.send_and_listen("adc", "Write ADC Idelays cntvalue", data, quiet = True)
          #if (self.FlagCheck_ICS == False) or (self.FlagCheck_ITS == False):     ## Added from F. Deiana
             #return
          if not(quiet):
             print("- Cntvalue for DOUT{} set to {:03d}".format(i, tap))

       ## CHECK THE CNT VALUE WORKS FOR DOUT1,2,3
       ret = 0
       for adcn in range(1,4):
          markers_found = 0
          for n in range(n_repetitions):
             data_obj = self.acquire_data(8)
             #if (self.FlagCheck_ICS == False) or (self.FlagCheck_ITS == False):     ## Added from F. Deiana
                #return
             
             data_l = data_obj.get_data()
             data_list = []
             for i in range(len(data_l)):
                (dout0, dout1, dout2, dout3) = data_l[i]
                temp = [ "{:032b}".format(int(dout0,16)) , "{:032b}".format(int(dout1,16)) , "{:032b}".format(int(dout2,16)) , "{:032b}".format(int(dout3,16)) ]
                data_list.append(temp)
              
             header = [['0011', '1001'],  ## ...3xxx9xxx...
                       ['0110', '1100'],  ## ...6xxxcxxx...
                       ['1100', '0110'],  ## ...cxxx6xxx...
                       ['1001', '0011']]  ## ...9xxx3xxx...
              ## Find alignment on DOUT0
             data_conc = data_list[0][adcn] + data_list[1][adcn]
             for j in range(32):
                h0 = data_conc[j:j+4]
                h1 = data_conc[j+16:j+20]
                if (h0 == header[adcn][0] and h1 == header[adcn][1]):
                   markers_found += 1
                   if not(quiet):
                      print("*", end = "")
                   break
          if markers_found == n_repetitions:
             if not(quiet):
                print(" DOUT{} -> ok".format(adcn))
          else:
             if not(quiet):
                print(" DOUT{} -> **WARNING**".format(adcn))
             ret = 1

       return ret, tap_list



   def align_data(self, quiet=False):
      self.send_and_listen("adc", "Adc Reset Alignment", quiet = True)

      data_obj = self.acquire_data(8)
      data_l = data_obj.get_data()
      data_list = []
      for i in range(len(data_l)):
         (dout0, dout1, dout2, dout3) = data_l[i]
         temp = [ "{:032b}".format(int(dout0,16)) , "{:032b}".format(int(dout1,16)) , "{:032b}".format(int(dout2,16)) , "{:032b}".format(int(dout3,16)) ]
         data_list.append(temp)

      ## Find alignement pointer
      row = 0
      header = [['0011', '1001'],  ## ...3xxx9xxx...
                ['0110', '1100'],  ## ...6xxxcxxx...
                ['1100', '0110'],  ## ...cxxx6xxx...
                ['1001', '0011']]  ## ...9xxx3xxx...

      ## Find alignment on DOUT0
      ## All four data lines should be aligned on the same pointer...
      data_conc = data_list[0][0] + data_list[1][0]
      align_ptr = 0
      ret = 0
      for j in range(33):
          h0 = data_conc[j:j+4]
          h1 = data_conc[j+16:j+20]
          if (h0 == header[0][0] and h1 == header[0][1]):
             align_ptr = j
             if not(quiet):
                print("\nShift found for DOUT0 -> {}".format(j))
             break
          if j == 32:
             ret = 1
             if not(quiet):
                print("Unable to find markers on DOUT0")

      ## Check the pointer works for dout1,2,3
      align_ptr_c = 32 - align_ptr
      data = write_adc_data_align_ptrs_data(align_ptr_c, align_ptr_c, align_ptr_c, align_ptr_c)
      self.send_and_listen("adc", "Adc Data Alignment Pointers", data, quiet = True)
      self.bit_shift = align_ptr_c

      for i in range(1,4):
         data_conc = data_list[0][i] + data_list[1][i]
         h0 = data_conc[align_ptr:align_ptr + 4]
         h1 = data_conc[align_ptr + 16  :align_ptr + 20]
         if (h0 == header[i][0] and h1 == header[i][1]):
            if not(quiet):
               print("Dout{} aligned correctly".format(i))
         else:
            ret = 1
            if not(quiet):
               print("Found channel with different alignment")

      return ret


   def get_bit_shift(self):                      
      return self.bit_shift
          

   def do_catiaTP(self, invert_phase=0b1, quiet=True):
      ## Write Resync RAM
      data = write_resync_ram_data(0x0, 0xc) ## IDLE
      self.send_and_listen("adc", "write resync ram", data)
      data = write_resync_ram_data(0x1, 0x1) ## Start
      self.send_and_listen("adc", "write resync ram", data)
      data = write_resync_ram_data(0x2, 0xd) ## CATIA TP
      self.send_and_listen("adc", "write resync ram", data)
      data = write_resync_ram_data(0x3, 0x0) ## Stop
      self.send_and_listen("adc", "write resync ram", data)
      ## Do Resync Sequence
      addr_max = 0x3
      self.send_and_listen("adc", "do resync sequence", ((invert_phase & 0b1) << 4) | addr_max)
      if not(quiet):
         print("Do CATIA test-pulse")
   

   def set_catiaTP(self, width=31, quiet=True):
      tpReg = width
      if not(quiet):
         print("Writing CATIA TP IIC register...")
      self.write_iic_register(0x19, tpReg)
      if not(quiet):
         print("Reg. 0x19 set to {}".format(tpReg))


   def test_catiaTP(self, tp_width=31, inv_phase=0b1, quiet=True):
      self.set_catiaTP(width=tp_width)
      self.send_and_listen("adc", "reset adc clk counters", quiet = True)
      if not(quiet):
         print("\nCounters reset done.\n")
      self.do_catiaTP(invert_phase=inv_phase)
      #print("Do CATIA test-pulse")
      reply = self.send_and_listen("adc", "read SEU counters", 2, quiet = True)    ## in v1.2 version this counter was for SEUD
      seud = reply[-4:]    
      reply = self.send_and_listen("adc", "read SEU counters", 3, quiet = True)
      seud = reply[-4:] + seud
      tp_width = int(seud, 16)
      if not(quiet):
         print("CATIA TP duration = {} clk".format(tp_width))
      
      return tp_width


   def check_counters(self, total_time = -1, EoE = False):
      """
      EoE = End on Error --> if True, the test ends if a CRC error is detected
      """
      self.send_and_listen("adc", "reset adc clk counters", quiet = True)
      print("\nCounters reset done.\n")

      N_COUNTERS = 12
      l_int_last = [0 for i in range(N_COUNTERS)]     ## list to store int value of previous cycle
      cycle = 0
      start_time = time.time()
      prev_time = start_time
      test = True
      ret = 0

      while test:

         print("\n\n\t >>> Cycle {} \n".format(cycle))
         l = [] ## to store reply strings
         l_int = [] ## to store int value

         cycle += 1
         for i in range(1, 47):
            if total_time > 0:
               time.sleep(np.min([total_time/(46*2), 0.2]))
            else:
               time.sleep(0.1)
            if i%8 == 0:
               print("*", end="", flush=True)

         ## loop to read all 9 counters (-> see titles list)
         ## the 32 bit counter has to be read in two steps (packet has room for 20 bit max)      

         for i in range(2*N_COUNTERS):
            reply = self.send_and_listen("adc", "read adc data seu errors", i)
            l.append(reply[-4:])

         print("\n")               
         print("---------------------------------------------------------")
         print("Error counters report")
         print("---------------------------------------------------------")

         titles = ["Data error -------->",
                   "CRC error --------->",
                   "CRC0 error -------->",
                   "N Words error ----->",
                   "N Idle error ------>",
                   "N Sample error ---->",
                   "N Frame error ----->",
                   "N Header error ---->",
                   "N Bsln inc -------->",
                   "N Sign com -------->",
                   "N Sign inc -------->",
                   "\nFrame counter:"
                   ]

         ## print the counters on screen
         for i in range(0, len(l), 2): 
            print(titles[int(i/2)], "0x"+l[i+1]+l[i], "- {:,}".format(int(l[i+1]+l[i], 16)))
            l_int.append(int(l[i+1]+l[i], 16))

         for i in range(len(l_int)-1,len(l_int)):
            diff = l_int[i] - l_int_last[i]
            self.frameDiff = diff
            if  diff > 0:
               print("FRAME N diff: {:,}".format(diff))
            else: 
               print("**WARNING** frame numebr overflow")
               #nframe_overflow += 1 
               diff = 0xffffffff + l_int[i] - l_int_last[i]
               print("FRAME N diff: {:,}".format(diff))
               
         ## check if something changed
         print("")
         
         ## 0 -> compl, 1 -> inclompl
         l_name = ["Data", "CRC", "CRC0", "Words", "Idle", "Samples", "Frames", "Header", "Base1", "Sig0", "Sig1"]

         for i in range(0, len(l_int)-1):
            if l_int[i] != l_int_last[i]:
               diff = l_int[i] - l_int_last[i]
               print("**WARNING** "+ l_name[i] + " counter moved forward by {:,}".format(diff))
               #if i == 7 and diff > 100000:
               #   check_and_correct_alignment()

         l_int_last = l_int
        
         now_time = time.time()
         elapsed_time = now_time - start_time
         print("\n\nElapsed time = {:.2f} s".format(elapsed_time))

         self.delta_time = now_time - prev_time
         prev_time = now_time

         if l_int[1] > 0 :                ## l_int[1] = CRC error counter
            print("*****************************")
            print("*** CRC error detected!!! ***")
            print("*****************************\n\n")
            ret = l_int[1]
            if EoE :
               print("*****************************")
               print("*** The test will now end ***")
               print("*****************************\n\n")
               test = False

         if total_time > 0:
            if elapsed_time > total_time:
               break

         self.header_errors = l_int[7]
      
      return ret      
            

   def get_headerErrors(self):
      return self.header_errors

   def get_frameDiff(self):
      return self.frameDiff
            
   def get_timeDiff(self):
      return self.delta_time
            
            
            
            
