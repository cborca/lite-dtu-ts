import matplotlib.pyplot as plt
import pandas as pd
import re
import os, sys
import glob
import time

#from test_functions import *
#from analysis_lib import pll_an, sine_wave, pulse_signal

chip_num = int(sys.argv[1])

#path = 'C:/Users/Fabio/Documents/python_conda/'
#data_path = path + 'data/LiTE-DTU/stb_test/'
#out_path = path + 'outputs/LiTE-DTU/stb_test/'

#path = "/home/cms/lite-dtu-sw/py-LiTE-DTU-kuodev/microtest/"
log_path = "data/pll/stb_test/logs/"
data_path = 'data/pll/stb_test/'
out_path = 'data/pll/stb_test/outputs/'

idle_word = '11101010101010101010101010101010'
sync_word = '00001100110011001100110011001111'

debug = False



log_file = "chip{}_log.dat".format(chip_num)
print("\n\nReading {} file\n".format(log_file))
c = 0
log_data = list()
full_data = list()
with open(log_path + log_file, "r") as f_log:
   lines = f_log.readlines()
   for line in lines:
      if line[0:17] == "**       Cycle n.":
         n_cycle = int(line[17:].split(" ")[0])
      if line[0:45] == "Running stability test for PLL VCO Cap sel = ":
         pll_val = int(line[45:].split("\n")[0])
      if line[0:14] == "FRAME N diff: ":
         frames = int("".join(line[14:].split("\n")[0].split(",")))
         full_data.append([n_cycle, pll_val, frames])
      if line[0:14] == "N Header error":
         header_errors = int( re.sub(",", "", line.split(" - ")[-1]) )
         if header_errors > 0:
            print("Cycle n. {}".format(n_cycle))
            print("Found header errors: {}".format(header_errors))
      if line[0:13] == "frame diff = ":
         frame_diff = int(line[13:].split("\n")[0])
         log_data.append([n_cycle, pll_val, frame_diff])
         c += 1

df_log = pd.DataFrame(log_data, columns = ['N', 'pll', 'frame_diff'])
df_full = pd.DataFrame(full_data, columns = ['N', 'pll', 'frames'])
#print(df_log)
#print(df_full)



bad_logs = df_log.N.values


print("\n\nReading acquired data files...\n")


tbd_format = list()
ok_format = list()
unknown_format = list()

fullWord_cycles = list()
fullSync_cycles = list()
fullIdle_cycles = list()
fullBsln_cycles = list()
fullFrame_cycles = list()
fullSignal_cycles = list()
failure_list = list()

#filenames = glob.glob(data_path + "{}/".format(chip_num) + "2023-*.txt")
filenames = glob.glob(data_path + "{}/".format(chip_num) + "bsln_*2023-*.txt")
n_files = len(filenames)

if n_files == c:

   file_list = sorted(filenames, key=lambda x:x[-17:])
   if debug:
      print(file_list)
   df_log["file"] = file_list

   for j, f in enumerate(file_list):
      #print(f)
      n = bad_logs[j]
      if debug:
         print("Cycle n.{}".format(n))
      if os.path.isfile(f):
         df_data = pd.read_csv(f, header=None)
         df_data.columns = ["adc0", "adc1", "adc2", "adc3"]
         data = df_data.adc0.unique()

         if len(data) == 1:
            if debug:
               print("Found file with same word always repeated: {}".format(f))
            fullWord_cycles.append(n)
            adc = data[0]
            if adc == sync_word:
               if debug:
                  print("Word repeated is a sync")
               fullSync_cycles.append(n)
               failure_list.append("sync_word")
            elif adc == idle_word:
               if debug:
                  print("Word repeated is an idle")
               fullIdle_cycles.append(n)
               failure_list.append("idle_word")
            elif adc[0:2] == "01":
               if debug:
                  print("Word repeated is a baseline complete")
               fullBsln_cycles.append(n)
               failure_list.append("bsln_word")
            elif adc[0:4] == "1101":
               if debug:
                  print("Word repeated is a frame delimeter")
               fullFrame_cycles.append(n)
               failure_list.append("bsln_word")
            elif adc[0:6] == "001010":
               if debug:
                  print("Word repeated is a signal complete")
               fullSignal_cycles.append(n)
               failure_list.append("sign_word")
            else:
               print("Unknown word: {} in file {}".format(adc, f))
               failure_list.append("unknown_word")
               print(n, adc)
               unknown_format.append(n)
               #sys.exit(1)

         elif len(data) > 1:
            tbd_format.append(n)
            if debug:
               print("Found cycle with more than one word: {}".format(f))
            failure_list.append("more_words")
            ok_format.append(n)
            for adc in df_data.adc0.values:
               if adc == sync_word:
                  if debug:
                     print("Sync word: {}".format(adc))
               elif adc == idle_word:
                  if debug:
                     print("IDLE: {}".format(adc))
               elif adc[0:2] == "01":
                  if debug:
                     print("Baseline complete: {}".format(adc))
               elif adc[0:4] == "1101":
                  if debug:
                     print("FRAME: {}".format(adc))
               elif adc[0:6] == "001010":
                  if debug:
                     print("Signal complete: {}".format(adc))
               else:
                  print("Unknown word: {}".format(adc))
                  print(n, adc)
                  unknown_format.append(n)
                  ok_format.pop(-1)
                  break
                  """
                  for i in range(32):
                     temp = adc[i : ] + adc[ : i]
                     print(temp)
                     if temp == sync_word:
                        print("Found misaligned sync word: {}".format(adc))
                        print("Re-aligned word Sync word: {}".format(temp))
                  """
      else:
         print("Unable to open file {}".format(f))
         sys.exit()

   print("End of files with acquired data.\n\n\n")

else:
   print("Number of txt files not matching info from log file: {} vs {}".format(n_files, c))
   sys.exit()

print("Saving csv file with failures list")
df_log["failure"] = failure_list
df_log.to_csv(out_path + "chip{}_fail.csv".format(chip_num))
print(df_log)


filename = "chip{}_out.csv".format(chip_num)
print("\n\nReading {} file\n".format(filename))
df = pd.read_csv(out_path + filename, index_col=0)
bad_cycles = df[df.crc>0].N.unique()

tbd_cycles = list()
good_format = list()
bad_format = list()
sameWord_cycles = list()
sameBsln_cycles = list()
sameFrame_cycles = list()

print("\n\nReading chipscope files...\n")
for n in bad_cycles:
   #print(n)
   dfc = df[df.N==n]
   pll_values = dfc[dfc.crc>0].pll.values
   for pll in pll_values:
      csv_name = 'chipscope_crc_{}_{}'.format(n, pll)
      #print(csv_name)
      filenames = glob.glob(data_path + "{}/".format(chip_num) + csv_name + "_*.csv")
      #print(filenames)
      for fcsv in filenames:
         if os.path.isfile(fcsv):
            #print(fcsv)
            df0 = pd.read_csv(fcsv)
            df1 = df0[::4]       ## take only one sample each 4 because chipscope provides 4 identical data samples
            df1.columns = ["buffer", "window", "trg", "adc1", "adc0", "crc_error", "nsamples_error", "nwords_error", "nframe_error", "header_error", "crc0_error", "read_fd_error"]
            df2 = df1[["trg", "adc0", "crc_error", "nsamples_error", "nwords_error", "nframe_error", "header_error", "crc0_error", "read_fd_error"]]
            data = df2.adc0.unique()
            if len(data) > 1:
               if debug:
                  print("Found cycle with more than one word: {}".format(n))
               tbd_cycles.append(n)
               good_format.append(n)
               for adc in df2.adc0.values:
                  if adc == idle_word:
                     if debug:
                        print("IDLE: {}".format(adc))
                  elif adc[0:2] == "01":
                     if debug:
                        print("Baseline complete: {}".format(adc))
                  elif adc[0:4] == "1101":
                     if debug:
                        print("FRAME: {}".format(adc))
                  else:
                     print("Unknown word: {}".format(adc))
                     print(n, adc)
                     bad_format.append(n)
                     good_format.pop(-1)
                     #sys.exit()
                     break

            elif len(data) == 1:
               if debug:
                  print("Found cycle with same word always repeated: {}".format(n))
               sameWord_cycles.append(n)
               #print(df1)
               adc = data[0]
               if adc[0:2] == "01":
                  if debug:
                     print("Word repeated is a baseline complete")
                  sameBsln_cycles.append(n)
               elif adc[0:4] == "1101":
                  if debug:
                     print("Word repeated is a frame delimeter")
                  sameFrame_cycles.append(n)
                  #sys.exit(1)
               else:
                  print("Unknown word: {}".format(adc))
                  print(n, adc)
                  bad_format.append(n)
                  sys.exit()
         else:
            print("Unable to open file {}".format(fcsv))
            sys.exit()



print("")
print("\nSummary")
print("")
print("Bad cycles from chipscope: {} ({})".format(bad_cycles, len(bad_cycles)))
print("Bad cycles from log file: {} ({})".format(bad_logs, len(bad_logs)))

print("")
print("Bad cycles with more than one word from chipscope: {} ({})".format(tbd_cycles, len(tbd_cycles)))
print("Bad cycles with good format from chipscope: {} ({})".format(good_format, len(good_format)))
print("Bad cycles with unknown word from chipscope: {} ({})".format(bad_format, len(bad_format)))

print("")
print("Bad cycles with more than one word from acquired data: {} ({})".format(tbd_format, len(tbd_format)))
print("Bad cycles with good format from acquired data: {} ({})".format(ok_format, len(ok_format)))
print("Bad cycles with unknown word from acquired data: {} ({})".format(unknown_format, len(unknown_format)))


print("")
print("")
print("Cycles with one word repeated from chipscope: {} ({})".format(sameWord_cycles, len(sameWord_cycles)))
print("Cycles with one baseline complete word repeated from chipscope: {} ({})".format(sameBsln_cycles, len(sameBsln_cycles)))
print("Cycles with one frame word repeated from chipscope: {} ({})".format(sameFrame_cycles, len(sameFrame_cycles)))

print("")
print("Cycles with one word repeated from acquired data: {} ({})".format(fullWord_cycles, len(fullWord_cycles)))
print("Cycles with one sync word repeated from acquired data: {} ({})".format(fullSync_cycles, len(fullSync_cycles)))
print("Cycles with one idle word repeated from acquired data: {} ({})".format(fullIdle_cycles, len(fullIdle_cycles)))
print("Cycles with one baseline complete word repeated from acquired data: {} ({})".format(fullBsln_cycles, len(fullBsln_cycles)))
print("Cycles with one frame word repeated from acquired data: {} ({})".format(fullFrame_cycles, len(fullFrame_cycles)))
print("Cycles with one signal complete word repeated from acquired data: {} ({})".format(fullSignal_cycles, len(fullSignal_cycles)))


