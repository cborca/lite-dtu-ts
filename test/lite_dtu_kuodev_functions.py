'''List of funcion to create and UDP packets, 

Command processor code: 

   target(3 downto 0) & command(7 downto 0) & data(19 downto 0)
         target string  -> 1 byte
         command string -> 2 bytes
         data string    -> 5 bytes

TBN: in order to ensure the desired string length we have to  
   1 - apply a filter (&) to avoid excessively long strings 
   2 - format with the correct length to avoid too short srings
       if we have zeros in significant positions

                             * * *
Functions: 

   packet_builder(target_string = "", command_string = "", data = 0x00000)
       return bytearray packet_to_send


'''


import socket
import time
import binascii

from SocketClientClass import tcp_client as SockClientTCP                               ## Added by Deiana
from Stanford_commlib import stanford_CG635, upper_string, AutoSearchPort       ## Added by Deiana
#from Stanford_commlib import scg_635 as SCG635


def packet_builder(target_string = "", command_string = "", data = 0x00000):

   '''
   This function builds a 32+32 bit string for the command processor,
   to be sent to the adc_controller module

   Supported strings naming:

   Cammel case, space separated word, uderscore separated words,
   upper or lower case. Any combination is accepted by the code. 
   **BUT** one should prefer readable form

   Examples:

      - "Reset ADC IDELAY"
      - "reset adc idelay"
      - "resetAdcIdelay"
      - "reset_adc_idelay"


   Available command string for adc controller:

   - reset adc serdes
   - reset adc idelays 
   - read adc idelays cnt value 
   - write adc idelays cnt value
   - set adc dout inversion 
   - set adc test mode

   - reset adc clk counters  
   - read adc pll lock counter   
   - read seu counters  
   - read adc pll lock sreg  

   - write resync ram
   - do resync sequence
   - set adc sync reset

   - adc reset alignment
   - adc data alignment pointers
   - read adc data align sreg  

   - read adc data misalignment ctrs   
   - read adc data seu errors

   - get adc clk watchdog state
   - get adc clk frequency  

   - acquire adc data   
   - set adc ram address   
   - read adc data ram   

   Available command string for hardware controller:

   - read GPIO switches
   - set GPIO LED
   - adc logic reset
   - read adc clock locked
   - set IIC SPI data
   - do IIC SPI operation
   - set IIC SPI clock div
   - read IIC SPI write ack RAM
   - read IIC SPI read RAM
   - set IIC SPI idle enable
   
   Available command string for swtich controller:

   - set ATM
   - set VINHP
   - set VINHN
   - set VINLP
   - set VINLN
   - read switch status
   
   '''

   ## convert the command string into lower case
   command_string = command_string.lower()
   ## split on spaces and join (without any separator)
   command_string = "".join(command_string.split())
   ## split on underscores and join (without any separator)
   command_string = "".join(command_string.split("_"))

   ## convert the target string into lower case
   target_string = target_string.lower()
   

   if target_string == "adc":
      target_hex = 0x4
      if command_string == "resetadcserdes": 
         command_hex = 0x01
      elif command_string == "resetadcidelays": 
         command_hex = 0x02
      elif command_string == "readadcidelayscntvalue": 
         command_hex = 0x03
      elif command_string == "writeadcidelayscntvalue": 
         command_hex = 0x04
      elif command_string == "setadcdoutinversion": 
         command_hex = 0x05
      elif command_string == "setadctestmode": 
         command_hex = 0x06
      elif command_string == "resetadcclkcounters": 
         command_hex = 0x08
      elif command_string == "readadcplllockcounter": 
         command_hex = 0x09
      elif command_string == "readseucounters": 
         command_hex = 0x0a
      elif command_string == "readadcplllocksreg": 
         command_hex = 0x0b
      elif command_string == "writeresyncram": 
         command_hex = 0x10
      elif command_string == "doresyncsequence": 
         command_hex = 0x11
      elif command_string == "setadcsyncreset": 
         command_hex = 0x12
      elif command_string == "adcresetalignment": 
         command_hex = 0x18
      elif command_string == "adcdataalignmentpointers": 
         command_hex = 0x19
      elif command_string == "readadcdataalignsreg": 
         command_hex = 0x1a
      elif command_string == "acquireadcdata": 
         command_hex = 0x1b
      elif command_string == "readadcdatamisalignmentctrs": 
         command_hex = 0x1c
      elif command_string == "readadcdataseuerrors": 
         command_hex = 0x20
      elif command_string == "getadcclkwatchdogstate": 
         command_hex = 0x80
      elif command_string == "getadcclkfrequency": 
         command_hex = 0x81
      elif command_string == "setadcramaddress": 
         command_hex = 0x90
      elif command_string == "readadcdataram": 
         command_hex = 0x91
      else:
         SCG635.RunningState_CG635("OFF")
         SockClientTCP.send_command("INV_CMD_STR")
         #raise Exception("Invalid command string")
 
   elif target_string == "hardware":
      target_hex = 0x1
      if command_string == "readgpioswitches": 
         command_hex = 0x01
      elif command_string == "setgpioled": 
         command_hex = 0x02
      elif command_string == "adclogicreset": 
         command_hex = 0x08
      elif command_string == "readadcclocklocked": 
         command_hex = 0x10
      elif command_string == "setiicspidata": 
         command_hex = 0x20
      elif command_string == "doiicspioperation": 
         command_hex = 0x21
      elif command_string == "setiicspiclockdiv": 
         command_hex = 0x22
      elif command_string == "readiicspiwriteackram": 
         command_hex = 0x23
      elif command_string == "readiicspireadram" : 
         command_hex = 0x24
      elif command_string == "setiicspiidleenable" : 
         command_hex = 0x25
      else:
         SCG635.RunningState_CG635("OFF")
         SockClientTCP.send_command("INV_CMD_STR")
         #raise Exception("Invalid command string")
         
   elif target_string == "switch":
      target_hex = 0x2
      if command_string == "setatm": 
         command_hex = 0x01
      elif command_string == "setvinhp": 
         command_hex = 0x02
      elif command_string == "setvinhn": 
         command_hex = 0x03
      elif command_string == "setvinlp": 
         command_hex = 0x04
      elif command_string == "setvinln": 
         command_hex = 0x05
      elif command_string == "readswitchstatus": 
         command_hex = 0x06
      else:
         SCG635.RunningState_CG635("OFF")
         SockClientTCP.send_command("INV_CMD_STR")
         #raise Exception("Invalid command string")

   else: 
      SCG635.RunningState_CG635("OFF")
      SockClientTCP.send_command("INV_TRGT_STR")
      #raise Exception("Invalid target string")

   target_hex_str = '{:01x}'.format(target_hex & 0xf)
   command_hex_str = '{:02x}'.format(command_hex & 0xff)
   data_hex_str = '{:05x}'.format(data & 0xfffff)

   packet_to_send = target_hex_str + command_hex_str + data_hex_str
   packet_to_send = '01010000' + packet_to_send

   return bytearray.fromhex(packet_to_send) 


## DATA BUILDERS

def write_resync_ram_data(addra = 0b0000, din = 0b0000):

   addra_str = '{:04b}'.format(addra & 0b1111)
   din_str = '{:04b}'.format(din & 0b1111)   

   data = int(addra_str + din_str, 2)

   return data


def write_iic_spi_ram_data(addra = 0x00, din = 0x00):

   addra_str = '{:07b}'.format(addra & 0b1111111)
   din_str = '{:08b}'.format(din & 0b11111111)   

   data = int(addra_str + din_str, 2)

   return data

   
def write_iic_spi_addrmax(mode = "w", addr_max = 18):

   if mode == "w":
      data = '01'
   elif mode == "r":
      data = '10'
   else:
      raise Exception("Invalid argument")
   
   data = data + '{:011b}'.format(0)
   data = data + '{:07b}'.format(addr_max & 0b1111111)
   data = int(data, 2)
   
   return data

def write_adc_data_align_ptrs_data(ptr0, ptr1, ptr2, ptr3):

   ptr0_str = '{:05b}'.format(ptr0 & 0b11111)
   ptr1_str = '{:05b}'.format(ptr1 & 0b11111)
   ptr2_str = '{:05b}'.format(ptr2 & 0b11111)
   ptr3_str = '{:05b}'.format(ptr3 & 0b11111)
  

   data = int(ptr0_str + ptr1_str + ptr2_str + ptr3_str, 2)

   return data

def write_do_iic_spi_operation_data(invert_dso = 0, disable_dt = 0, mode = "w", addr = 0x02, reg = 0x00):

   data = '00' + '{:01b}'.format(invert_dso)
   data = data + '{:01b}'.format(disable_dt)
   
   if mode == "w":
      data = data +'0'
   elif mode == "r":
      data = data + '1'
   else:
      raise Exception("Invalid argument")
      
   ## Device Address
   data = data + '{:07b}'.format(addr)
   ## Register Number (Starting)
   data = data + '{:08b}'.format(reg)
   data = int(data, 2)
   
   return data
   
   
def write_adc_idelays_cntvalue_data(cntvalue = 0, adc_select = 0):

   data = '{:09b}'.format(cntvalue & 0b111111111)
   data = data + '{:02b}'.format(adc_select & 0b11)
   data = int(data, 2)
   
   return data


"""
def compare_bit_strings(s1 = "", s2 = ""):

   n_flip = 0
   s1 = "{:08b}".format(int(s1,16))
   s2 = "{:08b}".format(int(s2,16))

   for i in range(len(s1)):
      if s1[i] != s2[i]:
         n_flip += 1      

   return n_flip

def reg_changed_idx_list(l1 = [], l2 = []):
   ### returns the list of the indexes of non equal elements of two lists
   ### of the same lenght
   
   if len(l1) != len(l2):
      raise Exception("The two list aren't of the same length")
   
   idx_list = []
   for i in range(len(l1)):
      if l1[i] != l2[i]:
         idx_list.append(i)
         
   return idx_list
   

################################################################################################################
################################################################################################################

   
def acq_data(udp_socket, N):
   
   remote_address = "192.168.1.10"
   remote_port = 10000
   buffer_size = 8192
   
   nSer = 4
   nCmd = 2 * nSer  ## number of commands in one RAM address cycle (=2 for each serializer to be read)
   full_pkt = bytearray()
   dlist = []
            
   for j in range(N):      ## build packet with selected number of commands (N)
      if j%8 != 7:
         data = j%8
      else: 
         data = j%8 + 8

      packet = packet_builder("adc", "Read ADC Data RAM", data)    ## build 64-bit command
      full_pkt.extend(packet)                                      ## assemble the commands together in a single packet

   udp_socket.sendto(full_pkt, (remote_address, remote_port))
   reply_string = udp_socket.recvfrom(buffer_size)[0]
   rcv_pkt = (binascii.hexlify(reply_string)).decode("utf-8")
   
   n_bytes = int(len(rcv_pkt) / 2)
   n_words = int(n_bytes / 8)

   dout = [ rcv_pkt[i*16 : (i+1)*16][-4:] for i in range(n_words) ]

   for i in range(int(len(dout)/8)):
      dout00 = "{:016b}".format(int(dout[i*8]  , 16))
      dout01 = "{:016b}".format(int(dout[i*8+1], 16)) 
      dout10 = "{:016b}".format(int(dout[i*8+2], 16))
      dout11 = "{:016b}".format(int(dout[i*8+3], 16)) 
      dout20 = "{:016b}".format(int(dout[i*8+4], 16))
      dout21 = "{:016b}".format(int(dout[i*8+5], 16)) 
      dout30 = "{:016b}".format(int(dout[i*8+6], 16))
      dout31 = "{:016b}".format(int(dout[i*8+7], 16)) 
      dout0 = dout01 + dout00
      dout1 = dout11 + dout10
      dout2 = dout21 + dout20
      dout3 = dout31 + dout30
      dlist.append([dout0, dout1, dout2, dout3])

   return dlist

"""
