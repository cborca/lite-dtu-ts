import sys
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from matplotlib.colors import LinearSegmentedColormap
from matplotlib.colors import ListedColormap
 
#chip_num = 535
chip_num = int(sys.argv[1])


path = 'data/pll/report/'
filename = "chip{}_out.csv".format(chip_num)
ndiv = 30

df = pd.read_csv(path + filename, index_col=0)
grs = df.groupby(df.N)
n = df.N.unique()
N_cycles = len(n)
print("Found {} cycles".format(N_cycles))
if np.max(n) > 30:
   print("Warning: too many cycles to plot in a single figure")
   print("Data will be displayed in plots with {} cycles each".format(ndiv))
   #n = range(30)

data_list = list()
for n_cycle in n:
    dfg = grs.get_group(n_cycle)
    df_sel = dfg.rename(columns={'crc': n_cycle})
    dfT = df_sel.T
    dfT.columns = [dfg.pll.values]
    data_list.append(dfT.loc[n_cycle])
df_crc = pd.concat(data_list, axis=1).T
#print(df_crc)

data_list = list()
for n_cycle in n:
    dfg = grs.get_group(n_cycle)
    df_sel = dfg.rename(columns={'bit': n_cycle})
    dfT = df_sel.T
    dfT.columns = [dfg.pll.values]
    data_list.append(dfT.loc[n_cycle])
df_bit = pd.concat(data_list, axis=1).T
#print(df_bit)


cmap = ListedColormap(['c','b','r'])
fig = list()
ax = list()
for i in range( int( np.ceil(N_cycles/ndiv) ) ):
   fig.append(0)
   ax.append(0)
   start = i*ndiv
   stop = (i+1)*ndiv
   df_crc_plot = df_crc.loc[start : stop]
   df_bit_plot = df_bit.loc[start : stop]
   #print(start, stop)
   #print(df_crc_plot)

   fig[i], ax[i] = plt.subplots( 1, figsize=( 15, df_crc_plot.shape[0]/3 ) )
   #note = df_crc_plot
   note = df_bit_plot
   ax[i] = sns.heatmap(df_crc_plot, annot=note, annot_kws={"size": 7}, linewidths=.5, linecolor='lightgray', cmap=cmap, vmin=-1.5, vmax=1.5)
   colorbar = ax[i].collections[0].colorbar
   colorbar.set_ticks([-1, 0, 1])
   colorbar.set_ticklabels(['not locked', 'OK', 'CRC errors'])
   _, labels = plt.yticks()
   plt.setp(labels, rotation=0)
   ax[i].set_title("CHIP {} - CRC errors (numbers indicate bit_shift for data word alignment)".format(chip_num))
   #ax[i].set_title("CRC errors (numbers indicate bit_shift for data word alignment)")
   ax[i].set_xlabel("PLL reg")
   ax[i].set_ylabel("Cycle")
   ax[i].tick_params(axis='both', which='major', labelsize=8)
   #ax[i].tick_params(axis='both', which='minor', labelsize=8)
   fig[i].tight_layout()
   fig[i].savefig(path + "/img/chip{}_{}.png".format(chip_num, i))


plt.show()

