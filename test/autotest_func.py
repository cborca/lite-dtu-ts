'''
List of funcion to run autotest procedure
'''

import time
import matplotlib.pyplot as plt
import pandas as pd
import os, sys  

from test_functions import *
from analysis_lib import pll_an, plt_an, sine_wave, pulse_signal

from SocketClientClass import tcp_client as SockClientTCP                       ## Added by Deiana
from Stanford_commlib import stanford_CG635, upper_string, AutoSearchPort       ## Added by Deiana
#from Stanford_commlib import scg_635 as SCG635


###########################################################################################################

def ldtu_init(ps, ldtu, cfgfile, N_ini, Iref, tol, invert_phase=0b0, verbosity=0, dtu_ver=3.0):
   ##############################
   ## LiTE-DTU initialization
   ## 1. Power ON
   ## 2. Full reset
   ## 3. I2C configuration
   ## 4. Check power consumption
   ##
   ## set ps = -1 to avoid use of external power supply
   ##
   
   next = False
   curr = [-1, -1, -1]
   Nconf = 0
   conf_err = 0

   for n in range(N_ini):
      N = n + 1

      ############################################
      ## Chip power ON and configuration
      if ps == -1:
         SockClientTCP.send_command("POWER_CYCLE")    ## Microtest power supply
      else:
         ps.power_off()                               ## INFN power supply
         time.sleep(0.5)
         ps.power_on()
         time.sleep(0.5)
      
      ldtu.set_switch("write", "ATM")                 ## set switches to 'Test-mode' (1,0,0,0,0)
      ldtu.reset_full(invert_phase, version=dtu_ver)  ## full reset
      curr_pre = ps.read_curr()                       ## read currents absorption from power supply

      if verbosity > 0:
         print("Configuring DTU with {} configuration file".format(cfgfile))
      ldtu.cu_config(cfgfile)                         ## config dtu
      Nconf += 1

      if verbosity > 2:
         ldtu_cfg = ldtu.read_iic_config(2, 0, 26, quiet=False)
      else:
         ldtu_cfg = ldtu.read_iic_config(2, 0, 26, quiet=True)
      ret = check_cfg(ldtu_cfg, cfgfile, quiet=True)
      if ret != 0:
         print("LiTE-DTU configuration not OK!!!\n\n")
         conf_err += 1

      else:
         if ps != -1:
            curr = ps.read_curr()               ## read currents absorption from power supply
            ret = check_curr(curr, Iref, tol)
            if verbosity > 0:
               print("Current absorption: {}".format(curr))
         else:
            ret = SockClientTCP.send_command("PWRCons")
         
         if ret != "OK":
            if verbosity > 0:
               print("Current Values not Ok --> reference values: {}\n\n".format(Iref))
            if ps == -1:
               SockClientTCP.send_command("POWER_CYCLE")
         else:
            if ps == -1:
               SockClientTCP.send_command("WRT_PCons")
            if verbosity > 0:
               print("Chip power-on successful")
            next = True
            break

   if not(next):
      print("\nLiTE-DTU initialization failed after {} attempts\n\n".format(N))
      return -1, curr, curr_pre, conf_err, Nconf, N
   else:
      print("\nLiTE-DTU initialization successful after {} attempts\n\n".format(N))
      return 0, curr, curr_pre, conf_err, Nconf, N

###########################################################################################################



def ldtu_pll(path, ps, ldtu, cfgfile, N_pll, save_mode, invert_phase=0b0, verbosity=0, dtu_ver=3.0):
   ##############################
   ## LiTE-DTU PLL manual lock
   ##
   ## set ps = -1 to avoid use of external power supply
   ##
   if verbosity > 1:
      be_quiet = False
   else:
      be_quiet = True

   pllReg     = ['0x00', '0xff']
   pllRegAuto = ['0x00', '0xff']
   pllRange   = -99
   pll_i_man  = -99
   pll_i_auto = -99
   pll_diff   = -99

   manual_lock = True
   if dtu_ver > 2.0:
      auto_lock = True
   else:
      auto_lock = False
      pllRegAuto = ['0x01', '0xff']
      pll_diff = -1

   next = False

   for n in range(N_pll):
      N = n + 1

      ############################################
      ## Chip power ON and configuration
      if ps == -1:
         SockClientTCP.send_command("POWER_CYCLE")    ## Microtest power supply
      else:
         ps.power_off()                               ## INFN power supply
         time.sleep(0.5)
         ps.power_on()
         time.sleep(1)
      
      ldtu.set_switch("write", "ATM")                 ## set switches to 'Test-mode' (1,0,0,0,0)

      if manual_lock:                                       ## perform PLL manual lock scan
         ldtu.reset_full(invert_phase, version=dtu_ver)     ## full reset
         ldtu.cu_config(cfgfile)                            
         ldtu.setManLock(version=dtu_ver, quiet=be_quiet)   ## config dtu for manual lock
      
         fn = build_fn().split(".")[0]
         outfile = path + "pll_scan/PLL_scan_{}.png".format(fn)
         pll_dict = ldtu.do_pll_scan()
         pll = pll_an()
         ret = pll.pll_analysis(pll_dict, outfile, save_mode, show=False)
         if ret != 0:
            if verbosity > 0:
               print("PLL scan not Ok !\n\n")
            if ps == -1:
               SockClientTCP.send_command("POWER_CYCLE")    ## Microtest power supply
            else:
               ps.power_off()                               ## INFN power supply
               time.sleep(0.5)
         else:
            if verbosity > 0:
               print("PLL manual scan successful")
            pllRange      = pll.get_pllRange()
            pllReg        = pll.get_pllReg()
            pll_i_man, __ = pllReg_values(pllReg)

            if pllRange < 8:
               if verbosity > 0:
                  print("Error: PLL lock range too small: {}".format(pllRange))
            else:
               if verbosity > 0:
                  print("PLL lock range = {}".format(pllRange))
                  print("PLL IIC registers -> 0x0f: {}, 0x10: {}\n".format(pllReg[0], pllReg[1]))
               if not(auto_lock):
                  ldtu.writePLLreg(pllReg, quiet=be_quiet)
                  next = True
               break

   if auto_lock:                                            ## Set PLL to auto-lock
      for n in range(N, N_pll):
         ldtu.reset_full(invert_phase, version=dtu_ver)        ## full reset
         ldtu.cu_config(cfgfile)                               
         ldtu.setAutoLock(version=dtu_ver, quiet=be_quiet)     ## config dtu for auto-lock
         ldtu.reset_pll(invert_phase, version=dtu_ver)
         ldtu.reset_dtu(invert_phase, version=dtu_ver)
         time.sleep(0.3)
         ldtu_cfg       = ldtu.read_iic_config(2, 0, 26)
         pllRegAuto     = [ hex(int(ldtu_cfg[9].split(" ")[1], 16) & 0x1), hex(int(ldtu_cfg[10].split(" ")[1], 16)) ]
         pll_i_auto, __ = pllReg_values(pllRegAuto)
         if verbosity > 0:
            print("PLL IIC registers -> 0x09: {}, 0x0a: {}\n".format(pllRegAuto[0], pllRegAuto[1]))

         if manual_lock and auto_lock:          ## compare manualLock and autoLock results
            pll_diff = pll_i_auto - pll_i_man
            if verbosity > 0:
               print("Compare Manual Lock vs Auto Lock")
            if abs(pll_diff) < 11:
               if verbosity > 0:
                  print("pll_auto - pll_manual = {} --> OK".format(pll_diff))
               next = True
               break
            else:
               N = n + 1
               if verbosity > 0:
                  print("pll_auto - pll_manual = {} --> NOT OK!!!!!!!!!!!!!!!!!!!!!!!!".format(pll_diff))
         else:
            break

   if not(next):
      print("\nLiTE-DTU PLL lock failed after {} attempts\n\n".format(N))
      return -1, pllReg, pllRange, pllRegAuto, pll_diff, N
   else:
      print("\nLiTE-DTU PLL lock successful after {} attempts\n\n".format(N))
      return 0, pllReg, pllRange, pllRegAuto, pll_diff, N

###########################################################################################################






###########################################################################################################

def ldtu_align(ps, ldtu, cfgfile, pllReg, idel, align_dbg, N_ali, invert_phase=0b0, verbosity=0, dtu_ver=3.0):
   ###################################
   ## LiTE-DTU data links alignment
   ##
   ## set ps = -1 to avoid use of external power supply
   ## idel = delays extra tap for the 4 lanes
   ## align_dbg = True to perform eye align scan debug on all 4 lanes
   ##
   if verbosity > 1:
      be_quiet = False
   else:
      be_quiet = True

   next = False
   N_eye = 0
   N_data_al = 0
   tap_list = [-1, -1, -1, -1]

   for n in range(N_ali):
      N = n + 1
      #################################
      ## Eye alignment
      ##
      ldtu.reset_full(invert_phase, version=dtu_ver)
      if verbosity > 0:
         print("Configuring DTU with {} configuration file".format(cfgfile))
      ldtu.cu_config(cfgfile)
      if not("autoLock" in cfgfile):
         if verbosity > 0:
            print("Configuring DTU PLL in manual lock")
         ldtu.writePLLreg(pllReg)

      ldtu.reset_pll(invert_phase, version=dtu_ver)
      ldtu.reset_dtu(invert_phase, version=dtu_ver)

      ## Test 4 Dout lanes eye independently
      if align_dbg:
         for ii in range(4):                
            print("\n\nDout{}".format(ii))
            ldtu.eye_align_dbg(dout_ref=ii, eye_scan_step=5)

      N_eye += 1
      ret, tap_list = ldtu.eye_align(idel, quiet=be_quiet)
      if ret != 0:
         if verbosity > 0:
            print("Eye alignment not Ok !\n\n")
         if ps == -1:
            SockClientTCP.send_command("POWER_CYCLE")    ## Microtest power supply
         else:
            ps.power_off()                               ## INFN power supply
            time.sleep(0.5)
            ps.power_on()
            time.sleep(1)
      else:
         if verbosity > 0:
            print("Eye alignment successful")

         #################################
         ## Data alignment
         N_data_al += 1
         ret = ldtu.align_data(quiet=be_quiet)
         if ret != 0:
            if verbosity > 0:
               print("Data alignment not Ok !\n\n")
            if ps == -1:
               SockClientTCP.send_command("POWER_CYCLE")    ## Microtest power supply
            else:
               ps.power_off()                               ## INFN power supply
               time.sleep(0.5)
               ps.power_on()
               time.sleep(1)
         else:
            if verbosity > 0:
               print("Data alignment successful")
            next = True
            break

   if not(next):
      print("\nLiTE-DTU data links alignment failed after {} attempts\n\n".format(N))
      return -1, tap_list, N, N_eye, N_data_al
   else:
      print("\nLiTE-DTU data links alignment successful after {} attempts\n\n".format(N))
      return 0, tap_list, N, N_eye, N_data_al

###########################################################################################################





###########################################################################################################

def adc_cal(ldtu, ADCH=True, ADCL=True, inv_phase=0b0):
   ##################################################################################
   ## ADC calibration
   ##
   if ADCH and ADCL:
      ldtu.reset_adcH(invert_phase=inv_phase)
      ldtu.reset_adcL(invert_phase=inv_phase)
      ldtu.adc_calib(invert_phase=inv_phase)       ## run calibration for both ADCs
   elif ADCH:
      ldtu.reset_adcH(invert_phase=inv_phase)
      ldtu.adcH_calib(invert_phase=inv_phase)      ## run calibration only for ADCH
   elif ADCL:
      ldtu.reset_adcL(invert_phase=inv_phase)
      ldtu.adcL_calib(invert_phase=inv_phase)      ## run calibration only for ADCL
   ldtu.reset_ATU(invert_phase=inv_phase)
   time.sleep(0.2)

###########################################################################################################




###########################################################################################################

def adc_test(path, ldtu, wfgH, wfgL, wfgS, bsln_opt, n_data_sin, enob_min, N_cal, save_mode, data_red=False, long_calib=False, invert_phase=0b0, verbosity=0):

   if verbosity > 1:
      be_quiet = False
   else:
      be_quiet = True

   bsln_mean, std_max = bsln_opt
   adc_h_mean, adc_h_stdv, adc_l_mean, adc_l_stdv = -99, -99, -99, -99
   fitH_param = -99, -99, -99, -99
   fitL_param = -99, -99, -99, -99
   enob_H_fit, enob_H_fft, enob_L_fit, enob_L_fft = -1, -1, -1, -1
   N_adc_bsln, N_adc_sine = 0, 0
   N_cal_H, N_cal_L = 0, 0
   ##################################################################################
   ## Test ADC
   ##
   next = False
   wfgH.set_wfg("calib")               ## set waveform generator to calibration mode
   wfgL.set_wfg("calib")               ## set waveform generator to calibration mode
   ldtu.set_switch("write", "DC")      ## enable switches for DC inputs (1,1,1,1,1)

   ## Set ADC test-mode and increase calibration time
   if long_calib:
      ldtu.write_iic_register(reg = 0x00, value = 0x01, addr = 0x00)
      ldtu.write_iic_register(reg = 0x00, value = 0x01, addr = 0x01)
      ldtu.write_iic_register(reg = 0x01, value = 0xfe, addr = 0x00)
      ldtu.write_iic_register(reg = 0x01, value = 0xfe, addr = 0x01)

   time.sleep(0.8)
   ## Run ADC calibration
   adc_cal(ldtu, ADCH=True, ADCL=True, inv_phase=invert_phase)
   N_cal_H += 1 
   N_cal_L += 1
   #adc_cal(ldtu, ADCH=True, ADCL=True, inv_phase=invert_phase)


   for n in range(N_cal):
      N = n + 1

      ## Check ADC calibration: baseline
      N_adc_bsln += 1
      adc_h_mean, adc_h_stdv, adc_l_mean, adc_l_stdv = ldtu.adc_bsln(quiet=be_quiet)
      ret_H, ret_L = check_baseline(adc_h_mean, adc_h_stdv, adc_l_mean, adc_l_stdv, bsln_mean, std_max)
      #ret_H, ret_L = 0, 0

      if (ret_H == 1) or (ret_L == 1):
         if N < N_cal:
            if (ret_H == 1) and (ret_L == 1):
               if verbosity > 0:
                  print("ADCs baseline not Ok !")
                  print("Restarting ADCs Calibration\n\n")
               ldtu.writeADC_ctrlReg(adcH=False, adcL=False, quiet=be_quiet)
               time.sleep(0.5)
               ldtu.writeADC_ctrlReg(adcH=True, adcL=True, quiet=be_quiet)
               adc_cal(ldtu, ADCH=True, ADCL=True, inv_phase=invert_phase)
               N_cal_H += 1 
               N_cal_L += 1
            elif ret_H == 1:
               if verbosity > 0:
                  print("ADC H baseline not Ok !")
                  print("Restarting ADC H Calibration\n\n")
               ldtu.writeADC_ctrlReg(adcH=False, adcL=True, quiet=be_quiet)
               time.sleep(0.5)
               ldtu.writeADC_ctrlReg(adcH=True, adcL=True, quiet=be_quiet)
               adc_cal(ldtu, ADCH=True, ADCL=False, inv_phase=invert_phase)
               N_cal_H += 1 
            elif ret_L == 1:
               if verbosity > 0:
                  print("ADC L baseline not Ok !")
                  print("Restarting ADC L Calibration\n\n")
               ldtu.writeADC_ctrlReg(adcH=True, adcL=False, quiet=be_quiet)
               time.sleep(0.5)
               ldtu.writeADC_ctrlReg(adcH=True, adcL=True, quiet=be_quiet)
               adc_cal(ldtu, ADCH=False, ADCL=True, inv_phase=invert_phase)
               N_cal_L += 1

      else:
         ## Check ADC calibration: sine wave
         ldtu.set_switch("write", "ATM")                 ## disable switches for DC inputs (1,0,0,0,0)
         wfgS.set_wfg("sine")                            ## configure the AC waveform generator for a sine wave
         freq = "0.5"
         freq_dict = build_freq(filename="cfg/freq_table_teledyne.txt", isFilt="NO")
         f = float(get_freqParam(freq_dict, freq, "Val"))
         a = float(get_freqParam(freq_dict, freq, "Ampl"))
         wfgS.set_sine(str(f), str(a))                   ## set the sine waveform frequency and amplitude

         #sys.exit()

         time.sleep(0.5)
         N_adc_sine += 1
         filename = build_fn(freq_dict)                  ## generate filename to save the data acquired
         data_sin = ldtu.acquire_data(n_data_sin)        ## acquire data
         #data_sin.write_raw(filename)                   ## save data in raw format
         data_sin.write_decoded(path, filename)          ## save data in decoded format
         wfgS.set_wfg("off")                             ## disable the AC waveform generator output
         sin = sine_wave(path + filename)                ## prepare for plot
         if save_mode > 0:
            sin.plot_sine(zoom=True, show=False)         ## plot sine wave
            if save_mode == 2:
               sin.save_plot()                           ## save figure
         ret_H = sin.do_fit(f/1e6, "H", quiet=be_quiet)
         ret_L = sin.do_fit(f/1e6, "L", quiet=be_quiet)
         
         if (ret_H[0] == 1) or (ret_L[0] == 1):
            ldtu.set_switch("write", "DC")               ## enable switches for DC inputs (1,1,1,1,1)
            wfgS.set_wfg("off")                          ## Added from Deiana
            if verbosity > 0:
               print("Bad sine waveform fit")
            time.sleep(1.0)

            if N < N_cal:
               if (ret_H[0] == 1) and (ret_L[0] == 1):
                  if verbosity > 0:
                     print("Restarting ADCs Calibration\n\n")
                  ldtu.writeADC_ctrlReg(adcH=False, adcL=False, quiet=be_quiet)
                  time.sleep(0.5)
                  ldtu.writeADC_ctrlReg(adcH=True, adcL=True, quiet=be_quiet)
                  adc_cal(ldtu, ADCH=True, ADCL=True, inv_phase=invert_phase)
                  N_cal_H += 1 
                  N_cal_L += 1
               elif ret_H[0] == 1:
                  if verbosity > 0:
                     print("Restarting ADC H Calibration\n\n")
                  ldtu.writeADC_ctrlReg(adcH=False, adcL=True, quiet=be_quiet)
                  time.sleep(0.5)
                  ldtu.writeADC_ctrlReg(adcH=True, adcL=True, quiet=be_quiet)
                  adc_cal(ldtu, ADCH=True, ADCL=False, inv_phase=invert_phase)
                  N_cal_H += 1 
               elif ret_L[0] == 1:
                  if verbosity > 0:
                     print("Restarting ADC L Calibration\n\n")
                  ldtu.writeADC_ctrlReg(adcH=True, adcL=False, quiet=be_quiet)
                  time.sleep(0.5)
                  ldtu.writeADC_ctrlReg(adcH=True, adcL=True, quiet=be_quiet)
                  adc_cal(ldtu, ADCH=False, ADCL=True, inv_phase=invert_phase)
                  N_cal_L += 1

         else:                                           ## Use ENOB from fit to validate ADC calibration
            __, yfit_H, ydata_H, fitH_param = ret_H
            __, yfit_L, ydata_L, fitL_param = ret_L
            enob_H_fit = sin.enob_from_fit(yfit_H, ydata_H)
            enob_L_fit = sin.enob_from_fit(yfit_L, ydata_L)
            if verbosity > 0:
               print("\nENOB ADCH fit = {:.2f} bit".format(enob_H_fit))
               print(  "ENOB ADCL fit = {:.2f} bit\n".format(enob_L_fit))
            ret_H = sin.check_enob(enob_H_fit, "ADCH", enob_min, quiet=be_quiet)
            ret_L = sin.check_enob(enob_L_fit, "ADCL", enob_min, quiet=be_quiet)
            
            if (ret_H == 1) or (ret_L == 1):
               ldtu.set_switch("write", "DC")       ## enable switches for DC inputs (1,1,1,1,1)
               wfgS.set_wfg("off")                  ## Added from Deiana
               if verbosity > 0:
                  print("ENOB not Ok !")
               time.sleep(1.0)
               
               if N < N_cal:
                  if (ret_H == 1) and (ret_L == 1):
                     if verbosity > 0:
                        print("Restarting ADCs Calibration\n\n")
                     ldtu.writeADC_ctrlReg(adcH=False, adcL=False, quiet=be_quiet)
                     time.sleep(0.5)
                     ldtu.writeADC_ctrlReg(adcH=True, adcL=True, quiet=be_quiet)
                     adc_cal(ldtu, ADCH=True, ADCL=True, inv_phase=invert_phase)
                     N_cal_H += 1 
                     N_cal_L += 1
                  elif ret_H == 1:
                     if verbosity > 0:
                        print("Restarting ADC H Calibration\n\n")
                     ldtu.writeADC_ctrlReg(adcH=False, adcL=True, quiet=be_quiet)
                     time.sleep(0.5)
                     ldtu.writeADC_ctrlReg(adcH=True, adcL=True, quiet=be_quiet)
                     adc_cal(ldtu, ADCH=True, ADCL=False, inv_phase=invert_phase)
                     N_cal_H += 1 
                  elif ret_L == 1:
                     if verbosity > 0:
                        print("Restarting ADC L Calibration\n\n")
                     ldtu.writeADC_ctrlReg(adcH=True, adcL=False, quiet=be_quiet)
                     time.sleep(0.5)
                     ldtu.writeADC_ctrlReg(adcH=True, adcL=True, quiet=be_quiet)
                     adc_cal(ldtu, ADCH=False, ADCL=True, inv_phase=invert_phase)
                     N_cal_L += 1

            else:
               enob_H_fft, enob_H2, maxFreq = sin.enob_from_fft("ADCH", is_plot=False, Ncut=1)    ## Evaluate ENOB also from FFT
               if verbosity > 0:
                  print("ENOB ADCH fft = {:.2f} bit".format(enob_H_fft))
               enob_L_fft, enob_L2, maxFreq = sin.enob_from_fft("ADCL", is_plot=False, Ncut=1)    ## Evaluate ENOB also from FFT
               if verbosity > 0:
                  print("ENOB ADCL fft = {:.2f} bit".format(enob_L_fft))
               
               if verbosity > 0:
                  print("\nTesting ADCs saturation...")
               wfgS.set_wfg("sine")                               ## configure the AC waveform generator for a sine wave
               a = float("2.5")
               wfgS.set_sine(str(f), str(a))                      ## set the sine waveform frequency and amplitude
               time.sleep(1)
               filename = build_fn(freq_dict)                     ## generate filename to save the data acquired
               data_sin = ldtu.acquire_data(500)                  ## acquire data
               data_sin.write_decoded(path, filename)             ## save data in decoded format
               wfgS.set_wfg("off")                                ## disable the AC waveform generator output
               sin = sine_wave(path + filename)                   ## prepare for plot
               if save_mode > 0:
                  sin.plot_sine(zoom=True, show=False, sat=True)  ## plot sine wave
                  if save_mode == 2:
                     sin.save_plot()                              ## save figure
                  
               ret_satH, ret_satL = sin.check_sat(quiet=be_quiet)
               
               if data_red:
                  sin.delete_rawData(path, filename, quiet=be_quiet)

               if (ret_satH == 1) or (ret_satL == 1):
                  ldtu.set_switch("write", "DC")       ## enable switches for DC inputs (1,1,1,1,1)
                  wfgS.set_wfg("off")
                  if verbosity > 0:
                     print("ADC saturation not Ok !")
                  time.sleep(1.0)
                  
                  if N < N_cal:
                     if (ret_satH == 1) and (ret_satL == 1):
                        if verbosity > 0:
                           print("Restarting ADCs Calibration\n\n")
                        ldtu.writeADC_ctrlReg(adcH=False, adcL=False, quiet=be_quiet)
                        time.sleep(0.5)
                        ldtu.writeADC_ctrlReg(adcH=True, adcL=True, quiet=be_quiet)
                        adc_cal(ldtu, ADCH=True, ADCL=True, inv_phase=invert_phase)
                        N_cal_H += 1
                        N_cal_L += 1
                     elif ret_satH == 1:
                        if verbosity > 0:
                           print("Restarting ADC H Calibration\n\n")
                        ldtu.writeADC_ctrlReg(adcH=False, adcL=True, quiet=be_quiet)
                        time.sleep(0.5)
                        ldtu.writeADC_ctrlReg(adcH=True, adcL=True, quiet=be_quiet)
                        adc_cal(ldtu, ADCH=True, ADCL=False, inv_phase=invert_phase)
                        N_cal_H += 1
                     elif ret_satL == 1:
                        if verbosity > 0:
                           print("Restarting ADC L Calibration\n\n")
                        ldtu.writeADC_ctrlReg(adcH=True, adcL=False, quiet=be_quiet)
                        time.sleep(0.5)
                        ldtu.writeADC_ctrlReg(adcH=True, adcL=True, quiet=be_quiet)
                        adc_cal(ldtu, ADCH=False, ADCL=True, inv_phase=invert_phase)
                        N_cal_L += 1                     
               else:
                  if verbosity > 0:
                     print("ADC saturation Ok.")
                  next = True
                  break


   if not(next):
      wfgH.set_wfg("off")                 ## disable the DC waveform generator output
      wfgL.set_wfg("off")                 ## disable the DC waveform generator output
      print("\nCalibration failed after {} attempts\n\n".format(N))
      return -1, adc_h_mean, adc_h_stdv, adc_l_mean, adc_l_stdv, fitH_param, fitL_param, enob_H_fit, enob_H_fft, enob_L_fit, enob_L_fft, N, N_adc_bsln, N_adc_sine, N_cal_H, N_cal_L
   else:
      #wfgH.set_wfg("off")                ## disable the DC waveform generator output
      #wfgL.set_wfg("off")                ## disable the DC waveform generator output
      print("\nCalibration successful after {} attempts\n\n".format(N))
      #ldtu.read_iic_config(0, 0, 90, quiet=False)
      #ldtu.read_iic_config(1, 0, 90, quiet=False)
      #adc0_cfg = ldtu.read_iic_config(0, 0, 90)
      #adc1_cfg = ldtu.read_iic_config(1, 0, 90)
      #ldtu_cfg = ldtu.read_iic_config(2, 0, 26)
      return  0, adc_h_mean, adc_h_stdv, adc_l_mean, adc_l_stdv, fitH_param, fitL_param, enob_H_fit, enob_H_fft, enob_L_fit, enob_L_fft, N, N_adc_bsln, N_adc_sine, N_cal_H, N_cal_L

###########################################################################################################





###########################################################################################################

def bsln_test(path, ldtu, wfgH, wfgL, adc_bsln, save_mode, data_red, invert_phase=0b0, verbosity=0):
   ###############################################
   ## Baseline subtraction test and calibration
   ## adc_bsln = ADC target pedestal
   ##

   if verbosity > 1:
      be_quiet = False
   else:
      be_quiet = True

   adc_sub = -99
   do_fit  = True

   wfgH.set_wfg("bsln")                                  ## set waveform generator to baseline mode 
   wfgL.set_wfg("bsln")                                  ## set waveform generator to baseline mode
   ldtu.set_switch("write", "DTU")                       ## Set DTU-mode and enable switches for DC inputs (0,1,1,1,1)
   ldtu.dtu_mode(invert_phase=invert_phase, quiet=True)  ## Set DTU-mode
   time.sleep(1)
   fn = build_fn().split(".")[0]
   outfile = path + "bslnSub_{}.png".format(fn)
   outfile_txt = path + "bslnSub_{}.dat".format(fn)
   bsln_list = list()
   bsln_sub = [60, 70, 80, 90, 100]   ## List of baseline subtraction values
   for b in bsln_sub:
      bHex = hex(b)
      ldtu.writeBslnreg("H", bHex)                             ## write baseline subtraction register
      filename = build_fn()                                    ## generate filename to save the data acquired
      data_bsln = ldtu.acquire_data(1024)                      ## acquire data
      data_bsln.write_raw(path, filename, "bin")               ## save data in raw format
      bsln = pulse_signal(path + filename)                     ## prepare for plot
      ret = bsln.decode_data()                                 ## decode raw data
      if ret == 0:
         #bsln.check_samples(quiet=be_quiet)                   ## check samples (baseline, G10, G1)
         bsln_mean = bsln.check_bsln(quiet=be_quiet)           ## check baseline values
         bsln_list.append(bsln_mean)
         if data_red:
            bsln.delete_rawData(path, filename, quiet=be_quiet)
         #bsln.plot_pulse(isFit=False, show=False, isZoom=False)  ## plot signal
      else:
         if verbosity > 0:
            print("Error in decoding baseline data!!")
         do_fit = False
   if do_fit:
      adc_sub = bsln.bsln_fit(bsln_sub, bsln_list, adc_bsln)
      bsln.bsln_txt(bsln_sub, bsln_list, outfile_txt)
      if save_mode == 1:
         bsln.bsln_plot(bsln_sub, bsln_list, adc_sub, outfile, save_fig=False)
      elif save_mode == 2:
         bsln.bsln_plot(bsln_sub, bsln_list, adc_sub, outfile, save_fig=True)

      if verbosity > 0:
         print("\nBaseline subtraction = {}\n".format(adc_sub))
   else:
      print("Error while performing baseline subtraction scan!!")

   return adc_sub      ## Baseline subtraction value to get defined pedestal
   ##
   ########################################################################################

###########################################################################################################



###########################################################################################################

def acq_bsln(path, ldtu, wfgH, wfgL, adc_bsln, pll_opt=[None]):
   if len(pll_opt) == 3:
      n_cycle = pll_opt[0]
      pll_val = pll_opt[1]
      date = pll_opt[2]
      filename = "bsln_{}_{}_{}.txt".format(n_cycle, pll_val, date)
   else:
      filename = build_fn()                                  ## generate filename to save the data acquired
   wfgH.set_wfg("bsln")                                      ## set waveform generator to baseline mode 
   wfgL.set_wfg("bsln")                                      ## set waveform generator to baseline mode
   data_bsln = ldtu.acquire_data(10000)                      ## acquire data
   data_bsln.write_raw(path, filename, "bin")                ## save data in raw format
   bsln = pulse_signal(path + filename)                      ## prepare for plot
   bsln.decode_data()                                        ## decode raw data

###########################################################################################################



###########################################################################################################

def dtu_stability(path, ldtu, wfgH, wfgL, bslnSub, duration):
   ldtu.set_switch("write", "DTU")     ## Set DTU-mode and enable switches for DC inputs (0,1,1,1,1)
   ldtu.dtu_mode(invert_phase=0b0)     ## Set DTU-mode
   bHex = hex(bslnSub)
   ldtu.writeBslnreg("H", bHex)        ## write ADCH baseline subtraction register
   wfgH.set_wfg("bsln")                ## set waveform generator to baseline mode 
   wfgL.set_wfg("bsln")                ## set waveform generator to baseline mode

   print("\n\nRunning DTU stability test...")
   ret = ldtu.check_counters(total_time=duration, EoE=False)      ## return number of CRC errors
   print("\n\nDTU stability test has ended.")
   return ret

###########################################################################################################



###########################################################################################################

def dtu_PLL_CRC_cycles(path, chip_num, ps, ldtu, wfgH, wfgL, cfgfile, bslnSub, duration=2, N_cycles=1, start=12, stop=128, dtu_ver=3.0):
   res_dict = dict()
   err_list = list()
   csv_folder = path + 'stb_test/outputs/'
   os.makedirs(csv_folder, exist_ok=True)  
   save_path = path + 'stb_test/{}/'.format(chip_num)
   os.makedirs(save_path, exist_ok=True)  

   for n_cycle in range(N_cycles):
      print("\n\n\n*****************************")
      print("**       Cycle n.{}        **".format(n_cycle))
      print("*****************************\n\n\n")
      
      print("Chip power ON")
      ps.power_on()
      
      time.sleep(1)
      ldtu.set_switch("write", "ATM")                    ## set switches to 'Test-mode' (1,0,0,0,0)
      
      print("Chip reset")
      ldtu.reset_full(invert_phase=0b0, version=dtu_ver) ## full reset
      
      print("Chip I2C config")
      ldtu.cu_config(cfgfile)                            ## config dtu
      if "manual" not in cfgfile:
         print("\nWARNING: the selected cfg file seems to be not set for PLL manual lock")
         print("PLL manual lock will be set now...\n")
      print("Setting PLL Manual Lock to perform stability test with VCO-Cap scan")
      ldtu.setManLock(version=dtu_ver, quiet=False)
      time.sleep(0.5)

      adc_cal(ldtu, wfgH, wfgL)

      print("\n\nStarting PLL scan...\n\n")
      #scan_dict = dtu_PLL_CRC(path, ps, ldtu, wfgH, wfgL, bslnSub, duration, n_cycle, start=63, stop=121, EoE=True)  
      scan_dict = dtu_PLL_CRC(path, ps, ldtu, wfgH, wfgL, bslnSub, duration, chip_num, n_cycle, start, stop, EoE=False)  
      print("\n\nPLL scan has ended...\n\n")
      if type(scan_dict) == int:
         print("\n\nCRC error detected during pll scan: stopping test\n\n")
         break
      else:   
         res_dict[n_cycle] = scan_dict
         print("Chip power OFF")
         ps.power_off()                                        ## INFN power supply
         print("Cycle n.{} has ended".format(n_cycle))
         for x in scan_dict["crc_err"].items():
            if x[1] > 0:
               err_list.append(n_cycle)
               break
         print("\n\nList of power cycles with CRC errors: {}\n\n".format(err_list))
         time.sleep(1)
   
   for n in res_dict.keys():
      #print(n)
      df1 = pd.DataFrame.from_dict(res_dict[n]["crc_err"], orient='index', columns=["crc"])
      df2 = pd.DataFrame.from_dict(res_dict[n]["bit_shift"], orient='index', columns=["bit"])
      df  = pd.concat([df1, df2], axis=1)
      df["N"] = n
      if n > 0:
         df_out = pd.concat([df_out, df])
      else:
         df_out = df
   df_out = df_out.reset_index()
   df_out = df_out.rename(columns = {'index':'pll'})
   df_out = df_out[["N", "pll", "crc", "bit"]]
   #print(df_out)
   df_out.to_csv(csv_folder + 'chip{}_out.csv'.format(chip_num))

   fig = plt.figure(figsize=(20,5))
   xi = list(range(len(res_dict[0]["crc_err"].keys())))
   for n in res_dict.keys():
      plt.plot(xi, res_dict[n]["crc_err"].values(), 'o--', label="{}".format(n))
   plt.title("PLL-CRC Scan")
   plt.xlabel("VCO cap sel")
   plt.ylabel("CRC errors")
   plt.xticks(xi, scan_dict["crc_err"].keys())
   plt.grid()
   plt.legend()
   fig.tight_layout()

   fig2 = plt.figure(figsize=(20,5))
   xi = list(range(len(res_dict[0]["bit_shift"].keys())))
   for n in res_dict.keys():
      plt.plot(xi, res_dict[n]["bit_shift"].values(), 'o--', label="{}".format(n))
   plt.title("PLL-CRC Scan")
   plt.xlabel("VCO cap sel")
   plt.ylabel("bit shift")
   plt.xticks(xi, scan_dict["bit_shift"].keys())
   plt.grid()
   plt.legend()
   fig2.tight_layout()

   plt.show()
      
###########################################################################################################



###########################################################################################################
   
def dtu_PLL_CRC(path, ps, ldtu, wfgH, wfgL, bslnSub, duration=10, chip_num=-1, n_cycle=0, start=12, stop=128, EoE=False):
   
   scan_dict = {"crc_err": {}, "bit_shift": {}}
   cnt_6 = 0                     ## 6-bit Digital Thermometer Code (MSB)
   cnt_3 = 0                     ## 3-bit Binary Code (LSB)
   save_path = path + 'stb_test/{}/'.format(chip_num)

   for ith in range(7):          ## 7 cycles to test all 6-bit Digital Thermometer codes

      for ibin in range(8):      ## 8 cycles to test all 3-bit Binary codes
         
         vcoCapSelect = "{:06b}{:03b}".format(cnt_6, cnt_3)    ## 'MSB' + 'LSB'
         cnt_3 = cnt_3 + 1
         reg0f = hex(int("{:07b}".format(0) + vcoCapSelect[0], 2))   ## Register 0f -> '0000000' + MSB of vcoCapSelect
         reg10 = hex(int(vcoCapSelect[1:], 2))                       ## Register 10 -> remaining bits of vcoCapSelect
         #print(reg0f, reg10)
         pll_val = int(vcoCapSelect, 2)
   
         if (pll_val < start) or (pll_val > stop):
            continue
         
         print("\n\n\n************************")
         print("************************")
         print("  NEW PLL value\n")

         print("Running stability test for PLL VCO Cap sel = {}\n".format(pll_val))
         crc_err = -1
         bit_shift = -1
         wfgH.set_wfg("off")                 ## disable the DC waveform generator output
         wfgL.set_wfg("off")                 ## disable the DC waveform generator output         
         ldtu.set_switch("write", "ATM")     ## set switches to 'Test-mode' (1,0,0,0,0)
         ldtu.sync_mode(invert_phase=0b0)
         ldtu.writePLLreg([reg0f, reg10])
         
         ldtu.reset_pll(invert_phase=0b0, version=3.0)
         time.sleep(0.5)
         ldtu.reset_dtu(invert_phase=0b0, version=3.0)
         
         time.sleep(0.5)
         ret = ldtu.eye_align()
         if ret[0] != 0:
            scan_dict["crc_err"][pll_val] = crc_err
            scan_dict["bit_shift"][pll_val] = bit_shift
            continue
         ret = ldtu.align_data()
         if ret != 0:
            scan_dict["crc_err"][pll_val] = crc_err
            scan_dict["bit_shift"][pll_val] = bit_shift
            continue
         bit_shift = ldtu.get_bit_shift()
         ldtu.dtu_mode(invert_phase=0b0)     ## Set DTU-mode
         ldtu.dtu_mode(invert_phase=0b0)     ## Set DTU-mode (repeat twice to try to avoid missed command)
         time.sleep(0.5)
         crc_err = dtu_stability(path, ldtu, wfgH, wfgL, bslnSub, duration)      ## number of CRC errors
         print("\n\nCRC errors = {}\n\n".format(crc_err))
         scan_dict["crc_err"][pll_val] = crc_err
         scan_dict["bit_shift"][pll_val] = bit_shift
         print("CRC errors dict: {}".format(scan_dict["crc_err"]))
         print("Data alignment bit shift dict: {}".format(scan_dict["bit_shift"]))
         
         if (crc_err > 0) and EoE:
            return 1
         frameDiff = ldtu.get_frameDiff()
         timeDiff  = ldtu.get_timeDiff()
         diff_limit = int(640E3 * timeDiff * 0.9)    ## expected frame words rate = 640k frames per second
         print("\nDIFF: {} (frame number diff) vs {} (MIN frame number diff)\n".format(frameDiff, diff_limit))
         if crc_err > 0:
            date = build_fn()[:-4]
            filename = "{}chipscope_crc_{}_{}_{}.csv".format(save_path, n_cycle, pll_val, date)
            os.system("rm -v hw_ila_data_*.btree")
            os.system("rm -v vivado*.*")
            os.system("rm -rv .Xil/")
            os.system("vivado -mode batch -source test/tcl/dbg_start_crc.tcl -tclargs {}".format(filename))
            headErr = ldtu.get_headerErrors()
            if headErr > 0:
               date = build_fn()[:-4]
               print("Found header errors!!!")
               filename = "{}chipscope_header_{}_{}_{}.csv".format(save_path, n_cycle, pll_val, date)
               os.system("vivado -mode batch -source test/tcl/dbg_start_header.tcl -tclargs {}".format(filename))
         if frameDiff < diff_limit:
            print("frame diff = {}".format(frameDiff))
            print("Acquiring 10000 baseline data words")
            date = build_fn()[:-4]
            acq_bsln(save_path, ldtu, wfgH, wfgL, adc_bsln=-1, pll_opt=[n_cycle, pll_val, date])
            """
            date = build_fn()[:-4]
            filename = "{}chipscope_frame_{}_{}_{}.csv".format(save_path, n_cycle, pll_val, date)
            os.system("vivado -mode batch -source test/tcl/dbg_start.tcl -tclargs {}".format(filename))
            """
      cnt_3 = 0
      cnt_6 = cnt_6 + 2**ith
     
   with open(save_path + 'CRC_errros_{}.dat'.format(n_cycle), 'w') as f:
      for k in scan_dict["crc_err"].keys(): 
         f.write("{}\t{}\n".format(k, scan_dict["crc_err"][k]))
   with open(save_path + 'bit_shift_{}.dat'.format(n_cycle), 'w') as f:
      for k in scan_dict["bit_shift"].keys(): 
         f.write("{}\t{}\n".format(k, scan_dict["bit_shift"][k]))

   os.system("rm -v hw_ila_data_*.btree")
   os.system("rm -v vivado*.*")
   os.system("rm -rv .Xil/")

   return scan_dict
   
###########################################################################################################



###########################################################################################################

def dtu_test(path, ldtu, wfgH, wfgL, wfg_opt, n_data_pulse, N_pul, save_mode, data_red, invert_phase=0b0, verbosity=0):

   if verbosity > 1:
      be_quiet = False
   else:
      be_quiet = True

   #ldtu.read_iic_config(0, 0, 90, quiet=False)
   #ldtu.read_iic_config(1, 0, 90, quiet=False)

   #############################################################################################
   #############################################################################################
   """
   wfgH.set_wfg("calib")               ## set waveform generator to calibration mode
   wfgL.set_wfg("calib")               ## set waveform generator to calibration mode
   ldtu.set_switch("write", "DC")      ## enable switches for DC inputs (1,1,1,1,1)

   time.sleep(0.8)

   ## Run ADC calibration
   adc_cal(ldtu, ADCH=True, ADCL=True, inv_phase=invert_phase)
   time.sleep(0.5)
   """
   #############################################################################################
   #############################################################################################


   ##################################################################################
   ## Pulse signals test
   ##
   ldtu.set_switch("write", "DTU")                       ## Set DTU-mode and enable switches for DC inputs (0,1,1,1,1)
   ldtu.dtu_mode(invert_phase=invert_phase,quiet=True)   ## Set DTU-mode


   bslnSubReg = "0x00"
   #bslnSubReg = "0xff"                    ## FIX-ME!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ldtu.writeBslnreg("H", bslnSubReg)                        ## write ADCH baseline subtraction register
   ldtu.writeBslnreg("L", bslnSubReg)                        ## write ADCL baseline subtraction register
   #ldtu.writeAdcSatReg(bslnSubReg)


   ###########################################################################################
   #ldtu.writeAdcDivReg(4)     ## set 2 or 4 to divide ADC_H output values by 2 or 4
   ###########################################################################################

   bsln_C1, bsln_C2, norm_ampl, sat_ampl, delay_H1, delay_H2, delay_L1, delay_L2 = wfg_opt

   Na = 0
   Nb = 0
   g10_dict = dict()
   g1_dict = dict()
   g10_dict["N_tot"], g10_dict["N_bsln"], g10_dict["N_G10"], g10_dict["N_G1"] = -99, -99, -99, -99
   g1_dict["N_tot"], g1_dict["N_bsln"], g1_dict["N_G10"], g1_dict["N_G1"] = -99, -99, -99, -99
   g10_dict["max"], g10_dict["bsln"], g10_dict["ampl"], g10_dict["slope"], g10_dict["t0"] = -99, -99, -99, -99, -99
   g1_dict["max"], g1_dict["bsln"], g1_dict["ampl"], g1_dict["slope"], g1_dict["t0"] = -99, -99, -99, -99, -99

   for g in ["normal", "saturation"]:     ## repeat acquisition for 2 different gain selection
      next = False

      #if g == "saturation":
         #ldtu.writeAdcSatReg()                  ## FIX-ME!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         #bslnSubReg = "0x8c"
         #bslnSatReg = "0xfa"
         #ldtu.writeBslnreg("H", bslnSubReg)          ## write ADCH baseline subtraction register
         #ldtu.writeBslnreg("L", bslnSubReg)          ## write ADCL baseline subtraction register
         #ldtu.writeAdcSatReg(bslnSatReg)

      for n in range(N_pul):

         N = n + 1

         #bsln_C1   =  -0.700         # (was -0.530) decreased to lower baseline
         #bsln_C2   =  -1.400         # (was -1.225) decreased to lower baseline
         #norm_ampl =   1.900         # (was 1.8)
         #sat_ampl  =   3.500         # (was 2.5)

         ###########################################################
         high_level_1  = bsln_C1 + norm_ampl
         high_level_2  = bsln_C2 + norm_ampl
         ofst1 = (high_level_1 + bsln_C1) / 2
         ofst2 = (high_level_2 + bsln_C2) / 2
         ampl1 = norm_ampl
         ampl2 = norm_ampl
         ###########################################################

         wfgL.set_wfg("pulse", ampl1=ampl1, ofst1=ofst1, ampl2=ampl2, ofst2=ofst2)  ## set parameters for ADCL input pulse

         ###########################################################
         if g == "normal":
            Na = N
         else:                ## saturation settings
            Nb = N
            ampl = sat_ampl
            high_level_1  = bsln_C1 + ampl
            high_level_2  = bsln_C2 + ampl
            ofst1 = (high_level_1 + bsln_C1) / 2
            ofst2 = (high_level_2 + bsln_C2) / 2
            ampl1 = ampl
            ampl2 = ampl
         ###########################################################

         wfgH.set_wfg("pulse", ampl1=ampl1, ofst1=ofst1, ampl2=ampl2, ofst2=ofst2)  ## set parameters for ADCH input pulse (normal)
         
         filename = build_fn()                                             ## generate filename to save the data acquired

         wfgH.set_wfg_delay(delay_H1, delay_H2)                            ## set delay for sinchronization of ADCH input pulse wrt ADCH
         wfgL.set_wfg_delay(delay_L1, delay_L2)                            ## set delay for sinchronization of ADCL input pulse wrt ADCH
    
         wfgL.set_burst("ext")            ## set ADCL pulse generator trigger to external (from ADCH pulse generator)
         wfgH.set_burst("man")            ## set ADCH pulse generator trigger to manual
         time.sleep(1)
         wfgH.do_burst()                  ## start trigger for ADCH pulse generator
         time.sleep(0.5)

         data_pulse = ldtu.acquire_data(n_data_pulse)       ## acquire data
         data_pulse.write_raw(path, filename, "bin")        ## save data in raw format
         pul = pulse_signal(path + filename)                ## prepare for plot
         ret0 = pul.decode_data()                           ## decode raw data

         ret1 = pul.check_samples(pulse_type=g, quiet=be_quiet)       ## check samples (baseline, G10, G1)
         if g == "normal":
            __, g10_dict["N_tot"], g10_dict["N_bsln"], g10_dict["N_G10"], g10_dict["N_G1"] = ret1
         else: 
            __, g1_dict["N_tot"], g1_dict["N_bsln"], g1_dict["N_G10"], g1_dict["N_G1"] = ret1
         
         ret2 = pul.plot_pulse(save_mode, pulse_type=g, show=False, reduce_data=data_red, quiet=be_quiet)     ## plot and fit pulse signal
         if g == "normal":
            __, g10_dict["max"], g10_dict["bsln"], g10_dict["ampl"], g10_dict["width"], g10_dict["t0"] = ret2
         else: 
            __, g1_dict["max"], g1_dict["bsln"], g1_dict["ampl"], g1_dict["width"], g1_dict["t0"] = ret2
         
         if (save_mode == 2) and (ret2[0] != 1):
            pul.save_plot(gain=g)                              ## save plot
         if (ret0 != 0) or (ret1[0] != 0) or (ret2[0] != 0):
            if verbosity > 0:
               print("\nDecode: {}, Samples type: {}, Signal fit: {}".format(ret0, ret1[0], ret2[0]))
               print("Pulse signal not Ok !")
               print("Restarting Pulse Generation\n\n")
         else:
            if verbosity > 0:
               print("\nSuccessful CATIA-like pulse with signal type {} after {} attempts\n\n".format(g, N))
            next = True
            break
        
      if not(next):
         print("\nPulse signal test failed after {} and {} attempts\n\n".format(Na, Nb))
         return -1, Na, Nb, g10_dict, g1_dict

   if next:
      print("\nPulse signal test successful after {} and {} attempts\n\n".format(Na, Nb))
      return 0, Na, Nb, g10_dict, g1_dict

###########################################################################################################



###########################################################################################################

def tp_test(path, ldtu, N_ctp, save_mode, tp_step=2, invert_phase=0b0, verbosity=0):
   ##################################################################################
   ## CATIA test-pulse generation test: measure tp duration
   ##
   next = False
   for n in range(N_ctp):
      N = n + 1
      x = list()
      tp_list = list()
      for w in range(0, 256, tp_step):
         tp_time = ldtu.test_catiaTP(tp_width=w, inv_phase=invert_phase)
         x.append(w)
         tp_list.append(tp_time)
      fn = build_fn().split(".")[0]
      outfile = path + "CATIA_TP_{}.png".format(fn)
      ctp = plt_an()
      ret = ctp.check_catiaTP(x, tp_list, outfile, save_mode)
      slope, const = ret[1:]
      if ret[0] != 0:
         if verbosity > 0:
            print("CATIA TP not Ok !")
            print("Restarting CATIA test-pulse generation\n\n")
      else:
         tp_time = ldtu.test_catiaTP(tp_width=0, inv_phase=invert_phase)
         next = True
         break
   
   if not(next):
      print("\nCATIA test-pulse test failed after {} attempts\n\n".format(N))
      return -1, N, slope, const
   else:
      print("\nCATIA test-pulse test successful after {} attempts\n\n".format(N))
      return 0, N, slope, const

###########################################################################################################


###########################################################################################################

def result_summary(tests_passed, test_time):
   ctp = plt_an()
   ctp.final_result_plot(tests_passed, test_time)

###########################################################################################################



###########################################################################################################

def do_enob(path, inst, ldtu, n_data_enob, options):
   ##################################################################################
   ## ENOB measurements using Rhode & Schwarz waveform generator
   ##

   wfgH, wfgL, sma100b = inst
   enob_scan, enob_freq, enob_ver, enob_filt, enob_OCXO, enob_manLock, enob_clkInt, enob_adcH = options
   enob_opt = [ enob_ver, enob_filt, enob_OCXO, enob_manLock, enob_clkInt, enob_adcH ]


   ## Prepare for ADC calibration
   sma100b.disable_output()
   wfgH.set_wfg("calib")               ## set waveform generator to calibration mode
   wfgL.set_wfg("calib")               ## set waveform generator to calibration mode
   ldtu.set_switch("write", "DC")      ## enable switches for DC inputs (1,1,1,1,1)

   ## Run ADC calibration
   adc_cal(ldtu, ADCH=True, ADCL=True, inv_phase=0b0)

   ## Prepare for sine-wave
   wfgH.set_wfg("off")                 ## disable the DC waveform generator output
   wfgL.set_wfg("off")                 ## disable the DC waveform generator output
   ldtu.set_switch("write", "ATM")                 ## disable switches for DC inputs (1,0,0,0,0)
   sma100b.enable_output()

   if enob_filt:
      flt = "YES"
   else:
      flt = "NO"

   freq_dict = build_freq(filename="cfg/freq_table.txt", isFilt=flt)
      
   if enob_scan:
      f = "scan"     ## perform frequency scan
   else:
      f = get_freqParam(freq_dict, enob_freq, "Name")      ## test only frequency selected in autotest.ini file

   if f != "scan":
      fname = build_fn(freq_dict=freq_dict, enob=True, options=[enob_opt, f])
      print("\nPreparing for frequency {}...\nData will be saved as {}".format(enob_freq, fname))
      sma100b.setfrequency(get_freqParam(freq_dict, enob_freq, "Val"))
      sma100b.setamplitude(get_freqParam(freq_dict, enob_freq, "Ampl"))
      time.sleep(1)
      print("Acquiring data...")
      data = ldtu.acquire_data(n_data_enob)
      print("Done.")
      data.write_decoded(path, fname, switch_ADCs=not(enob_adcH))
      fn = path + fname
      sin_enob = sine_wave(fn)
      #sin_enob.plot_sine(zoom=True)
      ret = sin_enob.do_fit(0, "H")
      if ret[0] == 1:
         print("Bad sinewave fit, test aborted.\n\n")
         sys.exit()
         
      __, yfit, ydata, fit_param = ret
      enob = sin_enob.enob_from_fit(yfit, ydata)
      print("\n\tENOB = {:.2f} bit\n".format(enob))

      enob_H1, enob_H2, maxFreq = sin_enob.enob_from_fft("ADCH", is_plot=True, Ncut=1, quiet=False)    ## Evaluate ENOB also from FFT
      print("\n\tENOB  = {:.2f} bit".format(enob_H1))
      print("\n\tENOB2 = {:.2f} bit".format(enob_H2))

   else:
      preset_freqs, filenames = build_fn(freq_dict=freq_dict, enob=True, options=[enob_opt, f])

      freq_list = []
      enobFIT_list = []
      enobFFT_list = []
      
      for freq, fname in zip(preset_freqs, filenames):
         print("\nPreparing for frequency {}, data will be saved as {}".format(freq, fname))
         sma100b.setfrequency(get_freqParam(freq_dict, freq, "Val"))
         sma100b.setamplitude(get_freqParam(freq_dict, freq, "Ampl"))   ## increase for v3.0
         time.sleep(1)
         print("Acquiring data...")
         data = ldtu.acquire_data(n_data_enob)
         print("Done.")
         #data.write_raw(fname)
         data.write_decoded(path, fname, switch_ADCs=not(enob_adcH))
         fn = path + fname
         sin_enob = sine_wave(fn)
         #sin_enob.plot_sine(zoom=True)
         ret = sin_enob.do_fit(0, "H")
         if ret[0] == 1:
            print("Bad sinewave fit, test aborted.\n\n")
            sys.exit()
            
         __, yfit, ydata, fit_param = ret
         enob = sin_enob.enob_from_fit(yfit, ydata)
         print("\n\tENOB_fit = {:.2f} bit\n".format(enob))

         freq_list.append(sin_enob.get_input_freq())
         enobFIT_list.append(round(enob,2))

         enob_H1, enob_H2, maxFreq = sin_enob.enob_from_fft("ADCH", is_plot=True, Ncut=1, quiet=False)    ## Evaluate ENOB also from FFT
         print("\n\tENOB_fft  = {:.2f} bit".format(enob_H1))
         enobFFT_list.append(round(enob_H1,2))
         #plt.show()

      print("freq = {}".format(freq_list))
      print("enobFIT = {}".format(enobFIT_list))
      print("enobFFT = {}".format(enobFFT_list))
      sin_enob.plot_enob(freq_list, enobFIT_list, enobFFT_list)

   sma100b.disable_output()


###########################################################################################################











