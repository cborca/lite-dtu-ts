import matplotlib.pyplot as plt
import pandas as pd
import os, sys  
import glob

#from test_functions import *
#from analysis_lib import pll_an, sine_wave, pulse_signal

chip_num = int(sys.argv[1])

path = 'data/pll/stb_test/'

data_path = path + str(chip_num)
out_path = path + "outputs"

files = glob.glob(data_path + '/CRC_err*.dat')

df_list = list()
for filename in files:
   print(filename)
   n = int(filename.split(".")[0].split("_")[-1])
   #print(n)
   df_crc = pd.read_csv(filename, sep='\t', header=None, names=["pll", "crc"])
   df_bit = pd.read_csv(data_path + "/bit_shift_{}.dat".format(n), sep='\t', header=None, names=["pll", "bit"])
   df_crc["bit"] = df_bit.bit.values
   df_crc["N"] = [n for i in range(len(df_crc.crc.values))]
   df_list.append(df_crc)

df_out = pd.concat(df_list)
df_out = df_out[["N", "pll", "crc", "bit"]]
print(df_out)
os.makedirs(out_path, exist_ok=True)  
df_out.to_csv(out_path + '/chip{}_out.csv'.format(chip_num))



