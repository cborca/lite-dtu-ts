import os
import numpy as np
import configparser
from datetime import datetime

from SocketClientClass import tcp_client as SockClientTCP           ## Added by Deiana


###########################################################################################################
###########################################################################################################





###########################################################################################################

def load_settings(INFN_mode = False):
   #####################################################################
   ## Load settings from .ini file
   ##
   config = configparser.ConfigParser()
   if INFN_mode:
      config.read(r"cfg/autotest_infn.ini")
   else:
      config.read(r"cfg/autotest.ini")

   ##
   dtu_ver   = config.getfloat('mode', 'dtu_ver')
   infn      = config.getboolean('mode', 'infn')
   offline   = config.getboolean('mode', 'offline')
   verbosity = config.getint('mode', 'verbosity')        ## 0: don't print; 1: small debug printout during test + report at the end; 2: print full debug during test               
   save_mode = config.getint('mode', 'save_mode')        ## 0: don't plot, don't save figures; 1: plot figures but don't save, 2: plot and save figures
   data_red  = config.getboolean('mode', 'data_red')     ## reduce data saved (txt files)
   ##
   inv_phase = config.getboolean('resync', 'inv_phase')                 ## Invert resync command phase
   ##
   keysight_ps_ipaddr = config.get('inst', 'keysight_ps_ipaddr')        ## Commented by Deiana
   sgc635_PortVID = config.get('inst', 'sgc635_PortVID')                ## Read Port vid for auto search port
   sgc635_PortPID = config.get('inst', 'sgc635_PortPID')                ## Read Port pid for auto search port
   scg635_connect  = config.get('inst', 'scg635_connect')               ## Read type of connection
   wfg_calibH_ipaddr  = config.get('inst', 'wfg_calibH_ipaddr' )
   wfg_calibL_ipaddr  = config.get('inst', 'wfg_calibL_ipaddr' )
   wfg_sin_HL_ipaddr  = config.get('inst', 'wfg_sin_HL_ipaddr' )
   rs_sma100b_ipaddr  = config.get('inst', 'rs_sma100b_ipaddr' )
   ##
   Iref = [ config.getfloat('curr', 'I1'), config.getfloat('curr', 'I2'), config.getfloat('curr', 'I3') ] 
   tol  =   config.getfloat('curr', 'tol')
   ##
   delay_0   = config.getint('align', 'Idelay_0')        ## Extra tap for dout0
   delay_1   = config.getint('align', 'Idelay_1')        ## Extra tap for dout1
   delay_2   = config.getint('align', 'Idelay_2')        ## Extra tap for dout2
   delay_3   = config.getint('align', 'Idelay_3')        ## Extra tap for dout3
   align_dbg = config.getboolean('align', 'align_dbg')   ## Perform eye align scan on all 4 lanes
   ##
   cfgfile      = "cfg/" + config.get('daq', 'cfg_file')
   n_data_sin   = config.getint('daq', 'n_data_sin')
   n_data_enob  = config.getint('daq', 'n_data_enob')
   n_data_pulse = config.getint('daq', 'n_data_pulse')
   ##
   N_ini     = config.getint('daq', 'N_ini')        ## Maximum number of attempts for LiTE-DTU init (power-on, config, currents)
   N_pll     = config.getint('daq', 'N_pll')        ## Maximum number of attempts for LiTE-DTU pll lock
   N_ali     = config.getint('daq', 'N_ali')        ## Maximum number of attempts for LiTE-DTU links alignment
   N_cal     = config.getint('daq', 'N_cal')        ## Maximum number of attempts for ADC calibration
   N_pul     = config.getint('daq', 'N_pul')        ## Maximum number of attempts for Pulse signal
   N_ctp     = config.getint('daq', 'N_ctp')        ## Maximum number of attempts for CATIA test-pulse
   ##
   long_calib = config.getboolean('adc', 'long_calib')   ## Enable ADCs long calibration
   bsln_mean  = config.getfloat('adc', 'bsln_mean')      ## Mean value of ADC baseline to assess calibration
   std_max    = config.getfloat('adc', 'std_max')        ## Maximum value for std of ADC baseline to assess calibration
   enob_min   = config.getfloat('adc', 'enob_min')       ## Minimum value for ENOB to assess ADC calibration
   adc_bsln   = config.getint('adc', 'adc_bsln')         ## ADC baseline target value
   ##
   bsln_C1   = config.getfloat('wfg', 'bsln_C1')      ## WFG low level setting for channel 1 (not inverted)
   bsln_C2   = config.getfloat('wfg', 'bsln_C2')      ## WFG low level setting for channel 2 (inverted)
   norm_ampl = config.getfloat('wfg', 'norm_ampl')    ## WFG amplitude setting for non-saturated pulse
   sat_ampl  = config.getfloat('wfg', 'sat_ampl')     ## WFG amplitude setting for saturated pulse
   delay_H1  = config.get('wfg', 'delay_H1')          ## WFG delay setting for WFGH channel 1
   delay_H2  = config.get('wfg', 'delay_H2')          ## WFG delay setting for WFGH channel 2
   delay_L1  = config.get('wfg', 'delay_L1')          ## WFG delay setting for WFGL channel 1
   delay_L2  = config.get('wfg', 'delay_L2')          ## WFG delay setting for WFGL channel 2
   ##
   tp_step = config.getint('tp', 'tp_step')              ## CATIA test-pulse step
   ##
   enob_meas    = config.getboolean('enob', 'enable')    ## enable detailed ENOB measurements
   enob_scan    = config.getboolean('enob', 'scan')      ## perform frequency scan
   enob_freq    = config.get('enob', 'freq')             ## select frequency
   enob_ver     = config.get('enob', 'ver')              ## select LiTE-DTU version
   enob_filt    = config.getboolean('enob', 'filt')      ## select filter
   enob_OCXO    = config.getboolean('enob', 'OCXO')      ## select clock generator
   enob_manLock = config.getboolean('enob', 'manLock')   ## select pll lock mode (manual or auto)
   enob_clkInt  = config.getboolean('enob', 'clkInt')    ## select clock source (internal or external)
   enob_adcH    = config.getboolean('enob', 'adcH')      ## select ADC (HighGain or LowGain)
   ##
   stb_en       = config.getboolean('stb', 'stb_en')     ## enable stability test
   pll_start    = config.getint('stb', 'pll_start')      ## first value for pll scan stability test
   pll_stop     = config.getint('stb', 'pll_stop')       ## last value for pll scan stability test
   n_cycles     = config.getint('stb', 'n_cycles')       ## number of power cycles for stability test
   time_step    = config.getint('stb', 'time_step')      ## time for each step of pll scan
   ##
   #####################################################################

   mode = [dtu_ver, infn, offline, verbosity, save_mode, data_red]
   inst = [keysight_ps_ipaddr, sgc635_PortVID, sgc635_PortPID, scg635_connect, wfg_calibH_ipaddr, wfg_calibL_ipaddr, wfg_sin_HL_ipaddr, rs_sma100b_ipaddr]
   opt  = [align_dbg, inv_phase]
   curr = [Iref, tol]
   idel = [delay_0, delay_1, delay_2, delay_3]
   daq  = [cfgfile, n_data_sin, n_data_enob, n_data_pulse, N_ini, N_pll, N_ali, N_cal, N_pul, N_ctp]
   adc  = [long_calib, bsln_mean, std_max, enob_min, adc_bsln]
   wfg  = [bsln_C1, bsln_C2, norm_ampl, sat_ampl, delay_H1, delay_H2, delay_L1, delay_L2]
   tp   = [tp_step]
   enob = [enob_meas, enob_scan, enob_freq, enob_ver, enob_filt, enob_OCXO, enob_manLock, enob_clkInt, enob_adcH]
   stb  = [stb_en, pll_start, pll_stop, n_cycles, time_step]
   
   return mode, inst, opt, curr, idel, daq, adc, wfg, tp, enob, stb

###########################################################################################################



###########################################################################################################

def check_folders(chip_folder, stb_en, chip_num):
   ##################################################################
   ## Prepare folders
   ##
   if chip_folder and not(stb_en):
      now = datetime.now()                                              ## Added by Deiana
      l = []                                                            ## Added by Deiana
      l.append(now.strftime("%Y"))                                      ## Added by Deiana
      l.append(now.strftime("%m"))                                      ## Added by Deiana
      l.append(now.strftime("%d"))                                      ## Added by Deiana
      l.append(now.strftime("%H%M%S"))                                  ## Added by Deiana
      foldername = "_".join(l)                                           ## Added by Deiana
      #save_folder  = "outputs/mass_test/{0:0>5}".format(chip_num)      ## Commented by Deiana
      save_folder  = "outputs/mass_test/{}".format(foldername)          ## Added by Deiana
      save_folder = save_folder + "__{0:0>5}".format(chip_num)           ## Edit by Deiana
      raw_folder   = save_folder + "/raw"
      dec_folder   = save_folder + "/decoded"
      pll_folder   = save_folder + "/pll"
      extra_folder = save_folder + "/extra"
   else:
      save_folder  = "data"
      raw_folder   = save_folder + "/raw"
      dec_folder   = save_folder + "/decoded"
      pll_folder   = save_folder + "/pll"
      extra_folder = save_folder + "/extra"

   if not(os.path.isdir(save_folder)):
      os.mkdir(save_folder)
   if not(os.path.isdir(raw_folder)):
      os.mkdir(raw_folder)
   if not(os.path.isdir(dec_folder)):
      os.mkdir(dec_folder)
   if not(os.path.isdir(pll_folder)):
      os.mkdir(pll_folder)
   if not(os.path.isdir(pll_folder+"/pll_scan")):
      os.mkdir(pll_folder+"/pll_scan")
   if not(os.path.isdir(extra_folder)):
      os.mkdir(extra_folder)

   rawDir = raw_folder + "/"
   decDir = dec_folder + "/"
   pllDir = pll_folder + "/"
   extraDir = extra_folder + "/"

   return rawDir, decDir, pllDir, extraDir, save_folder     ## FC 20.11.2023: added save_folder
   ##
   ##################################################################

###########################################################################################################



###########################################################################################################

def offline_currents(ref_values):
   #####################################################################
   ## Generate random currents value for offline mode
   ##
   curr = list()
   for i, val in enumerate(ref_values):
      curr.append(np.random.normal(val, val*0.03))
   return curr

###########################################################################################################






###########################################################################################################

def check_curr(curr, ref_values, margin):
   #####################################################################
   ## Compare currents wrt reference values
   ##
   ret = "OK"
   for i in range(len(ref_values)):
      if abs(curr[i] - ref_values[i]) / ref_values[i] > margin:
         ret = 1
         break
   return ret

###########################################################################################################





###########################################################################################################

def check_cfg(ldtu_cfg, cfgfile, Reg10=-1, quiet=False):
   #####################################################################
   ## Compare read configuration wrt config file
   ##
   ret = 0

   cfg_read = dict()
   for r in ldtu_cfg:
      cfg_read[r[0:2]] = r[-2:]
   
   cfg_write = dict()
   with open(cfgfile, "r") as f:
      lines = f.readlines()
      for i, l in enumerate(lines):
         row = l.split(" ")[0:2]
         reg = row[0]
         val = row[1]
         cfg_write[reg] = val
   if Reg10 != -1:
      cfg_write['10'] = Reg10

   if "autoLock" in cfgfile:
      skip_reg10 = True
      if not(quiet):
         print("PLL autolock enabled: skip check on PLL vcoCapSelect registers (addr = 0x10)")
   else:
      skip_reg10 = False

   for reg in cfg_write.keys():
      if not ((int(reg, 16) == 9) or (int(reg, 16) == 10)):
         if (int(reg, 16) != 16) or not(skip_reg10):
            if cfg_read[reg]!=cfg_write[reg]:
               print("Error in DTU configuration at register {}".format(reg))
               print("Read: {}, Expected: {}".format(cfg_read[reg], cfg_write[reg]))
               ret = 1

   return ret

###########################################################################################################






###########################################################################################################

def check_baseline(adc_h_mean, adc_h_stdv, adc_l_mean, adc_l_stdv, bsln_mean=340, std_max=2):
   #####################################################################
   ## Compare ADC baseline values wrt reference values
   ##
   ret_H = 0
   ret_L = 0
   if (adc_h_stdv > std_max):
      print("ADC H baseline std too high!!!")
      ret_H = 1
   if (adc_l_stdv > std_max):
      print("ADC L baseline std too high!!!")
      ret_L = 1
   if ( abs(adc_h_mean - bsln_mean) > 10 ):
      print("\nADC H baseline wrong value: {}\nReference value = {}\n".format(adc_h_mean, bsln_mean))
      ret_H = 1
   if ( abs(adc_l_mean - bsln_mean) > 10 ):
      print("\nADC L baseline wrong value: {}\nReference value = {}\n".format(adc_l_mean, bsln_mean))
      ret_L = 1

   return ret_H, ret_L

###########################################################################################################


def pllReg_values(pllReg_in):
   bin_val = bin( (int(pllReg_in[0], 16) << 8) + (int(pllReg_in[1], 16)) )[2:].zfill(9)
   cnt_6 = 0               ## 6-bit Digital Thermometer Code (MSB)
   cnt_3 = 0               ## 3-bit Binary Code (LSB)
   n = 0
   stop_loop = False
   for ith in range(7):                                        ## 7 cycles to test all 6-bit Digital Thermometer codes
      for ibin in range(8):                                    ## 8 cycles to test all 3-bit Binary codes
         vcoCapSelect = "{:06b}{:03b}".format(cnt_6, cnt_3)    ## 'MSB' + 'LSB'
         reg0f = "{:07b}".format(0) + vcoCapSelect[0]          ## Register 0f -> '0000000' + MSB of vcoCapSelect
         reg10 = vcoCapSelect[1:]                              ## Register 10 -> remaining bits of vcoCapSelect
         regs  = reg0f[-1] + reg10
         if bin_val == regs:
            stop_loop = True
         
         if stop_loop:
            break
         else:
            n = n + 1
            cnt_3 = cnt_3 + 1
      
      if stop_loop:
         break
      else:
         cnt_3 = 0
         cnt_6 = cnt_6 + 2**ith

   return n, bin_val




###########################################################################################################

def offline_pll():
   #####################################################################
   ## Generate random PLL manual lock scan values for offline mode
   ##
   r = np.random.rand()
   if r < 0.8:
      pll_dict = {'000000000': 11942, '000000001': 12278, '000000010': 9843, '000000011': 9888, '000000100': 12681, '000000101': 8586, '000000110': 6045, '000000111': 6050, '000001000': 11005, '000001001': 12944, '000001010': 5571, '000001011': 15924, '000001100': 9574, '000001101': 8838, '000001110': 9193, '000001111': 9015, '000011000': 3954, '000011001': 11269, '000011010': 10218, '000011011': 10332, '000011100': 13907, '000011101': 19801, '000011110': 0, '000011111': 0, '000111000': 0, '000111001': 0, '000111010': 0, '000111011': 0, '000111100': 0, '000111101': 0, '000111110': 18428, '000111111': 11508, '001111000': 17199, '001111001': 4875, '001111010': 10684, '001111011': 10579, '001111100': 10803, '001111101': 8661, '001111110': 9696, '001111111': 9593, '011111000': 3894, '011111001': 5750, '011111010': 9542, '011111011': 5967, '011111100': 9077, '011111101': 9173, '011111110': 5927, '011111111': 5662, '111111000': 11528, '111111001': 5427, '111111010': 9062, '111111011': 9278, '111111100': 8680, '111111101': 9254, '111111110': 7602, '111111111': 7684}
   else:
      pll_dict = {'000000000': 11942, '000000001': 12278, '000000010': 9843, '000000011': 9888, '000000100': 12681, '000000101': 8586, '000000110': 6045, '000000111': 6050, '000001000': 11005, '000001001': 12944, '000001010': 5571, '000001011': 15924, '000001100': 9574, '000001101': 8838, '000001110': 9193, '000001111': 9015, '000011000': 3954, '000011001': 11269, '000011010': 10218, '000011011': 10332, '000011100': 13907, '000011101': 19801, '000011110': 0, '000011111': 0, '000111000': 100, '000111001': 0, '000111010': 500, '000111011': 0, '000111100': 0, '000111101': 0, '000111110': 18428, '000111111': 11508, '001111000': 17199, '001111001': 4875, '001111010': 10684, '001111011': 10579, '001111100': 10803, '001111101': 8661, '001111110': 9696, '001111111': 9593, '011111000': 3894, '011111001': 5750, '011111010': 9542, '011111011': 5967, '011111100': 9077, '011111101': 9173, '011111110': 5927, '011111111': 5662, '111111000': 11528, '111111001': 5427, '111111010': 9062, '111111011': 9278, '111111100': 8680, '111111101': 9254, '111111110': 7602, '111111111': 7684}
      
   return pll_dict

###########################################################################################################







###########################################################################################################

def build_freq(filename="cfg/freq_table.txt", isFilt="NO"):
   #####################################################################
   ## Create frequency dictionary from file
   ##
   freq_dict = dict()
   rf = np.loadtxt(filename, dtype="U", skiprows=2, unpack=False, comments="%")
   for l in rf:
      if l[3] == isFilt:         ## select only rows with or without filter
         n = l[1].split(".")
         name = n[0] + "_" + n[1][ : -3]
         freq_dict[l[0]] = {"Val": l[1], "Ampl": l[2], "Filter": l[3], "Name": name}
   return freq_dict


def get_freqParam(freq_dict, f, param=None):
   #####################################################################
   ## Extract frequency from frequency dictionary
   ##
   #print("\n\n*********************************")
   #print(type(freq_dict))
   #print(freq_dict)
   #print(type(f))
   #print(f)
   try:
      temp = freq_dict[f]
   except:
      print("Selected frequency ({}) not available".format(f))
      print("Please select one from the following list:")
      preset_freqs = list(freq_dict.keys())
      print(preset_freqs)
      return 1

   if param == None:
         return temp
   else:
      try:
         return temp[param]
      except:
         print("Selected parameter ({}) not available".format(param))
         print("Please select one from the following list:")
         params = list(temp.keys())
         print(params)
         return 1
         

def build_fn(freq_dict=None, enob=False, options=['a', 'b']):
   #####################################################################
   ## Build filename based on current time
   ## If enob enabled, add information for ENOB analysis
   ##
   now = datetime.now()
   l = []
   l.append(now.strftime("%Y"))
   l.append(now.strftime("%m"))
   l.append(now.strftime("%d"))
   if not(enob):
      l.append(now.strftime("%H%M%S"))
      filename = "-".join(l)
      filename = filename + ".txt"

   else:
      vers, filt, OCXO, manLock, clkInt, adcH = options[0]
      freq = options[1]
      l.append(vers)
      if filt:
         l.append("filt")
      else:
         l.append("noFilt")
      if OCXO:
         l.append("OCXO")
      if manLock:
         l.append("manLock")
      else:
         l.append("autoLock")
      if clkInt:
         l.append("CLKINT")
      else:
         l.append("CLKEXT")
      if adcH:
         l.append("ADCH")
      else:
         l.append("ADCL")

      if freq != "scan":
         l.append(freq)
         filename = "_".join(l)
         filename = filename + ".txt"
      else:
         preset_freqs = list(freq_dict.keys())
         freq_list = [ get_freqParam(freq_dict, f, "Name") for f in preset_freqs ]
         fn = []
         temp_name = "_".join(l) 
         for f in freq_list:
            tmp = temp_name + "_" + f + ".txt"
            fn.append(tmp)
         filename = [preset_freqs, fn]

   return filename

###########################################################################################################




###########################################################################################################

def print_summary(chip_num, init_pass, pll_pass, align_pass, adc_pass, dtu_pass, ctp_pass):
   print("\n")
   print("***************************")
   print("** Summary chip n. {0: >5} **".format(chip_num))
   print("***************************")
   print("")
   print("LiTE-DTU initialization pass:   {}".format(init_pass))
   print("LiTE-DTU PLL lock pass:         {}".format(pll_pass))
   print("LiTE-DTU alignment pass:        {}".format(align_pass))
   print("LiTE-DTU ADC test pass:         {}".format(adc_pass))
   print("LiTE-DTU DTU signals test pass: {}".format(dtu_pass))
   print("LiTE-DTU CATIA TP test pass:    {}".format(ctp_pass))
   print("\n\n")

def write_log(chip_num, datalog_dict):
   fname = "outputs/datalog.dat"
   if not(os.path.isfile(fname)):
      fout = open(fname, "w")
      chip_str = "CHIP"                         ## FC 20.11.2023
      fout.write("{:<10}".format(chip_str))     ## FC 20.11.2023
      for k in datalog_dict.keys():
         fout.write("{:<12}".format(k))
   else:
      fout = open(fname, "a")
   fout.write("\n")
   fout.write("{:<10}".format(chip_num))        ## FC 20.11.2023
   for v in datalog_dict.values():
      fout.write("{:<12}".format(v))
   fout.close()

"""
def write_log(chip_num, pllReg, pllRange, enob_H_fit, enob_H_fft, enob_L_fit, enob_L_fft, N0, N, N1, N2, N3a, N3b, N4):
   #iA, iD, iO = curr[0], curr[1], curr[2]
   iA, iD, iO = -1, -1, -1
   vcoCapSelect_MSB = int(pllReg[0], 16)
   vcoCapSelect_LSB = int(pllReg[1], 16)
   vcoCapSelect = (vcoCapSelect_MSB << 8) + vcoCapSelect_LSB

   fname = "outputs/datalog.dat"
   if not(os.path.isfile(fname)):
      fout = open(fname, "w")
      fout.write("CHIP #   PLL_range   vcoCapSel  ENOB_Hfit  ENOB_Hfft  ENOB_Lfit  ENOB_Lfft    N0     N    N1    N2   N3a   N3b    N4")
   else:
      fout = open(fname, "a")
   fout.write("\n{0:0>6,d}{4: >12,d}{5: >12}{6: >11,.2f}{7: >11,.2f}{8: >11,.2f}{9: >11,.2f}{10: >6}{11: >6}{12: >6}{13: >6}{14: >6}{15: >6}{16: >6}".format(chip_num, iA, iD, iO, pllRange, vcoCapSelect, enob_H_fit, enob_H_fft, enob_L_fit, enob_L_fft, N0, N, N1, N2, N3a, N3b, N4))
   fout.close()
"""

def print_summary_pass(datalog_dict):
   print("LiTE-DTU initialization successful after {} attempts".format(datalog_dict["N_init"]))
   print("")
   if "I_ana" in datalog_dict.keys():
      print("I_ana = {} mA".format(datalog_dict["I_ana"]))
      print("I_dig = {} mA".format(datalog_dict["I_dig"]))
      print("I_ext = {} mA".format(datalog_dict["I_ext"]))
      print("")

   print("LiTE-DTU PLL lock successful after {} attempts".format(datalog_dict["N_pll"]))
   print("")
   hex_val = hex(datalog_dict["pllRegs"])
   pllReg_0 = hex( ( int(hex_val,16) & int("0xf00", 16) ) >> 8)
   pllReg_1 = hex(   int(hex_val,16) & int("0xff",  16) )
   print("PLL lock range = {}".format(datalog_dict["pllRange"]))
   print("PLL VCO cap value: {}".format(datalog_dict["pllRegs"]))
   print("PLL IIC registers -> 0x0f: {}, 0x10: {}".format(pllReg_0, pllReg_1))
   print("")

   print("LiTE-DTU data links alignment successful after {} attempts".format(datalog_dict["N_align"]))
   print("")
   print("Idelay_0: {}".format(datalog_dict["Idel_0"]))
   print("Idelay_1: {}".format(datalog_dict["Idel_1"]))
   print("Idelay_2: {}".format(datalog_dict["Idel_2"]))
   print("Idelay_3: {}".format(datalog_dict["Idel_3"]))
   print("")

   print("ADCs calibration successful after {} attempts".format(datalog_dict["N_adc"]))
   print("")
   print("ADCH baseline:")
   print("- mean: {}".format(datalog_dict["adc_h_mean"]))
   print("- std: {}".format(datalog_dict["adc_h_stdv"]))
   print("")
   print("ADCL baseline:")
   print("- mean: {}".format(datalog_dict["adc_l_mean"]))
   print("- std: {}".format(datalog_dict["adc_l_stdv"]))
   print("")
   print("Fit sine-wave ADCH")
   print("- ampl: {}".format(datalog_dict["fitH_ampl"]))
   print("- freq: {}".format(datalog_dict["fitH_freq"]))
   print("- phase: {}".format(datalog_dict["fitH_phase"]))
   print("- offset: {}".format(datalog_dict["fitH_ofst"]))
   print("")
   print("Fit sine-wave ADCL")
   print("- ampl: {}".format(datalog_dict["fitL_ampl"]))
   print("- freq: {}".format(datalog_dict["fitL_freq"]))
   print("- phase: {}".format(datalog_dict["fitL_phase"]))
   print("- offset: {}".format(datalog_dict["fitL_ofst"]))
   print("")
   print("ENOB ADCH")
   print("fit --> {} bit".format(datalog_dict["ENOB_Hfit"]))
   print("fft --> {} bit".format(datalog_dict["ENOB_Hfft"]))
   print("")
   print("ENOB ADCL")
   print("fit --> {} bit".format(datalog_dict["ENOB_Lfit"]))
   print("fft --> {} bit".format(datalog_dict["ENOB_Lfft"]))
   print("")
   print("Number of calibrations for ADC_H: {}".format(datalog_dict["N_cal_H"]))
   print("Number of calibrations for ADC_L: {}".format(datalog_dict["N_cal_L"]))
   print("")

   print("Baseline subtraction value: {}".format(datalog_dict["Bsln_sub"]))
   print("")

   print("CATIA-like pulse signals test successful after {} and {} attempts".format(datalog_dict["N_G10"], datalog_dict["N_G1"]))
   print("")
   print("CATIA-like pulse for gain 10")
   print("- bsln: {}".format(datalog_dict["G10_bsln"]))
   print("- ampl: {} (MAX = {})".format(datalog_dict["G10_ampl"], datalog_dict["G10_max"]))
   print("- slope: {}".format(datalog_dict["G10_width"]))
   print("- t0: {}".format(datalog_dict["G10_t0"]))
   print("")
   print("Total number of samples = {}".format(datalog_dict["G10_n_tot"]))
   if datalog_dict["G10_n_tot"] > 0:
      print("- number of Baseline samples = {} ({:.2f}%)".format(datalog_dict["G10_n_bsln"], datalog_dict["G10_n_bsln"]/datalog_dict["G10_n_tot"]*100))
      print("- number of Gain 10 samples = {} ({:.2f}%)".format(datalog_dict["G10_n_G10"], datalog_dict["G10_n_G10"]/datalog_dict["G10_n_tot"]*100))
      print("- number of Gain  1 samples = {} ({:.2f}%)".format(datalog_dict["G10_n_G1"], datalog_dict["G10_n_G1"]/datalog_dict["G10_n_tot"]*100))
   print("")

   print("CATIA-like pulse for gain 1")
   print("- bsln: {}".format(datalog_dict["G1_bsln"]))
   print("- ampl: {} (MAX = {})".format(datalog_dict["G1_ampl"], datalog_dict["G1_max"]))
   print("- slope: {}".format(datalog_dict["G1_width"]))
   print("- t0: {}".format(datalog_dict["G1_t0"]))
   print("")
   print("Total number of samples = {}".format(datalog_dict["G1_n_tot"]))
   if datalog_dict["G1_n_tot"] > 0:
      print("- number of Baseline samples = {} ({:.2f}%)".format(datalog_dict["G1_n_bsln"], datalog_dict["G1_n_bsln"]/datalog_dict["G1_n_tot"]*100))
      print("- number of Gain 10 samples = {} ({:.2f}%)".format(datalog_dict["G1_n_G10"], datalog_dict["G1_n_G10"]/datalog_dict["G1_n_tot"]*100))
      print("- number of Gain  1 samples = {} ({:.2f}%)".format(datalog_dict["G1_n_G1"], datalog_dict["G1_n_G1"]/datalog_dict["G1_n_tot"]*100))
   print("")

   print("CATIA test-pulse test successful after {} attempts".format(datalog_dict["N_catia"]))
   print("")
   print("Linear fit results:")
   print("- slope: {}".format(datalog_dict["CATIA_slope"]))
   print("- const: {}".format(datalog_dict["CATIA_const"]))
   print("")

   print("Elapsed time = {} s".format(datalog_dict["test_time"]))
   print("")

   print("Test Result: {}".format(datalog_dict["pass"]))
   print("")

"""
def print_summary_pass(pllReg, pllRange, enob_H_fit, enob_H_fft, enob_L_fit, enob_L_fft, N0, N, N1, N2, N3a, N3b, N4):
      print("LiTE-DTU initialization successful after {} attempts".format(N0))
      print("")
      #print("Current absorption: {}".format(curr))
      print("")

      print("LiTE-DTU PLL lock successful after {} attempts".format(N))
      print("")
      print("PLL lock range = {}".format(pllRange))
      print("PLL IIC registers -> 0x0f: {}, 0x10: {}".format(pllReg[0], pllReg[1]))
      print("")

      print("LiTE-DTU data links alignment successful after {} attempts".format(N1))
      print("")

      print("ADCs calibration successful after {} attempts".format(N2))
      print("")
      print("ENOB ADCH")
      print("fit --> {:.2f} bit".format(enob_H_fit))
      print("fft --> {:.2f} bit".format(enob_H_fft))
      print("")
      print("ENOB ADCL")
      print("fit --> {:.2f} bit".format(enob_L_fit))
      print("fft --> {:.2f} bit".format(enob_L_fft))
      print("")

      print("CATIA-like pulse signals test successful after {} and {} attempts".format(N3a, N3b))
      print("")

      print("CATIA test-pulse test successful after {} attempts".format(N4))
      print("")
"""
###########################################################################################################




###########################################################################################################
###########################################################################################################

