import pyvisa as visa


class ks_e36312a :
   def __init__(self, addr):
      self.rm = visa.ResourceManager()
      self.ps = self.rm.open_resource("TCPIP0::" + addr + "::inst0::INSTR")
      
   def power_on(self):
      self.ps.write(":OUTP ON,(@1,2,3)")

   def power_off(self):
      self.ps.write(":OUTP OFF,(@1,2,3)")

   def read_curr(self):
      self.ps.write("MEAS:CURR? (@1,2,3)")
      outstr   = self.ps.read()
      outlist  = outstr.split(',')
      currents = [ float(s) for s in outlist ]
      return currents
      
   

class td_t3afg120 :
   def __init__(self, addr):
      try: 
         self.rm = visa.ResourceManager()
         self.ipaddr = addr
         self.wfg = self.rm.open_resource('TCPIP0::' + self.ipaddr + '::inst0::INSTR')
         #reply = self.wfg.query('*IDN?')
         #print("WFG ID: " + reply.strip())
      except:
         raise Exception("Unable to reach the waveform generator at " + self.ipaddr)

   def set_wfg(self, mode, G="normal", ampl1=2.7, ofst1=-0.27, ampl2=2.7, ofst2=-0.27):
      if mode == "off":
         self.wfg.write("C1:OUTP OFF");
         self.wfg.write("C2:OUTP OFF");
      elif mode == "calib":
         self.wfg.write("ROSC INT")
         self.wfg.write("C1:OUTP ON,DC")
         self.wfg.write("C1:OUTP PLRT,NOR")
         self.wfg.write("C1:BSWV WVTP,DC")
         self.wfg.write("C1:BSWV OFST,-0.125")
         self.wfg.write("C1:OUTP LOAD,HZ")
         self.wfg.write("C2:OUTP ON,DC")
         self.wfg.write("C2:OUTP PLRT,NOR")
         self.wfg.write("C2:BSWV WVTP,DC")
         self.wfg.write("C2:BSWV OFST,1.325")
         self.wfg.write("C2:OUTP LOAD,HZ")
      elif mode == "bsln":
         self.wfg.write("C1:OUTP ON,DC")
         self.wfg.write("C1:OUTP PLRT,NOR")
         self.wfg.write("C1:BSWV WVTP,DC")
         self.wfg.write("C1:BSWV OFST,-0.125")
         self.wfg.write("C1:OUTP LOAD,HZ")
         self.wfg.write("C2:OUTP ON,DC")
         self.wfg.write("C2:OUTP PLRT,NOR")
         self.wfg.write("C2:BSWV WVTP,DC")
         self.wfg.write("C2:BSWV OFST,1.525")
         self.wfg.write("C2:OUTP LOAD,HZ")
      elif mode == "sat":
         self.wfg.write("C1:OUTP ON,DC")
         self.wfg.write("C1:OUTP PLRT,NOR")
         self.wfg.write("C1:BSWV WVTP,DC")
         self.wfg.write("C1:BSWV OFST,1.925")
         self.wfg.write("C1:OUTP LOAD,HZ")
         self.wfg.write("C2:OUTP ON,DC")
         self.wfg.write("C2:OUTP PLRT,NOR")
         self.wfg.write("C2:BSWV WVTP,DC")
         self.wfg.write("C2:BSWV OFST,-0.825")
         self.wfg.write("C2:OUTP LOAD,HZ")
      elif mode == "sine":
         self.wfg.write("C1:OUTP ON")             ## enalbing output
         self.wfg.write("C1:BSWV WVTP,SINE")      ## setting wave type to sine
         self.wfg.write("C1:BSWV FRQ,500000")     ## setting frequency to 500 kHz
         self.wfg.write("C1:BSWV AMP,2.35")       ## setting Vpp to 2.35 V (500 kHz)
         self.wfg.write("C1:OUTP LOAD,HZ")        ## setting High impedance load
         self.wfg.write("C2:OUTP ON")             ## enalbing output
         self.wfg.write("C2:BSWV WVTP,SINE")      ## setting wave type to sine
         self.wfg.write("C2:BSWV FRQ,500000")     ## setting frequency to 500 kHz
         self.wfg.write("C2:BSWV AMP,2.35")       ## setting Vpp to 2.35 V (500 kHz)
         self.wfg.write("C2:OUTP LOAD,HZ")        ## setting High impedance load
      elif mode == "pulse":
         ##----------------------------------------------------------------------------
         self.wfg.write("C1:BSWV WVTP, PULSE")           ## setting wave type to pulse
         self.wfg.write("C1:BSWV OFST, " + str(ofst1))   ## setting offset
         self.wfg.write("C1:BSWV AMP, " + str(ampl1))    ## setting Vpp amplitude (2.7)
         self.wfg.write("C1:BSWV FRQ, 4500")             ## setting frequency 4.5kHz
         self.wfg.write("C1:BSWV WIDTH, 28e-09")         ## setting pulse width (22ns)
         self.wfg.write("C1:BSWV FALL, 1.63e-08")        ## setting pulse falling time (16.3ns)
         self.wfg.write("C1:BSWV RISE, 9.6e-09")         ## setting pulse rising time (9.6ns, upper limit)
         self.wfg.write("C1:BSWV DLY, 0e-09")            ## setting pulse delay time
         self.wfg.write("C1:OUTP LOAD, HZ")              ## setting high impedance output load
         self.wfg.write("C1:OUTP ON")                    ## enabling output
         ##----------------------------------------------------------------------------
         self.wfg.write("C2:OUTP PLRT, INVT")            ## setting output polarity inversion
         self.wfg.write("C2:BSWV WVTP, PULSE")           ## setting wave type to pulse
         self.wfg.write("C2:BSWV OFST, " + str(ofst2))   ## setting offset
         self.wfg.write("C2:BSWV AMP, " + str(ampl2))    ## setting Vpp amplitude (2.7)
         self.wfg.write("C2:BSWV FRQ, 4500")             ## setting frequency 4.5kHz
         self.wfg.write("C2:BSWV WIDTH, 28e-09")         ## setting pulse width (22ns)
         self.wfg.write("C2:BSWV FALL, 1.63e-08")        ## setting pulse falling time (16.3ns)
         self.wfg.write("C2:BSWV RISE, 9.6e-09")         ## setting pulse rising time (9.6ns, upper limit)
         self.wfg.write("C2:BSWV DLY, 0e-09")            ## setting pulse delay time
         self.wfg.write("C2:OUTP LOAD, HZ")              ## setting high impedance output load
         self.wfg.write("C2:OUTP ON")                    ## enabling output
         ##----------------------------------------------------------------------------

      elif mode == "square":

         low_level = -1.08
         ampl1 =  2.2
         high_level = low_level + ampl1
         ofst1 = (high_level + low_level) / 2
         ampl = str(ampl1)
         ofst = str(ofst1)
            
         self.wfg.write("C1:BSWV WVTP, SQUARE")        ## setting wave type to square
         self.wfg.write("C1:BSWV OFST, " + str(ofst))  ## setting offset
         self.wfg.write("C1:BSWV AMP, " + str(ampl))   ## setting Vpp amplitude
         self.wfg.write("C1:BSWV FRQ, 160e3")          ## setting frequency
         self.wfg.write("C1:BSWV DUTY, 1")             ## setting pulse duty cycle
         self.wfg.write("C1:BSWV DLY, 0e-09")          ## setting pulse delay time
         self.wfg.write("C1:OUTP LOAD, HZ")            ## setting high impedance output load

         self.wfg.write("C2:OUTP PLRT, INVT")          ## setting output polariry inversion
         self.wfg.write("C2:BSWV WVTP, SQUARE")        ## setting wave type to square
         self.wfg.write("C2:BSWV OFST, " + str(ofst))  ## setting offset
         self.wfg.write("C2:BSWV AMP, " + str(ampl))   ## setting Vpp amplitude
         self.wfg.write("C2:BSWV FRQ, 160e3")          ## setting frequency
         self.wfg.write("C2:BSWV DUTY, 1")             ## setting pulse duty cycle
         self.wfg.write("C2:BSWV DLY, 0e-09")          ## setting pulse delay time
         self.wfg.write("C2:OUTP LOAD, HZ")            ## setting high impedance output load

         self.wfg.write("C1:OUTP ON")                  ## enabling output
         self.wfg.write("C2:OUTP ON")                  ## enabling output

         '''
         ##----------------------------------------------------------------------------
         self.wfg.write("C1:OUTP ON")             ## enabling output
         self.wfg.write("C1:BSWV WVTP, PULSE")    ## setting wave type to pulse
         self.wfg.write("C1:BSWV OFST, " + ofst)  ## setting offset
         self.wfg.write("C1:BSWV AMP, " + ampl)   ## setting Vpp amplitude (2.7)
         self.wfg.write("C1:BSWV FRQ, 4500")      ## setting frequency 4.5kHz
         self.wfg.write("C1:BSWV WIDTH, 100e-09")  ## setting pulse width (22ns)
         self.wfg.write("C1:BSWV FALL, 8.4e-09") ## setting pulse falling time (16.3ns)
         self.wfg.write("C1:BSWV RISE, 8.4e-09")  ## setting pulse rising time (9.6ns, upper limit)
         self.wfg.write("C1:BSWV DLY, 0e-09")     ## setting pulse delay time
         self.wfg.write("C1:OUTP LOAD, HZ")       ## setting high impedance output load
         ##----------------------------------------------------------------------------
         self.wfg.write("C2:OUTP ON")             ## enabling output
         self.wfg.write("C2:OUTP PLRT, INVT")     ## setting output polarity inversion
         self.wfg.write("C2:BSWV WVTP, PULSE")    ## setting wave type to pulse
         self.wfg.write("C2:BSWV OFST, " + ofst)  ## setting offset
         self.wfg.write("C2:BSWV AMP, " + ampl)   ## setting Vpp amplitude (2.7)
         self.wfg.write("C2:BSWV FRQ, 4500")      ## setting frequency 4.5kHz
         self.wfg.write("C2:BSWV WIDTH, 100e-09")  ## setting pulse width (22ns)
         self.wfg.write("C2:BSWV FALL, 8.4e-09") ## setting pulse falling time (16.3ns)
         self.wfg.write("C2:BSWV RISE, 8.4e-09")  ## setting pulse rising time (9.6ns, upper limit)
         self.wfg.write("C2:BSWV DLY, 0e-09")     ## setting pulse delay time
         self.wfg.write("C2:OUTP LOAD, HZ")       ## setting high impedance output load
         ##----------------------------------------------------------------------------
         '''
   def set_wfg_duty(self, duty="1"):
      self.wfg.write("C1:BSWV DUTY, " + duty)  ## setting duty cycle
      self.wfg.write("C2:BSWV DUTY, " + duty)  ## setting duty cycle
         
   #def set_wfg_delay(self, delay="20e-09"):
   #  self.wfg.write("C1:BSWV DLY, " + delay)  ## setting pulse delay time
   #  self.wfg.write("C2:BSWV DLY, " + delay)  ## setting pulse delay time

   def set_wfg_delay(self, d1, d2):
      self.wfg.write("C1:BSWV DLY, " + d1)      ## setting pulse delay time C1
      self.wfg.write("C2:BSWV DLY, " + d2)      ## setting pulse delay time C2
         
   def set_burst(self, mode="ext"):
      self.wfg.write("C1:BTWV STATE, ON")         ## setting burst wave state on
      self.wfg.write("C2:BTWV STATE, ON")         ## setting burst wave state on 
      self.wfg.write("C1:BTWV TIME, INF")         ## setting burst infinite mode
      self.wfg.write("C2:BTWV TIME, INF")         ## setting burst infinite mode 
      if mode=="man":
         self.wfg.write("C1:BTWV TRSR, MAN")      ## setting wfg trigger to manual
         self.wfg.write("C2:BTWV TRSR, MAN")      ## setting wfg trigger to manual
         self.wfg.write("C2:BTWV TRMD, RISE")     ## setting trigger output mode to rise
         self.wfg.write("ROSC EXT")               ## set clock source to be external
      elif mode=="ext":
         self.wfg.write("C2:BTWV TRSR, EXT")      ## setting wfg trigger to external
         self.wfg.write("C1:BTWV TRSR, EXT")      ## setting wfg trigger to external

   def do_burst(self):
      self.wfg.write("C2:BTWV MTRIG;:C1:BTWV MTRIG")

   def set_sine(self, freq, ampl):
      self.wfg.write("C1:BSWV FRQ, " + freq)    ## setting C1 frequency to user-defined value
      self.wfg.write("C1:BSWV AMP, " + ampl)    ## setting C1 amplitude to user-defined value
      self.wfg.write("C2:BSWV FRQ, " + freq)    ## setting C2 frequency to user-defined value
      self.wfg.write("C2:BSWV AMP, " + ampl)    ## setting C2 amplitude to user-defined value

   def set_dc(self, ampl):
      self.wfg.write("C2:BSWV OFST," + ampl)

   def set_f(self, freq):
      self.wfg.write("C1:BSWV FRQ, " + freq)    ## setting frequency of C1
      self.wfg.write("C2:BSWV FRQ, " + freq)    ## setting frequency of C2

   def set_a(self, ampl):
      self.wfg.write("C1:BSWV AMP, " + ampl)    ## setting Vpp amplitude of C1
      self.wfg.write("C2:BSWV AMP, " + ampl)    ## setting Vpp amplitude of C2



class rs_sma100b :
   def __init__(self, addr):
      self.rm = visa.ResourceManager()
      self.ipaddr = addr
      self.inst  = self.rm.open_resource('TCPIP0::' + self.ipaddr + '::inst0::INSTR')
        

   def enable_output(self):
      self.inst.write('OUTP ON')

   def disable_output(self):
      self.inst.write('OUTP OFF')

   def setfrequency(self, freq):
      """set frequency in specified units """
      self.inst.write('SOUR:FREQ '+ freq)

   def setamplitude(self, ampl):
      """set amplitude in specified units """
      self.inst.write('SOUR:POW '+ ampl )


      
      
      
      
      
      
      
      
      
      
      
      
      
