'''
Parse input file:
  - remove useless lines: clk at 160MHz, words at 40 MHz"
'''

import pandas as pd
import argparse
from termcolor import cprint

def main(args):
    ctr_netlist = "command_processor_0/adc_controller_0/adc0_seu_counters/LDTUv1b_SeuTeste_0/"
#   filename = "/home/cms/lite-dtu-sw/py-LiTE-DTU-kuodev/microtest/data/pll/report/538/chipscope_crc_3_31_2023-04-05-112731.csv"
    filename = args.filename
    wr_file = args.wr_file
    df = pd.read_csv(filename)
    df = df.drop(['Sample in Buffer', 'Sample in Window', 'command_processor_0/adc_controller_0/adc1_data_word[31:0]'], axis=1)
    df = df.rename(columns={
      "command_processor_0/adc_controller_0/adc0_data_word[31:0]": "ADC0", 
      ctr_netlist + "crc_error"      : "crc_error", 
      ctr_netlist + "nsamples_error" : "nsamples_error", 
      ctr_netlist + "nwords_error"   : "nwords_error", 
      ctr_netlist + "nframe_error"   : "nframe_error", 
      ctr_netlist + "header_error"   : "header_error", 
      ctr_netlist + "crc0_error"     : "crc0_error", 
      ctr_netlist + "read_fd_error"  : "read_fd_error"
    })
#   df = df.apply(lambda x : hex(int(x,2))[2:])
    df["ADC0"] = df["ADC0"].apply(lambda x : "{:08x}".format(int(x,2)))
    print(df.head())
    print("")

    if wr_file:
      out_file = filename[:-4] + "_decoded.txt"
      fout = open(out_file, "w")

    for index, row in df.iterrows():
      ## now the clock is 160 MHz
      ## we want a print every 25 ns (40 MHz clk)
      ## => 1 in 4 samples
      if index%4 == 0:
        if(row["ADC0"] == "eaaaaaaa"):
          cprint(row["ADC0"], "cyan", end = " ")
        elif(row["ADC0"][0] == "d"):
          cprint(row["ADC0"], "green", end = " ")
        else:
          print(row["ADC0"], end = " ") 
        if row["TRIGGER"] == 1:
          cprint(row["TRIGGER"], "red", end = " ")
          cprint(row["header_error"], "red", end = " <<<\n")
        else:
          print(row["TRIGGER"], row["header_error"])

        if wr_file:
          fout.write("{}  {}  {}\n".format(row["ADC0"], row["TRIGGER"], row["header_error"]))

    if wr_file:
      fout.close()

    print()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--filename", type = str, default = "/home/cms/lite-dtu-sw/py-LiTE-DTU-kuodev/microtest/data/pll/report/538/chipscope_crc_3_31_2023-04-05-112731.csv",
                        help = "Input file taken by chipscope")
    parser.add_argument("-w", "--wr_file", default = False, action='store_true',
                        help = "Enable write output decoded file")
##    parser.add_argument("-c", "--chip", type = int, default = 0,
##                        help = "Chip number")
    args = parser.parse_args()
    main(args)
