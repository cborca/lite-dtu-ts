# LiTE-DTU AutoTest for Microtest

## How to install Python requirements

From the root directory of the project run:

```shell
pip3 install -r requirements.txt
```

## How to run the test

The test can be run in two different modes:
* **Microtest mode**: test mode to be used with Microtest setup
* **INFN mode**: test mode to be used with INFN setup

This is due to the fact that the two setups have different external instruments as well as different settings to cope with the discrepancies in cables lenght and timing delays.

### Microtest mode

For **Microtest mode**, from this folder, use the following command to run the automatic test procedure:

```shell
python3 test/autotest.py -c 10
```

where 10 corresponds to chip_ID and must be changed accordingly for each chip tested.


### INFN mode

For **INFN mode**, instead, use the following command:

```shell
python3 test/autotest.py -i -c 10
```

where -i force the test in **INFN mode**.


### Display plots

Summary figures can be displayed at the end of the test by adding the option -p, like:

```shell
python3 test/autotest.py -c 10 -p
```


## How to change test settings

Settings for the test are defined in cfg/autotest.ini (for **Microtest mode**) and in cfg/autotest_infn.ini (for **INFN mode**). This selection is done automatically according to the test mode selected when you run the \"autotest.py" script.

### Main settings
* ***infn***: select INFN or Microtest mode to take into account the different external instruments of the 2 setups
    * **True**: INFN mode
    * **False**: Microtest mode
* ***verbosity***: select verbosity of console output during test 
    * **0**: minimal output
    * **1**: reduced debug output during test + full report at the end
    * **2**: full debug output during test
* ***save_mode***: select display and save mode for figures
    * **0**: don't plot figures, don't save .png files
    * **1**: plot figures but don't save .png files
    * **2**: plot figures and save .png files
* ***data_red***: reduce data saved (txt files)
    * **False**: keep original files
    * **True**: delete original files from baseline subtraction and CATIA-like pulse acquisitions, save only results used for plots
* ***inv_phase***: invert resync command phase to take into account the discrepancies in cables lenght and timing delays between the 2 setups
    * **False**: do not invert resync command phase (default mode)
    * **True**: invert resync command phase (Microtest mode)



## Auto-test procedure
### Initialization
1. Power on
2. I2C DTU config
3. Check currents

### PLL settings optimization
4. PLL manual-lock scan
5. Set auto-lock and compare results

### Links alignment
6. Eye align
7. Data align

### ADC calibration and test
8. ADC calibration procedure
9. Baseline test: check samples mean value and std with calibration signal
10. Sine wave test: fit + ENOB from fit and fft

### DTU mode test
11. Test baseline subtraction
12. Test gain selection and data compression with CATIA-like pulse signals

### CATIA test-pulse
13. Send CATIA test-pulse fast command and measure TP duration for different settings


## Datalog outputs
1. ***TS_version***: test suite version
2. ***pass***: test result
3. ***test_time***: time elapsed for the test
4. ***dtu_ver***: LiTE-DTU version
5. ***i2c_err***: number of tests ended due to I2C configuration error
6. ***pllRegs***: PLL registers from manual scan (2 registers merged)
7. ***pllRange***: PLL lock range
8. ***pllRegAuto***: PLL registers from autoLock (2 registers merged)
9. ***pll_diff***: pllRegAuto - pllRegs
10. ***Idel_0***: tap delay for dout0
11. ***Idel_1***: tap delay for dout1
12. ***Idel_2***: tap delay for dout2
13. ***Idel_3***: tap delay for dout3
14. ***adc_h_mean***: ADC_H baseline mean value
15. ***adc_h_stdv***: ADC_H baseline std value
16. ***adc_l_mean***: ADC_L baseline mean value
17. ***adc_l_stdv***: ADC_L baseline std value
18. ***fitH_ampl***: sine wave amplitude of ADC_H from fit
19. ***fitH_freq***: sine wave frequency of ADC_H from fit
20. ***fitH_phase***: sine wave phase of ADC_H from fit
21. ***fitH_ofst***: sine wave offset of ADC_H from fit
22. ***fitL_ampl***: sine wave amplitude of ADC_L from fit
23. ***fitL_freq***: sine wave frequency of ADC_L from fit
24. ***fitL_phase***: sine wave phase of ADC_L from fit
25. ***fitL_ofst***: sine wave offset of ADC_L from fit
26. ***ENOB_Hfit***: ENOB of ADC_H from fit
27. ***ENOB_Lfit***: ENOB of ADC_L from fit
28. ***ENOB_Hfft***: ENOB of ADC_H from fft
29. ***ENOB_Lfft***: ENOB of ADC_L from fft
30. ***Bsln_sub***: baseline subtraction register value
31. ***G10_n_tot***: total number of samples from CATIA-like pulse acquisition in normal mode
32. ***G10_n_bsln***: number of baseline samples from CATIA-like pulse acquisition in normal mode
33. ***G10_n_G10***: number of G10 samples from CATIA-like pulse acquisition in normal mode
34. ***G10_n_G1***: number of G1 samples from CATIA-like pulse acquisition in normal mode
35. ***G1_n_tot***: total number of samples from CATIA-like pulse acquisition in saturation mode
36. ***G1_n_bsln***: number of baseline samples from CATIA-like pulse acquisition in saturaiton mode
37. ***G1_n_G10***: number of G10 samples from CATIA-like pulse acquisition in saturation mode
38. ***G1_n_G1***: number of G1 samples from CATIA-like pulse acquisition in saturation mode
39. ***G10_max***: value of maximum sample from CATIA-like pulse acquisition in normal mode
40. ***G10_bsln***: baseline value from fit of samples from CATIA-like pulse acquisition in normal mode
41. ***G10_ampl***: amplitude value from fit of samples from CATIA-like pulse acquisition in normal mode
42. ***G10_width***: signal rise width value from fit of samples from CATIA-like pulse acquisition in normal mode
43. ***G10_t0***: start sample from fit of samples from CATIA-like pulse acquisition in normal mode
44. ***G1_max***: value of maximum sample from CATIA-like pulse acquisition in saturation mode
45. ***G1_bsln***: baseline value from fit of samples from CATIA-like pulse acquisition in saturation mode
46. ***G1_ampl***: amplitude value from fit of samples from CATIA-like pulse acquisition in saturation mode
47. ***G1_width***: signal rise width value from fit of samples from CATIA-like pulse acquisition in saturation mode 
48. ***G1_t0***: start sample from fit of samples from CATIA-like pulse acquisition in saturation mode
49. ***CATIA_slope***: slope of CATIA test pulse width scan
50. ***CATIA_const***: offset of CATIA test pulse width scan
51. ***N_init***: number of attempts for successful chip power on
52. ***Nconf***: number of attempts for successful I2C configuration
53. ***N_pll***: number of attempts for successful PLL lock
54. ***N_align***: number of attempts for successful alignment
55. ***Neye***: number of attempts for successful eye alignment
56. ***NdataAlign***: number of attempts for successful data alignment
57. ***N_adc***: number of attempts for successful ADC calibration and test
58. ***N_adc_bsln***: number of attempts for successful ADC DC test
59. ***N_adc_sine***: number of attempts for successful ADC AC test
60. ***N_cal_H***: number of calibrations of ADC_H
61. ***N_cal_L***: number of calibrations of ADC_L
62. ***N_G10***: number of attempts for successful CATIA-like non saturated pulse
63. ***N_G1***: number of attempts for successful CATIA-like saturated pulse
64. ***N_catia***: number of attempts for successful CATIA test-pulse


#### Only for Torino setup:
65. ***I_ana***: analogue power consumption from 3V power supply after configuration (channel 1)
66. ***I_dig***: digital power consumption from 3V power supply after configuration (channel 2)
67. ***I_ext***: external power consumption from 4V power supply after configuration (channel 3)
68. ***I0_ana***: analogue power consumption from 3V power supply before configuration (channel 1)
69. ***I0_dig***: digital power consumption from 3V power supply before configuration (channel 2)
70. ***I0_ext***: external power consumption from 4V power supply before configuration (channel 3)